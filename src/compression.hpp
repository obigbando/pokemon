/// @file compression.hpp
/// @brief defining trait class Compress_Trait, which is presently specialized twice for taking care of the Plain and the Two_bits compression schemes
/// @author C-Salt Corp.
#ifndef COMPRESSION_HPP_
#define COMPRESSION_HPP_
#include "constant_def.hpp"
//#include "compression/twobit.hpp"
#include "compression/n_bit_compress_update.hpp"
/// @class Compress_Trait
/// @brief a policy selection class takes a non-type template parameter compression_type \n this class is specialized twice, as its non-type template parameter compression_type specialized to be the respective values of Plain, i.e. 0, and TWO_BITS, i.e. 1 \n the value_type of the Compress_Trait class is respectively assigned as std::string or NBitCompress in its two specializations
/// @tparam compression_type non-type enum parameter, indicating current compression scheme
template <int CompressBitCount, CompressType C>
struct Compress_Trait
{};

/// @brief specialized form of the Compress_Trait class, with the compression_type specialized to be Plain, indicating that no compression scheme is applied
template <int CompressBitCount>
struct Compress_Trait <CompressBitCount, CompressType::Plain>
{
//    using value_type = std::string; // C++11 feature
	typedef std::string value_type;
};

/// @brief specialized form of the Compress_Trait class, with the compression_type specialized to be TWO_BITS, indicating that NBitCompress encode/compression scheme, defined in CompressRuleTrait<CompressBitCount, CompressType::TWO_BITS> of compression/twobit.hpp, is applied
template <int CompressBitCount>
struct Compress_Trait <CompressBitCount, CompressType::N_BITS>
{
	typedef NBitCompress <CompressRuleTrait <CompressBitCount, CompressType::N_BITS> > value_type;
};

/// @brief specialized form of the Compress_Trait class, with the compression_type specialized to be TWO_BITS_MASK, indicating that NBitCompress encode/compression scheme, defined in CompressRuleTrait<CompressBitCount, CompressType::TWO_BITS_MASK> of compression/twobit.hpp, is applied
template <int CompressBitCount>
struct Compress_Trait <CompressBitCount, CompressType::N_BITS_MASK>
{
	typedef NBitCompress <CompressRuleTrait <CompressBitCount, CompressType::N_BITS_MASK> > value_type;
};
#endif
