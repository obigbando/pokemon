/**
 *  @file annotation_raw_bed.hpp
 *  @brief provide the data structure keeping track of post alignment information, which is derived from raw_bed, but with extra member variables of chromosome (std::string), strand (char), end position (uint32_t), and annotation informatoin (vector<string>).
 *  @author C-Salt Corp.
 */
#ifndef ANNOTATION_RAW_BED_HPP_
#define ANNOTATION_RAW_BED_HPP_
#include "../constant_def.hpp"
#include "raw_bed.hpp"

#include "../pipeline/pipeline_preparator.hpp"

//std::map <std::string, std::string> g_genometable ({{ std::string ("chrX"), std::string (500000000, 'A')} ,});

/**
 * @struct AnnotationRawBed
 * @brief provide a structure keeping track of post annotation information.  It is derived from RawBed, but with some extra member variables such as chromosome (std::string), strand (char), end position (uint32_t), and annotation informatoin (vector<string>)
 * @tparam TUPLETYPE defaulted as tuple < string, int, string, uint64_t, int, string, string, uint64_t, int64_t, string, std::string, int, std::string >,indicate type of the corresponding sam data
 */
template <typename TUPLETYPE
			= std::tuple <
                        std::string, //QNAME
                        int, //SAM_FLAG, //FLAG
                        std::string, //RNAME
                        uint64_t, //POS
                        int, //MAPQ
                        std::string, //CIGAR
                        std::string, //RNEXT
                        uint64_t, //PNEXT
                        int64_t, //TLEN
                        std::string, //SEQ
                        std::string, //QUAL
                        UserDefineTags< boost::mpl::string<'NH'>, boost::mpl::string<'TL'> >
                        //UserDefineContent
                        >
		>
struct AnnotationRawBed
	: public RawBed <TUPLETYPE>
{
/** 
 * @brief used to represent the strand information
 */
	char strand_;
/** 
 * @brief used to represent the end information of the read
 */
	uint32_t end_;
/** 
 * @brief used to represent the chromosome information of the read
 */
	std::string chromosome_;
/** 
 * @brief used to represent the annotation information of the read
 */
	std::vector< std::vector <std::string> > annotation_info_;

/**
 * @brief used to indiate whether the current AnnotationRawBed data have been filtered, indicating value of 1, or not, indicating value of 0
 */
	size_t is_filtered_;

/** 
 * @brief Default constructor
 */
	AnnotationRawBed (void)
		: RawBed<TUPLETYPE>()
	{}

/** 
 * @brief constructor with input RawBed<TUPLETYPE> data
 */
	AnnotationRawBed (const RawBed<TUPLETYPE>& rawin)//(const Sam<TUPLETYPE>& samin)
		: RawBed<TUPLETYPE>(rawin)
		, strand_ (this->GetStrand (this->chr_idx_))
		, end_ (this->start_ + this->length_)
		, chromosome_ (this->GetChr (this->chr_idx_) )//( this->chr_prefix_ + (GetChr (this->chr_idx_) ) )
		, annotation_info_ (0)
		, is_filtered_ (0)
	{}

/** 
 * @brief operator<< overload
 */
    friend std::ostream& operator<< (std::ostream& out, const AnnotationRawBed<TUPLETYPE>& target)
    {
//		out <<target.chr_prefix_;
//		if( target.chr_idx_>=1 && target.chr_idx_<=32 )
//			out << (int)target.chr_idx_ <<"\t+\t";
//		else if( target.chr_idx_>=33 && target.chr_idx_<=64 )
//			out << (int)target.chr_idx_-32 <<"\t-\t";
//		else if( target.chr_idx_>=65 && target.chr_idx_<=90 )
//			out << target.chr_idx_ <<"\t+\t";
//		else if( target.chr_idx_>=97 && target.chr_idx_<=122)
//			out << (char)(target.chr_idx_ -32) <<"\t-\t";
		out << target.GetChr (target.chr_idx_) <<'\t'<< target.GetStrand (target.chr_idx_) <<'\t';
        out<<target.start_                      <<'\t'
        <<target.length_                        <<'\t'
        <<(int)target.tail_length_              <<'\t'
        <<target.multiple_alignment_site_count_ <<'\t'
        <<target.reads_count_                   <<'\t'
        <<target.tail_mismatch_                 <<'\t'
        <<"is_filtered_ " << target.is_filtered_<<'\n';
		out << "annotation content " << target.strand_ << '\t'<<target.end_<<'\t'<<target.chromosome_<<'\n';
		size_t ii=0, jj=0;
		for (auto& q : target.annotation_info_)
		{	
			out<<"level 1, db id # "<< ii << "\tAnno size " << q.size() <<'\n';
			for (auto& qq : q)
			{
				out<<"level 2, db_depth # "<<jj<<'\t';
				out<<qq<<'\t';
				++jj;
			}
			++ii;
		}
        return out;
    }

	std::string ToSam (void)
	{
		std::stringstream out, temp;
		if (strand_=='+')
			out <<"rd"<<this->start_<<'\t'<<"0"<<'\t'<<chromosome_<<'\t';
		else if (strand_=='-')
			out <<"rd"<<this->start_<<'\t'<<"16"<<'\t'<<chromosome_<<'\t';
		//parse columns[3-10]
		out << this->start_<<'\t'<<"0"<<'\t'<<(this->length_-this->tail_length_)<<"M"<<this->tail_length_<<"S"<<'\t'<<"*"<<'\t'<<"0"<<'\t'<<"0"<<'\t'
			<< (PipelinePreparator<>::gGenometable_[chromosome_]).substr (this->start_, this->length_ - this->tail_length_) <<'\t'<<"*"<<'\t';
		out << "NH:i:"<< this->multiple_alignment_site_count_;
		if (this->tail_length_!=0)
		{
        	std::map <int, char> mtable ({ {0,'A'}, {1,'C'}, {2,'G'}, {3,'T'} });
	        std::string temp;
	        for (auto index=this->tail_length_-1; index>=0; --index)
    	        temp.push_back ( mtable [(this->tail_mismatch_ >> (2*index) ) % 4] );
        	out << "\tTL:Z:"<<temp;//<<'\n'; redudant \n
		}
		else
			;//out <<'\n'; redundant \n
		return out.str();
	}
	std::string getTail(void)
	{
		std::string tail = "";
		if (this->tail_length_!=0)
		{
        	std::map <int, char> mtable ({ {0,'A'}, {1,'C'}, {2,'G'}, {3,'T'} });
	        for (auto index=this->tail_length_-1; index>=0; --index)
    	        tail.push_back ( mtable [(this->tail_mismatch_ >> (2*index) ) % 4] );	
		}
		if(strand_ == '-')
		{
			std::reverse(tail.begin(), tail.end());
		}
		return tail;
	}
	char Complement(char c)
	{
		switch (c) {
			case 'A': c = 'T'; break;
			case 'T': c = 'A'; break;
			case 'C': c = 'G'; break;
			case 'G': c = 'C'; break;
			default : return;
		}
		return c;
	}
	
//	static std::map <std::string, std::string> g_genometable; 
//	static std::string g_headerstr;

	public:
/** 
 * @brief parsing and return strnd information based on the chr_idx of the RawBed data structure
 */
//	char GetStrand (uint8_t chr_idx)
//	{
//		if (chr_idx < 128)
//			return '+';
//		else
//			return '-';
//		if ( (  chr_idx>=1 && chr_idx<=32 ) || 
//			 ( chr_idx>=65 && chr_idx<=90 ) )	//forward strand
//			return '+';
//		else if ( ( chr_idx>=33 && chr_idx<=64 ) || 
//				  ( chr_idx>=97 && chr_idx<=122 ) )	//forward strand
//			return '-';
//	}

/** 
 * @brief parsing and return chromosome information based on the chr_idx of the RawBed data structure
 */
//	std::string GetChr (char chr_idx)
//	{
//		if ( chr_idx>=1 && chr_idx<=32 )	//numeric fwd strand
//			return std::to_string ( (int)chr_idx );
//		else if( chr_idx>=33 && chr_idx<=64 ) //numeric rev strand
// 			return std::to_string ( (int)chr_idx - 32 );
//		else if ( chr_idx>=65 && chr_idx<=90 )	//char fwd strand
//			return std::string (1,chr_idx);
//		else if ( chr_idx>=97 && chr_idx<=122 )	//char rev strand
//			return std::string (1,(chr_idx-32) );
//	}
};

//template <typename TUPLETYPE>
//std::map <std::string, std::string> AnnotationRawBed<TUPLETYPE>::g_genometable
//({{ std::string ("chrX"), std::string (500000000, 'A')}});

//template <typename TUPLETYPE>
//std::string AnnotationRawBed<TUPLETYPE>::g_headerstr
//("@HD\tVN:1.0\tSO:unsorted\n@SQ\tSN:chrX\tLN:151058754\n");
/*
/mnt/godzilla/BOWTIE_INDEX/tailer/hg18_chrX.chrLen
*/

#endif
