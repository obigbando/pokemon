///@file Wig.hpp
///@brief Provide definition for Wig Format. 
///@author C-Salt Corp.
#ifndef WIG_HPP_
#define WIG_HPP_
#include <tuple>
#include <string>
#include <cstdint>
#include <vector>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include "../constant_def.hpp"
#include "../tuple_utility.hpp"
#include "../wrapper_tuple_utility.hpp"
#include "is_tuple_type.hpp"

/// @struct Wig
/// @brief Include member elements:\n static type (enum format_type);\n data (TUPLETYPE);\n	eof_flag (bool).
/// @tparam TUPLETYPE defaulted as tuple < string, string, int, Wrapper < vector < tuple < uint32_t, double> > > \n the Wig format indicate member elements: data, defaulted as tuple of: string, indicating track info;\n string,indicating chrom info;\n int, indicating span value;\n vecotor < tuple < uint32, doube > >, indicating start position and corresponding data value.
template < class TUPLETYPE = std::tuple < std::string, //track info 
										std::string,  //chrom
										int, //span
										Wrapper< std::vector < 
										Wrapper< std::tuple<uint32_t, double> > > >	//start position, data value
										>
		 >
struct Wig
{
	static_assert( IsTupleType<TUPLETYPE>::value == true, "ARGUMENT IS NOT A TUPLE");
//Members
///@memberof struct Wig <TUPLETYPE>
///@brief a member element, having a data format capable of storing Wig data\n
///defaulted as tuple < string, string, int, vector < tuple < uint32_t, double > > >
	TUPLETYPE data;
///@memberof struct Wig <TUPLETYPE>
///@brief an enumerated static identifier, indicating aforementioned struct is in Wig<TUPLETYPE> format
	static const format_types type = format_types::WIG;
///@memberof struct Wig <TUPLETYPE>
///@brief a bool flag, indicating whether the Wig data, got by Wig_reader's get_next_entry function, correponds to the eof of the read ifstream object
	bool eof_flag;
///@typedef TupleType as a alias for TUPLETYPE 
	typedef TUPLETYPE TupleType;

//Constructors
    /// @brief Default constructor
    /// @see TEST(Wig, default_constructor) unit test example
	Wig ()	//default constructor
		: data ( TUPLETYPE() ), eof_flag (false)
	{}
    /// @brief Copy constructor
    /// @see TEST(Wig, copy_constructor) unit test example
	Wig ( const Wig& Wg) //Copy constructor
		: data (Wg.data), eof_flag (Wg.eof_flag)
	{}

    /// @brief Assignment operator
    /// @see TEST(Wig, assignment_constructor) unit test example
	Wig& operator=( const Wig& other) //Assignment operator
	{
		eof_flag = other.eof_flag;
		data = other.data;
		return *this;
	}

    /// @brief Move constructor
    /// @see TEST(Wig, move_constructor) unit test example
	Wig (Wig && other)  //Move constructor
		: data( std::move( other.data ) ), eof_flag ( std::move( other.eof_flag) )
	{}

    /// @brief Move assignment operator
    /// @see TEST(Wig, move_assignment) unit test example
	Wig& operator= (Wig && other) //Move assignment operator
	{
		data = std::move(other.data);
		eof_flag = std::move (other.eof_flag);
		return *this;
	}

	/// @brief Consructor with data_in, in TUPLETYPE format
	/// @see TEST(Wig, tupletype_copy_constructor)
	Wig (TUPLETYPE & data_in)
		: data (data_in), eof_flag (false) 
	{}

	/// @brief construct a Wig <TUPLETYPE> with data_in in TUPLETYPE format
	/// @param data_in in TUPLETYPE format
	/// @return struct Wig <TUPLETYPE>
	/// @see TEST(Wig, tupletype_move_constructor)
	Wig (TUPLETYPE &&data_in) 
		: data ( std::move (data_in) ), eof_flag (false) 
	{}//std::cerr<<"move constructor with TUPLETYPE"<<std::endl;};	//constructor with input data
	
	/// @brief construct a Wig <TUPLETYPE> with an EOFFLAG in bool format
	/// @param EofFlag in bool format
	/// @return struct Wig <TUPLETYPE>
	/// @see TEST(Wig, eof_constructor)
	Wig (bool EofFlag) 
		: data ( TUPLETYPE() ),  eof_flag (EofFlag)
	{}//std::cerr<<"EOF constructor"<<std::endl;}	//constructor with input eof_flag

//Functions
    /// @brief Overload operator << to facilitate lexical_cast, having Bed data s converted into a single std::string, so that ofstream operation can be easily achieved
    /// @see TEST(Wig, overload_smaller_operator) unit test example
	friend std::ostream& operator<< (std::ostream& out, const Wig& s)
	{	
		std::string result;
		TupleUtility< TupleType, std::tuple_size< TupleType >::value >::PrintTuple(s.data, result);
		out << result;
		return out;
	}

    /// @brief serialization implementation for Wig struct
    /// @see TEST(Wig, serialization) unit test example
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive &ar, const unsigned int version)
	{
		TupleUtility < TUPLETYPE, std::tuple_size<TUPLETYPE>::value >
			:: SerializeTuple (data, ar, version);
		ar & eof_flag;
	}
};

#endif
