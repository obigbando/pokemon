/**
 *  @file anno_db_bed.hpp
 *  @brief provide the data structure keeping track of post alignment information 
 *  @author C-Salt Corp.
 */
#ifndef ANNO_DB_BED_HPP_
#define ANNO_DB_BED_HPP_
#include <algorithm>
#include <array>
#include <iostream>
#include <string>
#include <tuple>
#include "../constant_def.hpp"
#include "bed.hpp"


template<class FROM, class TO>
struct Convert
{};

template<>
struct Convert	< std::tuple<std::string, uint32_t, uint32_t, std::string, std::string, std::string >
				  ,std::tuple <char, uint32_t, uint32_t, std::vector<std::string> >
				>
{
	const char* chr_prefix_;// = "chr";
	const std::array <int, 256> mtable_;
	
	Convert()
	{
		mtable_['A']=0;mtable_['C']=1;mtable_['G']=2;mtable_['T']=3;
	}
	
	AnnoDbBed
	void operator(std::tuple<std::string, uint32_t, uint32_t, std::string, std::string, std::string > from)
	{
		
	}
};



template<class TUPLETYPE = std::tuple <char, uint32_t, uint32_t, std::vector<std::string> > >
struct AnnoDbBed
	:public Bed<TUPLETYPE>
{
	static const char* chr_prefix_;// = "chr";
	static const std::array <int, 256> mtable_;
	

/** 
 * @brief Default constructor
 */
	//AnnoDbBed(BED_TYPE &bed)
	
		
	
	/*
	bool operator< (const RawBed<TUPLETYPE>& input) const
	{
		if ( chr_idx_ < input.chr_idx_ )
			return true;
		else if ( chr_idx_ > input.chr_idx_ )
			return false;
		else
		{
			if ( start_ < input.start_ )
				return true;
			else if (input.start_ < start_)
				return false;
			else
			{
				if (length_ < input.length_)
					return true;
				else if (input.length_ < length_)
					return false;
				else
				{
					if (tail_length_ < input.tail_length_)
						return true;
					else if ( input.tail_length_ < tail_length_)
						return false;
					else
					{
						if (tail_mismatch_ < input.tail_mismatch_)
							return true;
						else //if ( input.tail_mismatch < tail_mismatch )
							return false;
					}
				}
			}
		}
	}

	friend std::ostream& operator<< (std::ostream& out, const RawBed<TUPLETYPE>& target)
    {
		out <<target.chr_prefix_;
//		[target, &out] () 
//			{
			if ((int)(target.chr_idx_)!=77 && (int)(target.chr_idx_)!=88 && (int)(target.chr_idx_)!=89 )
				out <<(int)target.chr_idx_<<'\t';
			else
				out <<target.chr_idx_<<'\t';
//			};
//		(int)target.chr_idx_					<<'\t'
		out
		<<target.start_ 						<<'\t'
		<<target.length_ 						<<'\t'
		<<(int)target.tail_length_				<<'\t'
		<<target.multiple_alignment_site_count_	<<'\t'
		<<target.reads_count_ 					<<'\t'
		<<target.tail_mismatch_ 				<<'\n';
		return out;
    }

private:
	char ParseChrInfo (std::string Q)
	{
		if (Q.size()>1)
		{
			if( (int)Q[0]>=48 && (int)Q[0]<=57 )    //double digit
				return (char) std::stoi (Q);
			else // multiple chararacter
				return Q[0];
		}
		else if (Q.size()==1)
		{
			if ((int)Q[0]>=48 && (int)Q[0]<=57) //single digit
				return (char) std::stoi (Q);
			else
				return boost::lexical_cast <char> (Q);
		}
	}

	uint32_t ParseTailInfo (std::string tailseq)
	{
		uint32_t tmp_cs=0;
		if (tail_length_ !=0 && tail_length_ <= 16)
		{
			tailseq.resize (tail_length_);
			for(INTTYPE i(0); i < tail_length_; ++i)
				tmp_cs += mtable_[ tailseq [i] ] * std::pow(4,(tail_length_-1-i)) ;
			std::cerr<<"tail seq "<<tailseq<<'\t'<<tmp_cs<<'\n';
			return tmp_cs;
		}
		else
			return tmp_cs;
	}

    int ParseChrInfoImpl (char* c)
	{
		int i = 0;
		while (*(c+i) != '\0')
			++i;
		return i;
	}
	*/
};


template <typename TUPLETYPE>
const char* AnnoDbBed <TUPLETYPE>::chr_prefix_ = "chr";

template <typename TUPLETYPE>
const std::array<int, 256> AnnoDbBed <TUPLETYPE>::mtable_ = 
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

#endif
