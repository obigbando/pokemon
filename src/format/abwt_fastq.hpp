#ifndef ABWT_FASTQ_HPP_
#define ABWT_FASTQ_HPP_

#include "../file_reader.hpp"

/* definition for class Fastq */


// Fastq_parser<Tailer>
class Fastq_parser
	:public FileReader_impl <Fastq , std::tuple <std::string, std::string, std::string, std::string>, SOURCE_TYPE::IFSTREAM_TYPE >
{
private:
	std::string _name {};
	std::string _sequence {};
	std::string _quality {};
	Fastq_parser& operator = (const Fastq_parser&);
	

	
public:
	Fastq < std::tuple <std::string, std::string, std::string, std::string > > current_object;
	
	typedef Fastq<> value_type;
	class badFastq {};
	Fastq_parser () = default;
	
	explicit Fastq_parser (std::vector<std::string> is)
		: FileReader_impl <Fastq , std::tuple <std::string, std::string, std::string, std::string>, SOURCE_TYPE::IFSTREAM_TYPE > (is)
	{}
	
	void parser_combo(std::function<void(void)> callback)
	{
		for (auto file_idx=0; file_idx != this->file_num_; ++file_idx)
		{
			while (true)
			{
				current_object = this->get_next_entry (file_idx);
				if ( current_object.eof_flag )
					break;
				
				//_name = std::move( std::get<0> (current_object.data) );
				//_sequence = std::move( std::get<1> (current_object.data) );
				
				callback();
			}
		}
	}

	/*
	explicit Fastq_parser (std::istream& is) {
		if (is.peek() == '@') {
			is.get ();
			std::getline (is, _name);
			std::getline (is, _sequence);
			boost::to_upper (_sequence);
			is.ignore (100000, '\n');
			std::getline (is, _quality);
			is.peek ();
		} else
			throw badFastq ();
	}
	*/
	Fastq_parser (const Fastq_parser& other):
		_name {other._name},
		_sequence {other._sequence},
		_quality {other._quality}
		{}
	Fastq_parser (Fastq_parser&& other):
		_name {std::move (other._name)},
		_sequence {std::move (other._sequence)},
		_quality {std::move (other._quality)}
		{}
	Fastq_parser& operator=(Fastq_parser&& other) {
		if (this != &other) {
			_name.swap (other._name);
			_sequence.swap (other._sequence);
			_quality.swap (other._quality);
			}
			return *this;
		}
		std::string getName () const {
			return _name;
		}
		std::string getSeq () const {
			return _sequence;
		}
		std::string getQuality () const {
			return _quality;
		}
		friend std::ostream& operator << (std::ostream& os, const Fastq_parser& fastq) {
			os << '@' << fastq._name << '\n' << fastq._sequence << "\n+\n" << fastq._quality << '\n';
		}
		
}; /* end of class Fastq definition */



#endif
