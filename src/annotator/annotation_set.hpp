#ifndef ANNOTATION_SET_HPP_
#define ANNOTATION_SET_HPP_
#include <vector>
#include <string>
#include <initializer_list>
#include <functional>
#include <iostream>
#include <set>
#include "../format/annotation_raw_bed.hpp"

template <
		class InputType,
		int N, //number of annotator 
		class FIRST_ANNOTATOR, //the first annotator
		class... ARGS //other annotator
	>
struct AnnotationSet
	: public FIRST_ANNOTATOR
	, public AnnotationSet <InputType, N - 1, ARGS... >
{
//Member function
	AnnotationSet () 
		: FIRST_ANNOTATOR ( ),
		  AnnotationSet <InputType, N - 1, ARGS... > ()
	{
		
		this -> all_annotate_function.emplace_back ( 
			std::bind ( 
				&FIRST_ANNOTATOR::Annotate, 
				this, 
				std::placeholders::_1,
				std::placeholders::_2
			 ) );
	}
	/*
	AnnotationSet ( std::vector < std::string > vec_file_path ) 
		: FIRST_ANNOTATOR ( *( vec_file_path.rbegin() + ( N - 1 ) ) ),
		  AnnotationSet <InputType, N - 1, ARGS... > ( vec_file_path )
	{
		this -> all_annotate_function.emplace_back ( 
			std::bind ( 
				&FIRST_ANNOTATOR::Annotate, 
				this, 
				std::placeholders::_1
			 ) );
	}
	*/
};

//the final recursion
template <
	class InputType,
	class FIRST_ANNOTATOR
	>
struct AnnotationSet <InputType, 1, FIRST_ANNOTATOR >
	: public FIRST_ANNOTATOR
{
//Member variable
	typedef std::function < int ( typename InputType::value_type&, int ) > FunctionType;
	//typedef std::function < void ( AnnotationRawBed<>& ) > FunctionType;
	
	//std::vector < InputType > all_annotate_function;
	std::vector < FunctionType > all_annotate_function;

//Mermber function
	AnnotationSet ()
		:FIRST_ANNOTATOR ()
	{
		this -> all_annotate_function.emplace_back ( 
			std::bind ( 
				&FIRST_ANNOTATOR::Annotate, 
				this, 
				std::placeholders::_1,
				std::placeholders::_2
			 ) );
	}
	/*
	AnnotationSet ( std::vector < std::string > vec_file_path ) 
		: FIRST_ANNOTATOR ( vec_file_path.back() )
	//AnnotationSet () 
	//	: FIRST_ANNOTATOR ()
	{
		this -> all_annotate_function.emplace_back ( 
			std::bind ( 
				&FIRST_ANNOTATOR::Annotate, 
				this, 
				std::placeholders::_1
			 ) );
	}
	*/
	//template < class ALIGNED_TYPE>
	inline void AnnotateAll ( InputType& aligned_seq_group )
	{
		
		for ( auto& read : aligned_seq_group )
		{
			int db_idx(0);
			read.annotation_info_.resize( all_annotate_function.size() );
			for ( auto& annotate_function : all_annotate_function )
			{
				annotate_function ( read, db_idx );
				++db_idx;
			}
			
		}
	}
};

#endif
