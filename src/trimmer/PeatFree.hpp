/**
 *  @file PeatFree.hpp
 *  @brief provide PeatCloud implementation detail
 *  @author C-Salt Corp.
 */
#ifndef PEAT_CLOUD_HPP_
#define PEAT_CLOUD_HPP_
#include "pair_end_adapter_trimmer.hpp"
#include "../file_reader.hpp"
#include "../format/curl_send_impl.hpp"
/**
 * @class QQ
 * @brief provide a class passed in constructor of boost::iostreams::gzip_compressor to specify certain gz operation parameters
 */
boost::iostreams::gzip_params QQ 
(
    1,//boost::iostreams::gzip::default_compression,//1,
    boost::iostreams::gzip::deflated,
    boost::iostreams::gzip::default_window_bits,
    boost::iostreams::gzip::default_mem_level,
    boost::iostreams::gzip::default_strategy,
    "",
    "",
    0  
);

/**
 * @brief a 25MB size required by BaseSpace for upload in part 
 */
size_t curl_send_size = 26214400;

/**
 * @typedef TupleType
 */
typedef std::tuple<std::string, std::string, std::string, std::string> TupleType;

/**
 * @typedef FileReaderComponent
 * @brief define FileReader with specified template parameters of ParallelTypes::NORMAL, Fastq, TUPLETYPE, SOURCE_TYPE::CURL_TYPE_GZ, and curl_default_handle_mutex as FileReaderComponent
 */
typedef FileReader <ParallelTypes::NORMAL, 
					Fastq, 
					TupleType,
					SOURCE_TYPE::CURL_TYPE_GZ, 
					curl_default_handle_mutex>
					FileReaderComponent;

/**
 * @typedef PeatComponent
 * @brief define PairEndAdapterTrimmer with specified template parameters of ParallelTypes::NORMAL, Fastq, TUPLETYPE, and defaulted TrimTrait type as PeatComponent
 */
typedef PairEndAdapterTrimmer < ParallelTypes::NORMAL, 
								Fastq, 
								TupleType,
								TrimTrait< std::string, LinearStrMatch<double>, double, double > >
								PeatComponent;

/**
 * @typedef FileReaderComponentMT
 * @brief define FileReader with specified template parameters of ParallelTypes::M_T, Fastq, TUPLETYPE, SOURCE_TYPE::CURL_TYPE_GZ, and curl_default_handle_mutex as FileReaderComponentMT
 */
typedef FileReader <ParallelTypes::M_T, 
					Fastq, 
					TupleType,
					SOURCE_TYPE::CURL_TYPE_GZ, 
					curl_default_handle_mutex>
					FileReaderComponentMT;

/**
 * @typedef PeatComponentMT
 * @brief define PairEndAdapterTrimmer with specified template parameters of ParallelTypes::M_T, Fastq, TUPLETYPE, and defaulted TrimTrait type as PeatComponentMT
 */
typedef PairEndAdapterTrimmer < ParallelTypes::M_T, 
								Fastq, 
								TupleType,
								TrimTrait<std::string, LinearStrMatch<double>, double, double > >
								PeatComponentMT;

/**
 * @class PEAT_CLOUD
 * @brief provide a PeatCloud implementation for BaseSpace APP 
 * @tparam READER_COMPONENT indicate the type of FileReader, which must includes a trait of format_type, a constructor with url_in, content_ptr, and size_vec, respectively in types of std::vector<std::string, std::map<int, std::vector<READER_COMPONENT::format_type> >, and std::vector<uint64_t>
 * @tparam PEAT_COMPONENT indicate the type of PairEndAdapterTrimmer, which must includes a trait of parameter_trait_type, a constructor with content_ptr and i_parameter_trait, respectively in types of std::map<int, std::vector<READER_COMPONENT::format_type> > and PEAT_COMPONENT::parameter_trait_type, and a member function named Trim
 */
template <typename READER_COMPONENT, typename PEAT_COMPONENT > 
class PEAT_CLOUD 
	: public READER_COMPONENT
	, public PEAT_COMPONENT
{
	private:
		std::vector < boost::iostreams::filtering_streambuf <boost::iostreams::input> > gz_deflate_pipeline_;
		std::vector < CurlSendImpl <> > senders_;
		std::vector <std::stringstream*> deflate_result_stringstream_;
		std::vector <std::thread> vec_thread_;
		bool flag;
		size_t accumulate_length, accumulate_length2;
		size_t min_trans_size_;
	public:
		PEAT_CLOUD	( std::vector < std::string >& url_in
				, std::vector <uint64_t>& size_vec
				, const std::vector < std::string >& url_upload_info1
				, const std::vector < std::string >& url_upload_info2
				, std::map<int, std::vector< typename READER_COMPONENT::format_type > >* content_ptr 
//				, std::map<int, std::vector< size_t > > * trim_pos_ptr
				, typename PEAT_COMPONENT::parameter_trait_type i_parameter_trait
//				, typename PEAT_COMPONENT::peat_trait_type::MismatchIndicatorType m_indicator
//				, typename PEAT_COMPONENT::peat_trait_type::adapter_compare_scheme_type a_mismatch
//				, typename PEAT_COMPONENT::peat_trait_type::gene_compare_scheme_type g_mismatch  
				, size_t min_trans_size_in = 100000000
				)
		: READER_COMPONENT (url_in, content_ptr, size_vec )
		, PEAT_COMPONENT ( //content_ptr, //trim_pos_ptr, 
							i_parameter_trait )//, m_indicator, a_mismatch, g_mismatch )
		, gz_deflate_pipeline_ (2)
		, senders_ ({ 
			CurlSendImpl <> (url_upload_info1[0], url_upload_info1[1], url_upload_info1[2], url_upload_info1[3] ),
			CurlSendImpl <> (url_upload_info2[0], url_upload_info2[1], url_upload_info2[2], url_upload_info2[3] ) 
					})
		, vec_thread_ (0)
		, flag (true)
		, min_trans_size_ (min_trans_size_in)
		{
			for ( auto i=0; i!=2; ++i)
			{
				deflate_result_stringstream_.push_back ( new std::stringstream ( std::ios_base::in | std::ios_base::out| std::ios::binary  ) );
				gz_deflate_pipeline_[i].push (boost::iostreams::gzip_compressor(QQ));
				gz_deflate_pipeline_[i].push (*(deflate_result_stringstream_.back()) );
			}
		}

		bool Parse_N_Trim (size_t& length, size_t& length2)
		{
			std::cerr<<"PNT begin"<<std::endl;
			std::for_each ( this -> file_handle.begin(), this -> file_handle.end(), [&] ( std::stringstream* Q )
					{	if ( ! Q->good() )	flag = false;	});
			if (!flag)
				return false;
			this -> Read_combo ( boost::bind
					(&PEAT_COMPONENT::Trim, this, this->result2), this->parameter_trait.num );

			for ( auto& i : (*this->result2)[0] )
				length += i.get_data_length();
			for ( auto& i : (*this->result2)[1] )
				length2 += i.get_data_length();
			std::cerr<<"done PNT length length2 "<<length<<' '<<length2<<std::endl;
			return true;
		}

		bool Parse_N_Trim_all (int i)
		{
			size_t sum = 0, rd_count = 0; 
			accumulate_length = deflate_result_stringstream_[0]->tellp() - deflate_result_stringstream_[0]->tellg(),
			accumulate_length2 = deflate_result_stringstream_[1]->tellp() - deflate_result_stringstream_[1]->tellg();//,tg=100000000;
			bool eof=false;//, last_run=false;

			if ( accumulate_length2 < min_trans_size_ && 
				accumulate_length >= min_trans_size_ )
				send_impl (curl_send_size, 0);
			if ( accumulate_length < min_trans_size_ && 
				accumulate_length2 >= min_trans_size_ )
				send_impl (curl_send_size, 1);

			while (accumulate_length < min_trans_size_ || accumulate_length2 < min_trans_size_ )
			{
				if ( !Parse_N_Trim (accumulate_length, accumulate_length2) )
				{
					eof=true;
					break;
				}
				for ( auto& i : (*this->result2)[0] )
					(*deflate_result_stringstream_[0]) << i;
				for ( auto& j : (*this->result2)[1] )
					(*deflate_result_stringstream_[1]) << j;
				(*this->result2)[0].clear();
				(*this->result2)[1].clear();
			}

			send_impl (curl_send_size);
			if (eof)
				send_impl_end (curl_send_size); 
			return !eof;
		}

		void terminate (const std::string& response)
		{
			senders_[1].send_response (response);
		}
	
		std::vector<int> send_impl (int length, size_t index)
		{
			std::cerr<<"entering send_impl length "<<length<<std::endl;
			std::vector<int> result(2, 0);
			int ssize = length;//length[index]-10;
			char* k1 = new char [ssize+1];
			result[index] = boost::iostreams::read ( gz_deflate_pipeline_[index], k1, ssize );
			std::cerr<<"current result"<<result[index]<<std::endl;
			if (result[index] == -1)
				return result;
			clean_buf (index);
			vec_thread_.emplace_back (
				std::thread (
					[this, length] (size_t index, std::vector<int> result, char* k1) 
					{
						this->senders_[index].send (k1, 1, result[index]);
						delete []k1;
					}
					, index, result, k1
				) 
			);
			return result;
		}

		void clean_buf (size_t index)
		{
			std::streamsize read_tg = deflate_result_stringstream_[index]->tellg();
			std::streamsize read_tp = deflate_result_stringstream_[index]->tellp();
			char* ka = new char [read_tp-read_tg];
			(*deflate_result_stringstream_[index]).read(ka, read_tp-read_tg);//boost::iostreams::read (outos[index], ka, read_tp-read_tg);
			(*deflate_result_stringstream_[index]).str("");
			deflate_result_stringstream_[index]->clear();
			(*deflate_result_stringstream_[index]).seekp (0, std::ios::end);
			deflate_result_stringstream_[index]->seekp(0);
			(*deflate_result_stringstream_[index]).write(ka, read_tp-read_tg);
			(*deflate_result_stringstream_[index]).seekp (0, std::ios::end);
			deflate_result_stringstream_[index]->seekg(0);
			(*deflate_result_stringstream_[index]).seekp (0, std::ios::end);
			delete [] ka;
		}

		std::vector<int> send_impl (int length)//(void)
		{
			std::vector<int> result(2,0);
			int ssize = length;
	
			for (size_t index=0; index!=2; ++index)
			{
				char* k1 = new char [ssize+1];
				result[index] = boost::iostreams::read ( gz_deflate_pipeline_[index], k1, ssize );
				std::cerr<<"current result"<<result[index]<<std::endl;
				if (result[index] == -1)// && 
				{
					if (index==0 )//result[1] == -1 )
						continue;
					else if //result[index] == -1 && 
						(index==1 )//result[1] == -1 )
						return result;
				}
				clean_buf (index);
				vec_thread_.emplace_back (
					std::thread (
						[this, length] (size_t index, std::vector<int> result, char* k1) 
						{
							if ( result[index] < length )//|| result[1] < length )
							{
								size_t pos = 0;
								while ( result[index] > 5242880 )
								{
									result[index]-=5242880;
									this->senders_[index].send (k1+pos, 1, 5242880);
									pos += 5242880;
								}
								this->senders_[index].send (k1+pos, 1, result[index]);
							}
							else
								this->senders_[index].send (k1, 1, result[index]);
							delete []k1;
						}
						, index, result, k1
					) 
				);
			}
			return result;
		}

		void send_impl_end (size_t length)
		{
			while (true)
			{
				auto result = send_impl (length);
				if (result[0] == -1 && result[1] ==-1)
					break;
			}
			for (auto& Q: vec_thread_)
				Q.join();
			senders_[0].send_end(); 
			senders_[1].send_end(); 
		}
};

/**
 * @class PEAT_CLOUD_MT
 * @brief provide a PeatCloud implementation for BaseSpace APP 
 * @tparam READER_COMPONENT indicate the type of FileReader, which must includes a trait of format_type, a constructor with url_in, content_ptr, and size_vec, respectively in types of std::vector<std::string, std::map<int, std::vector<READER_COMPONENT::format_type> >, and std::vector<uint64_t>
 * @tparam PEAT_COMPONENT indicate the type of PairEndAdapterTrimmer, which must includes a trait of parameter_trait_type, a constructor with content_ptr and i_parameter_trait, respectively in types of std::map<int, std::vector<READER_COMPONENT::format_type> > and PEAT_COMPONENT::parameter_trait_type, and a member function named Trim
 */
template <typename READER_COMPONENT, typename PEAT_COMPONENT > 
class PEAT_CLOUD_MT 
	: public READER_COMPONENT
	, public PEAT_COMPONENT
{
	private:
		std::vector < boost::iostreams::filtering_streambuf <boost::iostreams::input> > gz_deflate_pipeline_;
		std::vector < CurlSendImpl <> > senders_;
		std::vector < std::shared_ptr<std::stringstream> > deflate_result_stringstream_;
		std::vector <std::thread> vec_thread_;
		bool flag;
		size_t accumulate_length, accumulate_length2;
		size_t min_trans_size_;
		std::queue <int> send_impl_future_vec_;

	public:
		PEAT_CLOUD_MT (   std::vector < std::string >& url_in
						, std::vector <uint64_t>& size_vec
						, const std::vector < std::string >& url_upload_info1
						, const std::vector < std::string >& url_upload_info2
						, std::map<int, std::vector< typename READER_COMPONENT::format_type > >* content_ptr 
						, typename PEAT_COMPONENT::parameter_trait_type i_parameter_trait
						, size_t min_trans_size_in = 100000000
						)
			: READER_COMPONENT (url_in, content_ptr, size_vec )
			, PEAT_COMPONENT ( //content_ptr, 
								i_parameter_trait )
			, gz_deflate_pipeline_ (2)
			, senders_ ({ 
				CurlSendImpl <> (url_upload_info1[0], url_upload_info1[1], url_upload_info1[2], url_upload_info1[3] ),
				CurlSendImpl <> (url_upload_info2[0], url_upload_info2[1], url_upload_info2[2], url_upload_info2[3] ) 
						})
			, vec_thread_ (0)
			, flag (true)
			, min_trans_size_ (min_trans_size_in)
		{
			for ( auto i=0; i!=2; ++i)
			{
				deflate_result_stringstream_.push_back (
					std::make_shared<std::stringstream> (std::ios_base::in | std::ios_base::out| std::ios::binary) );
				gz_deflate_pipeline_[i].push (boost::iostreams::gzip_compressor(QQ));
				gz_deflate_pipeline_[i].push (*(deflate_result_stringstream_.back()) );
			}
		}

		bool Parse_N_Trim (size_t& length, size_t& length2)
		{
			std::for_each ( this -> file_handle.begin(), this -> file_handle.end(), [&] ( std::stringstream* Q )
					{	if ( ! Q->good() )	flag = false;	});
			if (!flag)
				return false;
			this -> Read_combo ( boost::bind 
					(&PEAT_COMPONENT::Trim, this, this->result2), this->parameter_trait.num );
			for ( auto& i : (*this->result2)[0] )
				length += i.get_data_length();
			for ( auto& i : (*this->result2)[1] )
				length2 += i.get_data_length();
			return true;
		}

		bool Parse_N_Trim_all (const std::string& response, size_t uid)
		{
			size_t sum = 0, rd_count = 0;
			accumulate_length = deflate_result_stringstream_[0]->tellp() - deflate_result_stringstream_[0]->tellg();
			accumulate_length2 = deflate_result_stringstream_[1]->tellp() - deflate_result_stringstream_[1]->tellg();
			bool eof=false;
			while ( accumulate_length < min_trans_size_ && accumulate_length2 < min_trans_size_ )
			{
				if ( !Parse_N_Trim (accumulate_length, accumulate_length2) )
				{
					eof=true;
					break;
				}
				for ( auto& i : (*this->result2)[0] )
					(*deflate_result_stringstream_[0]) << i;
				for ( auto& j : (*this->result2)[1] )
					(*deflate_result_stringstream_[1]) << j;
				(*this->result2)[0].clear();
				(*this->result2)[1].clear();
			}
			if (accumulate_length > min_trans_size_ )
				send_impl (curl_send_size, 0, response, uid);
			if (accumulate_length2 > min_trans_size_ )
				send_impl (curl_send_size, 1, response, uid);
			if (eof)
				send_impl_end (curl_send_size, response, uid); 
			return !eof;
		}

		void terminate (const std::string& response)
		{
			senders_[1].send_response (response);
		}

		std::vector<int> send_impl (int length, size_t index , const std::string& update_url, size_t uid_in)
		{
			std::vector<int> result(2, 0);
			int ssize = length;
	
			char* k1 = new char [ssize+1];
			result[index] = boost::iostreams::read ( gz_deflate_pipeline_[index], k1, ssize );
			send_impl_future_vec_.push (
			this->ptr_to_GlobalPool->JobPost (
				[this, length, result, k1, index] ()
				{
					auto ii = result[index];
					if ( ii < length )//|| result[1] < length )
					{
						size_t pos = 0;
						while ( ii > 5242880 )
						{
							ii-=5242880;
							this->senders_[index].send (k1+pos, 1, 5242880);
							pos += 5242880;
						}
						this->senders_[index].send (k1+pos, 1, ii);
					}
					else
						this->senders_[index].send (k1, 1, ii);
					delete []k1;
				}
				));
			uint64_t trans_length_unzip, qq;
            qq = clean_buf(index);

			if (index==0)
			{
				if (this->Curl_device_[0]->got_size_!=this->Curl_device_[0]->gz_file_size_)
				//	uint64_t pp = this->Curl_device_[0]->got_size_ * 10000 / this->Curl_device_[0]->gz_file_size_;
					senders_[0].send_progress_info ( update_url, uid_in, //pp,
														(this->Curl_device_[0]->got_size_ * 10000 / this->Curl_device_[0]->gz_file_size_));
				else
					senders_[0].send_progress_info ( update_url, uid_in, 9990);
			}
			return result;
		}

		size_t clean_buf (size_t index)
		{
			std::streamsize read_tg = deflate_result_stringstream_[index]->tellg();
			std::streamsize read_tp = deflate_result_stringstream_[index]->tellp();
			char* ka = new char [read_tp-read_tg];
			(*deflate_result_stringstream_[index]).read(ka, read_tp-read_tg);//boost::iostreams::read (outos[index], ka, read_tp-read_tg);
			(*deflate_result_stringstream_[index]).str("");
			deflate_result_stringstream_[index]->clear();
			(*deflate_result_stringstream_[index]).seekp (0, std::ios::end);
			deflate_result_stringstream_[index]->seekp(0);
			(*deflate_result_stringstream_[index]).write(ka, read_tp-read_tg);
			(*deflate_result_stringstream_[index]).seekp (0, std::ios::end);
			deflate_result_stringstream_[index]->seekg(0);
			(*deflate_result_stringstream_[index]).seekp (0, std::ios::end);
			delete [] ka;
			return read_tp-read_tg;
		}

		std::vector<int> send_impl (int length, const std::string& update_url, size_t uid_in)//(void)
		{
			std::cerr<<"=====================entering paired send impl ========================"<<std::endl;
			std::vector<int> result(2, 0);
			int ssize = length;//length[index]-10;
			std::vector < std::thread > local_thread;
			char* k1 = new char [length+1];
			char* k2 = new char [length+1];
	
			local_thread.emplace_back ( std::thread (
			[length, this, ssize] (int& result0, char* k1)
			{	result0 = boost::iostreams::read ( gz_deflate_pipeline_[0], k1, ssize );	}
			, std::ref(result[0]), k1 ) );
	
			local_thread.emplace_back ( std::thread (
			[length, this, ssize] (int& result1, char* k2)
			{	result1 = boost::iostreams::read ( gz_deflate_pipeline_[1], k2, ssize );	}
			, std::ref(result[1]), k2 ) );
	
			for (auto& g : local_thread)
				g.join();

			std::cerr<<"current result"<<result[0]<<'\t'<<result[1]<<std::endl;  
	
			size_t trans_length_unzip, qq;
			for (size_t index=0; index!=2; ++index)
			{
				if (result[index] == -1)// && 
				{
					if (index==0 )//result[1] == -1 )
						continue;
					else if //result[index] == -1 && 
						(index==1 )//result[1] == -1 )
						return result;
				}
				//if (result[index] == -1 && index==0 )//result[1] == -1 )
				//	continue;
				//else if (result[index] == -1 && index==1 )//result[1] == -1 )
				//	return result;
				if (index==0)
				{
					qq = clean_buf(index);
					if (qq != 0)
						trans_length_unzip = accumulate_length - clean_buf (index);//(deflate_result_stringstream_[index]);
					else
						trans_length_unzip = 0;
				}
				else
					clean_buf (index);
			}

			if (result[0]>0)
			{	
//++current_send_impl_count_;
//			vec_thread_.emplace_back (
//				std::thread 
//send_impl_future_vec_.push_back (
//int ii = result[0];
send_impl_future_vec_.push (
this->ptr_to_GlobalPool->JobPost (
//				(
//std::function<void(void)> qq = 
					[this, length, result, k1] ()//(int result0, char* k1) 
			{
int ii = result[0];
				if ( ii < length )//|| result[1] < length )
				{
					size_t pos = 0;
					while ( ii > 5242880 )
					{
						ii-=5242880;
						this->senders_[0].send (k1+pos, 1, 5242880);
						pos += 5242880;
					}
					this->senders_[0].send (k1+pos, 1, ii);
				}
				else
					this->senders_[0].send (k1, 1, ii);
				delete []k1;
			}//;
//			, result[0], k1 )
			)
);
			}

			if (result[1]>0)
			{
//++current_send_impl_count_;
send_impl_future_vec_.push (
this->ptr_to_GlobalPool->JobPost (
//			vec_thread_.emplace_back (
//				std::thread (
					[this, length, result, k2] ()//(int result1, char* k2) 
			{
auto ii = result[1];
				if ( ii < length )//|| result[1] < length )
				{
					size_t pos = 0;
					while ( ii > 5242880 )
					{
						ii-=5242880;
						this->senders_[1].send (k2+pos, 1, 5242880);
						pos += 5242880;
					}
					this->senders_[1].send (k2+pos, 1, ii);
				}
				else
					this->senders_[1].send (k2, 1, ii);
				delete []k2;
			}
//			, result[1], k2 )
			));
			}
	
			std::cerr<<"=====================leaving paired send impl========================"<<'\n'<<"current result[0]\t[1]\tsend_impl_future_vec_.size() "<<result[0]<<'\t'<<result[1]<<'\t'<<send_impl_future_vec_.size()<<std::endl;
			senders_[0].send_progress_info ( update_url, this->Curl_device_[0]->file_length_, uid_in, trans_length_unzip );
			return result;
		}
			
		void send_impl_end (size_t length, const std::string& update_url, size_t uid_in)
		{
			while (true)
			{
				auto result = send_impl (length, update_url, uid_in);
				if (result[0] == -1 && result[1] ==-1)
					break;
			}
			while (send_impl_future_vec_.size()!=0)
			{
				this->ptr_to_GlobalPool->FlushOne (send_impl_future_vec_.front());
				send_impl_future_vec_.pop();
			}
			senders_[0].send_progress_info ( update_url, uid_in, 0, true );
			senders_[0].send_end(); 
			senders_[1].send_end(); 
		}
};

#endif
