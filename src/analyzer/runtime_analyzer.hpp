/**
 *  @file runtime_analyzer.hpp
 *  @brief 
 *  @author C-Salt Corp.
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "htslib/htslib/sam.h"
#include <iostream>
#include <string>
#include <vector>
#include <tuple>
#include <sstream>

#include <boost/algorithm/string/split.hpp>

#include "analyzer.hpp"
#include "../src/format/annotation_raw_bed.hpp"
#include "../src/format/sam.hpp"
#include "sam2rawbed.hpp"

class BamParserRegion
{
private:
	std::string filename;
	
	samFile *in;
	char *fn_ref = 0;
	char moder[8];
	bam_hdr_t *header;
	bam1_t *bam;
	
	hts_idx_t *bai_idx;
	hts_itr_t *bam_iter;
	
	kstring_t *kstr;

public:

	BamParserRegion(std::string fn)
		:filename(fn), bam_iter(0)
	{
		open();
	}
	~BamParserRegion()
	{
		delete_kstring(kstr);
		hts_itr_destroy(bam_iter);
		hts_idx_destroy(bai_idx);
		bam_destroy1(bam);
		bam_hdr_destroy(header);
		sam_close(in);
	}
private:
	bool open()
	{
		strcpy(moder, "r");
		strcat(moder, "b");
		
		in = sam_open(filename.c_str(), moder, fn_ref);
		
		header = sam_hdr_read(in);
		header->ignore_sam_err = 0;
		
		bam = bam_init1();
		
		bai_idx = bam_index_load( filename.c_str() );
		
		kstr = new_kstring();
		
		if(bai_idx == 0)
		{
			std::cerr << "Fail to load the BAM index" << std::endl;
			return false;
		}
	}
	
	kstring_t* new_kstring()
	{
		kstring_t *kstr = new kstring_t;
		kstr->s = new char;
		kstr->l = 0;
		kstr->m = 0;
		return kstr;
	}
	void delete_kstring(kstring_t *kstr)
	{
		if(kstr != 0)
		{
			delete[] kstr->s;
			delete kstr;
		}
	}
public:
	bool set_region(std::string region)
	{
		//std::cerr << "set_region()" << std::endl;
		hts_itr_t *tmp_bam_iter = bam_iter;
		if ((bam_iter = bam_itr_querys(bai_idx, header, region.c_str())) == 0) {
			std::cerr << "Fail to parse region" << std::endl;
			return false;
		}
		if(tmp_bam_iter != bam_iter)
		{
			hts_itr_destroy(tmp_bam_iter);
		}
		return true;
	}
	uint32_t ReadRegionLines(std::string region, std::vector<std::string> &lines)
	{
		if( !set_region(region) )
		{
			return 0;
		}
		
		uint32_t num = 0;
		kstring_t *kstr = new_kstring();
		while (bam_itr_next((BGZF*)in->fp, bam_iter, bam) >= 0)
		{
			sam_format1(header, bam, kstr);
			lines.push_back(kstr->s);
			++num;
		}
		return num;
	}
	
	bool getline(std::string &line)
	{
		if (bam_itr_next((BGZF*)in->fp, bam_iter, bam) >= 0)
		{
			sam_format1(header, bam, kstr);
			line = kstr->s;
			return true;
		}
		return false;
	}
	

};


class RuntimeAnalyzer
{
	std::string filename;
	std::string current_region;
	
public:
	typedef AnalyzerParameter <AnalyzerTypes::LengthDistribution> AnaParaLenDist;
	typedef AnalyzerParameter <AnalyzerTypes::Heterogeneity> AnaParaLenDistH;
	
	BamParserRegion bam_parser;
	
	RuntimeAnalyzer(std::string fn)
		:filename(fn), bam_parser(fn), current_region("")
	{}
private:
	
public:
	
	uint32_t bam2rawbed(std::string &region, std::vector< AnnotationRawBed<> > &results, int limit_number = 0)
	{
		std::cerr << "bam2rawbed" << std::endl;
		if(current_region != region)
		{
			current_region = region;
			if(!bam_parser.set_region(region))
			{
				return false;
			}
		}
		uint32_t num (0);
		std::string sam_str;
		
		std::map < RawBed<>, uint16_t > rawbed_map_;
		
		
		while( bam_parser.getline(sam_str) )
		{
			
			Sam<> sam(sam_str);
			std::cerr << sam << std::endl;
			
			++ (*((&(rawbed_map_[RawBed<>(sam)]))-5));
			
			//results.emplace_back( sam );
			//results.emplace_back( Sam<>(sam_str) );
			
			//std::cerr << "results size " << results.size() << std::endl;
			num++;
			if(limit_number != 0 && num == limit_number)
				break;
		}
		
		for (auto& i : rawbed_map_)
			results.emplace_back(i.first);
		
		return num;
	}
	
	void LengthDistribution(std::string region)
	{
		typedef boost::mpl::vector
		<
			boost::mpl::map
			<
				 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
				 //,boost::mpl::pair<AnaParaLenDist::Printer, LenDistNormalPrinter>
			>
		> AnalyzerTypeList;
		
		typedef Analyzer <std::vector< AnnotationRawBed<> >*, AnalyzerTypeList> Analyzers;
		
		std::vector< AnnotationRawBed<> > results;
		
		bam2rawbed(region, results);
		
		std::cerr << "results size " << results.size() << std::endl;
		
		
		Analyzers analyzer;
		analyzer.run(&results);
		
	}
	
	void TailingRatio(std::string region)
	{
		typedef boost::mpl::vector
		<
			boost::mpl::map
			<
				 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
				 ,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadTailing>
				 ,boost::mpl::pair<AnaParaLenDist::Printer, LenDistSeqPrinter>
			>
		> AnalyzerTypeList;
		
		typedef Analyzer <std::vector< AnnotationRawBed<> >*, AnalyzerTypeList> Analyzers;
		
		std::vector< AnnotationRawBed<> > results;
		
		bam2rawbed(region, results);
		
		std::cerr << "results size " << results.size() << std::endl;
		
		
		Analyzers analyzer;
		analyzer.run(&results);
	}
	
	void SeedRatio(std::string region)
	{
		typedef boost::mpl::vector
		<
			boost::mpl::map
			<
				 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
				 ,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadSeed<> >
				 ,boost::mpl::pair<AnaParaLenDist::Printer, LenDistSeqPrinter>
			>
		> AnalyzerTypeList;
		
		typedef Analyzer <std::vector< AnnotationRawBed<> >*, AnalyzerTypeList> Analyzers;
		
		std::vector< AnnotationRawBed<> > results;
		
		bam2rawbed(region, results);
		
		std::cerr << "results size " << results.size() << std::endl;
		
		
		Analyzers analyzer;
		analyzer.run(&results);
	}
	void First1nt(std::string region)
	{
		typedef boost::mpl::vector
		<
			boost::mpl::map
			<
				 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
				 ,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition<1, 0, 1> >
				 ,boost::mpl::pair<AnaParaLenDist::Printer, LenDistSeqPrinter>
			>
		> AnalyzerTypeList;
		
		typedef Analyzer <std::vector< AnnotationRawBed<> >*, AnalyzerTypeList> Analyzers;
		
		std::vector< AnnotationRawBed<> > results;
		
		bam2rawbed(region, results);
		
		std::cerr << "results size " << results.size() << std::endl;
		
		
		Analyzers analyzer;
		analyzer.run(&results);
	}

	
};


