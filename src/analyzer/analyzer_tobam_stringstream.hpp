/**
 *  @file analyzer_tobam.hpp
 *  @brief achieve tobam conversion for the analyzer 
 *  @author C-Salt Corp.
 */
#ifndef ANALYZER_TOBAM_HPP_
#define ANALYZER_TOBAM_HPP_

#include "../tuple_utility.hpp"
#include "../constant_def.hpp"
#include "../converter/sam2bam_up.hpp"
#include "analyzer_tobai.hpp"
#include "boost/filesystem.hpp"
/**
 * @brief Analyzer 此class為提供轉換bam檔的Analyzer參數
 * @tparam ANALYZER_TYPE 特化為 ToBam
 */
template<>
class AnalyzerParameter<AnalyzerTypes::ToBam>
{
public:
	/** 
	 * @brief 定義ToBam參數有哪些
	 */
	/// @brief AnalyzerType 特化Analyzer的type，每一個AnalyzerParameter都應該要有此參數
	typedef boost::mpl::int_<0> AnalyzerType;
	
	/// @brief FilterType 決定Filter後，此analyzer 要取那個Tag，-1=>全，1=>去掉filter tag=1，0=>卻掉filter tag=0。每一個AnalyzerParameter都應該要有此參數
	typedef boost::mpl::int_<1> FilterType;
	
	/// @brief DbIndexType 決定annotation 的 db。可以為空，代表不做。可以為 -1，代表全做。可以為數字，指定db。
	typedef boost::mpl::int_<2> DbIndexType;
	
	/// @brief DbDepthType 決定 第N個 annotation。可以為空，代表不做。可以為 -1，代表全做。可以為數字，指定第N個 annotation。
	typedef boost::mpl::int_<3> DbDepthType;
	
	/// @brief DbDepthNameType 決定annotation name。可以為空，代表全部名字。可以為字串(boost::mpl::string)，指定 annotation name為何。
	typedef boost::mpl::int_<4> DbDepthNameType;
};


/**
 * @brief Analyzer 的實作，此為產生bam檔的特化版本。
 * @tparam INPUT_TYPE 輸入資料的型別，一定為 vector，通常為 vector<AnnoRawBed>
 * @tparam ANALYZER_TYPELIST Analyzer要用的參數設定，通常為 boost::mpl::vector<boost::mpl::map<boost::mpl::pair<KEY, VALUE> > >
 * @tparam ANALYZER_TYPE 特化 Analyzer用的參數，此為ToBam 特化
 */
template<class INPUT_TYPE, class ANALYZER_TYPELIST>
class AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBam>
{
public:
	/// @brief 此為要使用的 analyzer parameter
	typedef AnalyzerParameter <AnalyzerTypes::ToBam> AnaPara;
	
	/// @brief FilterType 決定Filter後，此analyzer 要取那個Tag，-1=>全，1=>去掉filter tag=1，0=>卻掉filter tag=0。每一個AnalyzerParameter都應該要有此參數
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::FilterType, boost::mpl::int_<1> >::type FilterType;
	
	/// @brief DbIndexType 決定annotation 的 db。可以為空，代表不做，此轉為 -2。可以為 -1，代表全做。可以為數字，指定db。
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::DbIndexType, boost::mpl::int_<-2> >::type DbIndexType;
	
	/// @brief DbDepthType 決定 第N個 annotation。可以為空，代表不做，此轉為 -2。可以為 -1，代表全做。可以為數字，指定第N個 annotation。
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::DbDepthType, boost::mpl::int_<-2> >::type DbDepthType;
	
	/// @brief DbDepthNameType 決定annotation name。可以為空，此轉為 "-1"，代表全部名字。可以為字串(boost::mpl::string)，指定 annotation name為何。
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::DbDepthNameType, boost::mpl::string<'-1'> >::type DbDepthNameType;

	/// @brief 輸入的資料
	INPUT_TYPE &in;
	
	/** 
	 * @brief 輸出的資料型別 \n output map <file_name, bam_seq>
	 */
	typedef std::map<std::string, std::shared_ptr < Sam2BamUp<> > > OutPutType;
	
	/// @brief 輸出的資料
	OutPutType out_set_;
	
	/// @brief a buffer to hold the achieved pipeline result, as a pipeline index to its corresponding map of file_name and shared_ptr <stringstream>.  The result will be temporary buffered in gOutSet_ till the results corresponding to all of the precending pipeline indexes have been achieved.  Then the result will be merged into gBuf_ object.
	static std::map< int, std::map <std::string, std::shared_ptr <std::stringstream> > > gOutSet_;
	
	/// @brief a global mutex object for establishing lock_guard object for merging results coming from each of the posted tobam pipelines
	static std::mutex gOutMutex_;

	/// @brief a sequential index, keeping record till which pipe line index that the results have been achieved and been merged into gBuf_ object.  The design is a must be since the merging operation of each tobam pipelines must be achieved in the order of the pipeline index.
	static int gCurrentPipelineIndex_;
	
	/// @brief the object to hold the merged result.  The upload operation or file writing operation will be conducted based on the merged result stored in gBuf_ element
	static std::map <std::string, std::shared_ptr <std::stringstream> > gBuf_;

	/// @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	/// @brief AnalyzerImpl 建構子
	AnalyzerImpl(INPUT_TYPE &i)
		: in (i)//, this_analyzer_count_(0)
	{}

	/// @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	/// @brief AnalyzerImpl 建構子
	AnalyzerImpl()
	{}

	/// @brief a cluster of flags indicating which pipeline has been achieved and bufferred into the static gBuf_ object
	static std::vector <int> flag_;

	// @brief recording map [file_name, tuple < pair<start_block_position, current block_length>, map[pair<chromosome, 16K_tile_index>, tuple< min_uoffset, max_uoffset, read's_start, read's_end>] >]
	typedef std::map <std::string, std::tuple <std::pair<int, int>, std::map < std::pair<std::string, int>, std::tuple <int, int, int, int> > > > BamIndexType;

	/// @brief structure keeping the written bam file length, i.e. the coffset value, and multiple pieces of information having the # of 16kb tile and its corresponding offset value, i.e. uoffset value. 
	BamIndexType out_index_;

	static std::map < int, BamIndexType > gIndex_;

	void* operator()(size_t pipe_index, bool eof_flag)//, std::vector<std::string>& barcode_vec)
	{
		boost::filesystem::path dir("output/");
		boost::filesystem::create_directory(dir);

		for (auto& item : in)
		{
			std::string output_path = std::string("output/sample-") + std::to_string(item.first) + std::string("/");
			boost::filesystem::path dir_analyzer(output_path);
			boost::filesystem::create_directory(dir_analyzer);

			std::stringstream name;
			name << output_path << "out.bam";
			auto name_str = name.str();
			out_set_.insert ( {name_str, std::make_shared < Sam2BamUp<> > (name_str, PipelinePreparator<>::gBam_Header_)} ); 
			for (auto& sam : item.second)
				(out_set_[name_str])->run (sam.str());
		}

		for (auto& item : out_set_)
			out_index_[item.first] = item.second->end_Sam2Bam();

		{
			std::lock_guard<std::mutex> lock(gOutMutex_);

			if ( pipe_index >= flag_.size() )
				flag_.resize (pipe_index+1);
			flag_[pipe_index]=7;

			CopyToBuf (gOutSet_[pipe_index], out_set_);
//			gIndex_.insert ({pipe_index, out_index_});
			MergeImpl (pipe_index);
//			UpdateIndex (pipe_index, eof_flag);
		}

		if (eof_flag)
		{
			TerminateBamFile ();
			for (auto& qq : (gBuf_) )
			{
				std::stringstream ss;
				ss << qq.first;
				std::ofstream ff (ss.str().c_str());
				ff.write ( gBuf_[qq.first]->str().c_str(), gBuf_[qq.first]->str().size()-1);
				ff.close();
			}
			ClearContent ();
		}

		return (void*) &in;
	}
	/**
	 * @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	 * @brief 主要執行Analyzer的對外介面
	 * @param this_analyzer_count 第N個 使用者要求的length distribution，因為輸出資料節過為 vector<map<...>>
	 * @return void
	 */
	void* operator()(int this_analyzer_count, size_t pipe_index, bool eof_flag, size_t barcode_index)
	{
		if(this_analyzer_count == 0)
		{
			/// @brief input, filter = each, db_idx = -1, db_depth=0, db_depth_value, sys
			Analysis (in, -1, -1, 0, "-2", pipe_index, 0);
			Analysis (in, 1, -1, 0, "-2", pipe_index, 0);
			//Analysis (in, 0, -1, 0, "-2", 0);
		}
		Analysis (in, FilterType::value, DbIndexType::value, DbDepthType::value, boost::mpl::c_str<DbDepthNameType>::value, pipe_index); 
		MergeToBuf (pipe_index);
		if (eof_flag)
		{
			TerminateBamFile ();
			UpdateIndex ();//pipe_index);
			WriteToFile (barcode_index);
			ClearContent ();
		}
		return (void*) in;
	}

	void PrintIndex (void)
	{
		std::cerr<<"======tobam: PrintIndex========="<<'\n';
		for (auto& item0 : gIndex_)
		{
			std::cerr<<"pipe_index: "<<item0.first<<'\n';
			for (auto& item1 : item0.second)
			{
				std::cerr<<"file_name : hdr_block_length_ : ofss_length_ "<<item1.first<<'\t'<<std::get<0>(item1.second).first<<'\t'<<std::get<0>(item1.second).second<<'\n';
				for (auto& item2 : std::get<1>(item1.second) )
				{
					std::cerr<<"chromsome : tile "<<item2.first.first<<'\t'<<item2.first.second<<'\n';
					std::cerr<<"minuoffset : maxuoffset : start : end "<<std::get<0>(item2.second)<<'\t'<<std::get<1>(item2.second)<<'\t'<<std::get<2>(item2.second)<<'\t'<<std::get<3>(item2.second)<<'\n';
				}
			}
		}
	}
private:
	/**
	 * @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	 * @brief provide an interface to clear all static members
	 * @return void
	 */
	void ClearContent (void)
	{
		gOutSet_.clear();
		gCurrentPipelineIndex_=0;
		flag_.clear();
		gBuf_.clear();
		gIndex_.clear();
	}

	/**
	 * @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	 * @brief have the current obtained bam contents, recorded into out_set_, copied into gOutSet_ and, if possible, merged into gBuf_
	 * @return void
	 */
	void MergeToBuf (size_t pipe_index)
	{
		for (auto& item : out_set_)
			out_index_[item.first] = item.second->end_Sam2Bam();	
		//close current bam files, in stringstream format, without adding tailing empty bgzf block, i.e. preserve future appending possibility
		{
			std::lock_guard<std::mutex> lock(gOutMutex_);

			if ( pipe_index >= flag_.size() )
				flag_.resize (pipe_index+1);
			flag_[pipe_index]=7;	//use flag_ to to identify other pipeline that tobam operation corresponding to this very pipe_index has been achieved.  	
									//int 7 used as an indication for pipeline opeartion achieved

			CopyToBuf (gOutSet_[pipe_index], out_set_);	//having bam file contents of out_set_ recorded into gOutSet_
			MergeImpl (pipe_index);	//having bam fragments in gOutSet_ merged into gBuf_.  
			gIndex_.insert ({pipe_index, out_index_});
		}
	}
	
	/**
	 * @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	 * @brief implementation for copy out_set_ content into gOutSet_
	 * @return void
	 */
	void CopyToBuf(std::map<std::string, std::shared_ptr<std::stringstream> >& map_sum, OutPutType &map_item)
	{
		for (auto& q1 : map_item)
			map_sum[q1.first] = q1.second->ofss_;	//simply have [file_name, std::shared_ptr<std::stringstream>] pair, kept in the out_set_ copied into global object gOutSet_
	}

	/**
	 * @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	 * @brief have bam content in gOutSet_ merged into gBuf_.  It is noted that the merge operation must be achieved following the order of  pipe_index, e.g. merging operation corresponding to pipe_index=0 must be achieved prior to that corresponding to pipe_index=1, and so forth.
	 * @return void
	 */
	void MergeImpl (size_t pipe_index)
	{
		int jump = gCurrentPipelineIndex_;
		for (auto ind=gCurrentPipelineIndex_; ind!=flag_.size(); ++ind)
		{
			if (flag_[ind]==7)
				++jump;
			else
				break;
		}	//scanning through flag_ to know till which pipe_index that all of its antecedent counterparts have been done, 
			//so that we can sequentially have each of which merged into gBuf_

		for (int out_index=gCurrentPipelineIndex_; out_index!=jump; ++out_index)	//merge pipe content from gCurrentPipelineIndex_ till jump
		{
			for (auto& pipe_content : (gOutSet_[out_index]) )
			{
				auto flag = gBuf_.insert ({pipe_content.first, std::make_shared < std::stringstream > () });
				if (flag.second == true)	//i.e. current bam content is the first bam file fragment corresponding to the file name of pipe_content.first
					InitiateBamFile (pipe_content.first);	//write bam file header block in front of the bam content

				auto pipe_bam_content = (gOutSet_[out_index][pipe_content.first])->str();
				auto bam_end = (gOutSet_[out_index][pipe_content.first])->tellp(), bam_start = (gOutSet_[out_index][pipe_content.first])->tellg();
				(gBuf_[pipe_content.first])->write ( pipe_bam_content.c_str(), bam_end-bam_start );
				(gOutSet_[out_index][pipe_content.first]).reset ();	//have the bam content written into gBuf_
			}
		} 
		gCurrentPipelineIndex_=jump;
	}

	/**
	 * @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	 * @brief provide an interface to close bam files and append the tailing 27 byte bgzf blocks at the rear of each gBuf_ files to terminate bam file written for good. 
	 * @return void
	 */
	void TerminateBamFile (void)
	{
		for (auto& item : gBuf_)
		{
			Sam2BamUp<> SsCloser (item.first, PipelinePreparator<>::gBam_Header_);	//provide a Sam2BampUp object for tailing bgzf block written 
			SsCloser.final_Sam2Bam();	
			auto FinalContent = SsCloser.ofss_->str();
			auto content_end = SsCloser.ofss_->tellp(), content_start = SsCloser.ofss_->tellg();
			(gBuf_[item.first])->write ( FinalContent.c_str(), content_end-content_start );
		}
	}

	/**
	 * @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	 * @brief provide an interface to close bam files and append the tailing 27 byte bgzf blocks at the rear of each gBuf_ files to terminate bam file written for good. 
	 * @return void
	 */
	void InitiateBamFile (const std::string& file_name)
	{
		Sam2BamUp<> Ssheader (file_name, PipelinePreparator<>::gBam_Header_);	//provide a Sam2BampUp object for header block written 
		Ssheader.WriteBamHeader();//		auto hdr_length = 
		std::get<0>(out_index_[file_name]).first = std::get<0>(Ssheader.end_Sam2Bam()).first;//hdr_length;	//record header block length in out_index_ object
		auto headerContent = Ssheader.ofss_->str();
		auto hdr_end = Ssheader.ofss_->tellp(), hdr_start = Ssheader.ofss_->tellg();
		(gBuf_[file_name])->write ( headerContent.c_str(), hdr_end-hdr_start );
	}

	/**
	 * @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	 * @brief provide an interface to do bam and bai file writting
	 * @return void
	 */
	void WriteToFile (size_t barcode_index)
	{
		std::string output_path = std::string("output/sample-") + std::to_string(barcode_index) + std::string("/bam/");
		boost::filesystem::path dir_analyzer(output_path);
		boost::filesystem::create_directory(dir_analyzer);

		ToBai (&gIndex_, output_path);
		for (auto& qq : (gBuf_) )
		{
			std::stringstream ss;
			ss << output_path << qq.first;
			std::ofstream ff (ss.str().c_str());
			ff.write ( gBuf_[qq.first]->str().c_str(), gBuf_[qq.first]->str().size()-1);
			ff.close();
		}
	}

	/**
	 * @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	 * @brief have the start position corresponding to this pipe_index, i.e. std::get<0>(gIndex_[idx][item.first]).first, updated to be the sum of start_block_position and current block_length of previous pipe_index, i.e. std::get<0>(gIndex_[idx-1][item.first])).first and std::get<0>(gIndex_[idx-1][item.first]).second
	 * @return void
	 */
	void UpdateIndex (void)
	{
		for (auto& item : gBuf_)
		{
			int index=0;
			for (; index!=gIndex_.size() && gIndex_[index].find (item.first) == gIndex_[index].end(); ++index)
				;
			if (index==gIndex_.size())
				continue;
			else
				for (size_t idx=index+1; idx!=gIndex_.size(); ++idx)
					(std::get<0>(gIndex_[idx][item.first])).first = (std::get<0>(gIndex_[idx-1][item.first])).first+(std::get<0>(gIndex_[idx-1][item.first])).second;
		}
	}
	
	/**
	 * @brief Analysis在run時，所呼叫的function，主要是判斷此 read 是不是要記錄
	 * @param is_filter Filter後 read(anno_rawbed)內部所記錄的 filter tag
	 * @param set_filter 使用者參數所設定的 filter tag (要filter 哪個 tag)，配合 is_filter 來決定 read 是否納入計算。-1 表示全部納入計算，0 表示去掉 tag=0，1表示去掉tag=1
	 * @return bool 回傳是否 filter掉（不納入計算）
	 */
	inline bool IsFilter(const int is_filter, const int set_filter)
	{
		if(set_filter == -1)
		{
			return false;
		}
		else
		{
			if(is_filter == set_filter)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	/**
	 * @brief Analysis實作，對 read做計算，記入進 output
	 * @param in 輸入的資料
	 * @param filter 使用者定的 filter 參數，詳情請看上面的 typedef
	 * @param db_index 使用者定的 db_index 參數，詳情請看上面的 typedef
	 * @param db_depth 使用者定的 db_depth 參數，詳情請看上面的 typedef
	 * @param db_depth_name 使用者定的 db_depth_name 參數，詳情請看上面的 typedef
	 * @return void
	 */
	void Analysis2(INPUT_TYPE in, const int filter, const int db_index, const int db_depth, const char* db_depth_name, size_t pipe_index)
	{
		for(auto &anno_rawbed : *in)
		{
//			if(this_analyzer_count_ == 0 )
//			{
				//必做
				//所有 reads 的 len dist，sys=0 => db_idx =false, db_depth = false
				CalOutput< decltype(anno_rawbed)>(anno_rawbed, 0, -1, 0, 0, "", pipe_index);
				
				//沒有被 Filter掉
				if(IsFilter(anno_rawbed.is_filtered_, filter))
					CalOutput< decltype(anno_rawbed)>(anno_rawbed, 0, 0, 0, 0, "", pipe_index);
				
				//有被 Filter掉
				//std::cerr << "IsFilter " <<IsFilter(anno_rawbed.is_filtered_, filter) << std::endl;
				if(!IsFilter(anno_rawbed.is_filtered_, filter))
					CalOutput< decltype(anno_rawbed)>(anno_rawbed, 0, 1, 0, 0, "", pipe_index);	
//			}


			if(IsFilter(anno_rawbed.is_filtered_, filter))
				continue;
			
			if(db_index == -2) // db_index = false
			{
				//不做其他，只把 total reads length 分布（沒有任何細分類）
				continue;
			}
			else if(db_index >= 0)
			{
				//做特定 db_idx
				if(anno_rawbed.annotation_info_[db_index].size() == 0)
					continue;
				
				//所有 reads 的 len dist，sys=1 => db_idx = int, db_depth = false
				CalOutput< decltype(anno_rawbed)>(anno_rawbed, 1, filter, db_index, 0, "", pipe_index);

				//要做 depth = int
				if(db_depth == -2)
				{
					continue;
				}
				else if(db_depth >= 0)
				{
					//CalOutput< decltype(anno_rawbed), GET_READ_LENGTH_TYPE,CAL_READ_COUNT_TYPE >(anno_rawbed, 2, filter, db_idx, db_depth, "");
					if(db_depth_name == "-1")//bool false
					{
						//在特定depth下，所有 name 全做
						CalOutput< decltype(anno_rawbed)>
						(anno_rawbed, 2, filter, db_index, db_depth, anno_rawbed.annotation_info_[db_index][db_depth].c_str(), pipe_index);
						continue;
					}
					//name 有指定，做特定
					if(db_depth_name == anno_rawbed.annotation_info_[db_index][db_depth].c_str())
					{
						CalOutput< decltype(anno_rawbed)>
						(anno_rawbed, 2, filter, db_index, db_depth, anno_rawbed.annotation_info_[db_index][db_depth].c_str(), pipe_index);
					}
					continue;
				}
				else //depth == -1
				{
					
					//所有全做
					for(int db_dep(0); db_dep < anno_rawbed.annotation_info_[db_index].size(); ++db_dep)
					{
						CalOutput< decltype(anno_rawbed)>
						(anno_rawbed, 2, filter, db_index, db_dep, anno_rawbed.annotation_info_[db_index][db_dep].c_str(), pipe_index);
					}

				}
				
			}
			else // db_index == -1, depth all, name all
			{
				
				for(int db_idx(0); db_idx != anno_rawbed.annotation_info_.size(); ++db_idx)
				{
					if(anno_rawbed.annotation_info_[db_idx].size() == 0)
						continue;
					//所有db 個別的 total len dist
					CalOutput< decltype(anno_rawbed)>(anno_rawbed, 1, filter, db_idx, 0, "", pipe_index);
					
					if(db_depth == -2)
					{
						continue;
					}
					else if(db_depth >= 0)
					{
						if(db_depth_name == "-1")//bool false
						{
							//在特定depth下，所有 name 全做
							CalOutput< decltype(anno_rawbed)>
							(anno_rawbed, 2, filter, db_idx, db_depth, anno_rawbed.annotation_info_[db_idx][db_depth].c_str(), pipe_index);
							continue;
						}
						//name 有指定，做特定
						if(db_depth_name == anno_rawbed.annotation_info_[db_idx][db_depth].c_str())
						{
							CalOutput< decltype(anno_rawbed)>
							(anno_rawbed, 2, filter, db_idx, db_depth, anno_rawbed.annotation_info_[db_idx][db_depth].c_str(), pipe_index);
						}
					}
					else
					{
						// db_depth == -1, 全做
						for(int db_dep(0); db_dep < anno_rawbed.annotation_info_[db_idx].size(); ++db_dep)
						{
							CalOutput< decltype(anno_rawbed)>
							(anno_rawbed, 2, filter,db_idx, db_dep, anno_rawbed.annotation_info_[db_idx][db_dep].c_str(), pipe_index);
		
						}

					}				
					
				}
			}
		}
	}
	
	/**
	 * @brief Analysis實作，對 read做計算，記入進 output
	 * @param in 輸入的資料
	 * @param filter 使用者定的 filter 參數，詳情請看上面的 typedef
	 * @param db_index 使用者定的 db_index 參數，詳情請看上面的 typedef
	 * @param db_depth 使用者定的 db_depth 參數，詳情請看上面的 typedef
	 * @param db_depth_name 使用者定的 db_depth_name 參數，詳情請看上面的 typedef
	 * @return void
	 */
	void Analysis(INPUT_TYPE in, const int filter, const int db_index, const int db_depth, const char* db_depth_name, size_t pipe_index, int sys = 2)
	{
		for(auto &anno_rawbed : *in)
		{
            CalOutput< decltype(anno_rawbed)>(anno_rawbed, 0, -1, 0, 0, "", pipe_index);
/*			if(IsFilter(anno_rawbed.is_filtered_, filter))
				continue;
			
			for(int db_idx(0); db_idx != anno_rawbed.annotation_info_.size(); ++db_idx)
			{
				int check_db_idx = get_key(db_index, db_idx);
				if(check_db_idx == -3)
					continue;
				
				for(int db_dep(0); db_dep != anno_rawbed.annotation_info_[db_idx].size(); ++db_dep)
				{
					int check_db_depth = get_key(db_depth, db_dep);
					if(check_db_depth == -3)
						continue;
						
					std::string &db_depth_value = anno_rawbed.annotation_info_[db_idx][db_dep];
					std::string check_db_name = get_key(db_depth_name, db_depth_value);
					if(check_db_name == "-3")
						continue;
					
					CalOutput< decltype(anno_rawbed)> 
					(anno_rawbed, sys, filter, check_db_idx, check_db_depth, check_db_name.c_str(), pipe_index);
				}
			}
*/		}
	}

	std::string get_key(const std::string &type, const std::string &value)
	{
		if(type == "-1")
		{
			return value;
		}
		else if(type == "-2")
		{
			return "";
		}
		else
		{
			if(type == value)
			{
				return value;
			}
			else
			{
				return "-3";
			}
		}
		
	}

	int get_key(const int &type, const int &value)
	{
		if(type == -1)
		{
			return value;
		}
		else if(type == -2)
		{
			return -2;
		}
		else
		{
			if(type == value)
			{
				return value;
			}
			else
			{
				return -3;
			}
		}
	}
	
	/**
	 * @brief Analysis在run時，所呼叫的function，主要是簡化重複程式碼，此部份主要負責 insert into map of output
	 * @tparam READ_TYPE 自動決定 anno rawbed data type
	 * @param anno_rawbed read 的資料結構，詳情請看 annotation_raw_bed.hpp
	 * @param sys 此為特殊參數，主要是output所必須記錄，0=> db_index為空 and db_depth為空，1=> db_index為空 and db_depth不為空，2=>db_index不為空 and db_depth不為空
	 * @param filter 使用者定的 filter 參數，詳情請看上面的 typedef
	 * @param db_index 使用者定的 db_index 參數，詳情請看上面的 typedef
	 * @param db_depth 使用者定的 db_depth 參數，詳情請看上面的 typedef
	 * @param db_depth_name 使用者定的 db_depth_name 參數，詳情請看上面的 typedef
	 * @return void
	 */
	template<class READ_TYPE>
	inline void CalOutput (READ_TYPE &anno_rawbed ,const int sys, const int filter, const int db_index, const int db_depth, const char* db_depth_name, size_t pipe_index)
	{
		std::stringstream name;
		name <<sys<<filter<<db_index<<db_depth<<db_depth_name<<".bam";	//output bam file name construction
		auto name_str = name.str();
		if (out_set_.find (name_str) != out_set_.end() )
			(out_set_[name_str])->run (anno_rawbed);	//using established std::shared_ptr < Sam2BamUp<> > to have inputted annotation_raw_bed written into bam file
		else 
		{
			out_set_.insert ( {name_str, std::make_shared < Sam2BamUp<> > (name_str, PipelinePreparator<>::gBam_Header_)} ); //anno_rawbed.g_headerstr)});
			(out_set_[name_str])->run (anno_rawbed);	//establish [file_name, std::shared_ptr <Sam2BamUp<> > pair to do bam file written operation
		}
	}
};



template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::mutex
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBam>::gOutMutex_;

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::map<int, std::map<std::string, std::shared_ptr<std::stringstream> > >
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBam>::gOutSet_;

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
int 
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBam>::gCurrentPipelineIndex_=0;

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::vector <int> 
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBam>::flag_(0);

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::map <std::string, std::shared_ptr <std::stringstream> >
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBam>::gBuf_;

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::map < int, std::map <std::string, std::tuple < std::pair<int, int>, std::map < std::pair<std::string, int>, std::tuple <int, int, int, int> > > > >
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBam>::gIndex_;

template<class INPUT_TYPE>
class AnalyzerImplInitToBam
{
public:

	INPUT_TYPE in;
	size_t pipe_index_;
	bool eof_flag_;
	std::map<int,int> analyzer_count_type_;

	//typedef std::map <std::string, std::tuple <std::pair<int, int>, std::map <int, std::tuple <int, int, int, int> > > > BamIndexType;
	typedef std::map <std::string, std::tuple <std::pair<int, int>, std::map < std::pair<std::string, int>, std::tuple <int, int, int, int> > > > BamIndexType;
	static std::map <int, BamIndexType >* gIndexPtr_;
	
	AnalyzerImplInitToBam(INPUT_TYPE i)
		: in (i)
	{}

	AnalyzerImplInitToBam()
	{}

	~AnalyzerImplInitToBam()
    {
		delete gIndexPtr_;
	}

	template<class ANALYZER_TYPELIST>
	void //	std::map <int, BamIndexType >*
	operator()(ANALYZER_TYPELIST t)
	{
		//取得 analyzer type
		typedef typename boost::mpl::at<ANALYZER_TYPELIST, boost::mpl::int_<0> >::type AnalyzerType;
		AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerType::value> Analysis(in);
		gIndexPtr_ = (std::map <int, BamIndexType >*) Analysis(analyzer_count_type_[AnalyzerType::value], 
				pipe_index_, eof_flag_ );	
		++analyzer_count_type_[AnalyzerType::value];
	}

	AnalyzerImplInitToBam(INPUT_TYPE i, size_t pipe_index, bool eof_flag)
		: in (i), pipe_index_ (pipe_index), eof_flag_ (eof_flag)
	{}
};

template<class INPUT_TYPE>
std::map <int, std::map <std::string, std::tuple <std::pair<int, int>, std::map < std::pair<std::string, int>, std::tuple <int, int, int, int> > > > >*
AnalyzerImplInitToBam <INPUT_TYPE>::gIndexPtr_;


template<class INPUT_TYPE, class ANALYZER_TYPELIST>
class AnalyzerToBam
{
public:
	typedef std::map <std::string, std::tuple <std::pair<int, int>, std::map < std::pair<std::string, int>, std::tuple <int, int, int, int> > > > BamIndexType;

	std::map <int, BamIndexType>* 
	run (INPUT_TYPE in, size_t pipe_index=0, bool eof_flag =false) 
	{
		boost::mpl::for_each<ANALYZER_TYPELIST> ( AnalyzerImplInitToBam<INPUT_TYPE>( in, pipe_index, eof_flag ) );
		return AnalyzerImplInitToBam<INPUT_TYPE>::gIndexPtr_;
	}
};

#endif


