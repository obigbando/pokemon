#ifndef ANALYZER_HETEROGENEITY_HPP_
#define ANALYZER_HETEROGENEITY_HPP_

#include "../constant_def.hpp"
#include "../tuple_utility.hpp"

#include "analyzer_utility.hpp"
#include "analyzer_policy.hpp"
#include "analyzer_printer.hpp"

/**
 * @brief Analyzer 此class為提供計算Heterogeneity的Analyzer參數
 * @tparam ANALYZER_TYPE 特化為 Heterogeneity
 */
template<>
class AnalyzerParameter<AnalyzerTypes::Heterogeneity>
{
public:
	/** 
	 * @brief 定義Heterogeneity參數有哪些
	 */
	
	/// @brief AnalyzerType 特化Analyzer的type，每一個AnalyzerParameter都應該要有此參數
	typedef boost::mpl::int_<0> AnalyzerType;
	
	/// @brief FilterType 決定Filter後，此analyzer 要取那個Tag，-1=>全，1=>去掉filter tag=1，0=>卻掉filter tag=0。每一個AnalyzerParameter都應該要有此參數
	typedef boost::mpl::int_<1> FilterType;
	
	/// @brief DbIndexType 決定annotation 的 db。可以為空，代表不做。可以為 -1，代表全做。可以為數字，指定db。
	typedef boost::mpl::int_<2> DbIndexType;
	
	/// @brief DbDepthTypeFor3Or5Prime identifying the dp depth indicating the 3 or 5 prime information
	typedef boost::mpl::int_<3> DbDepthTypeFor3Or5Prime;
	
	/// @brief DbDepthTypeForName identifying the dp depth indicating the name of the target small silencing RNA name information
	typedef boost::mpl::int_<4> DbDepthTypeForName;

	/// @brief DbDepthTypeForName identifying the name indicating 5 Prime species
	typedef boost::mpl::int_<5> Db5PrimeName;

	/// @brief DbDepthTypeForName identifying the name indicating 3 Prime species
	typedef boost::mpl::int_<6> Db3PrimeName;
	
	typedef boost::mpl::int_<7> Printer;
	/*	//not sure whether we need to leave string for naming & 3p'/5p' assigning interface
	/// @brief DbTypeFor3Or5Prime
	typedef boost::mpl::int_<5> DbTypeFor3or5Prime;
	
	
	/// @brief DbTypeForName
	typedef boost::mpl::int_<6> DbTypeForName;
	*/

	typedef boost::mpl::int_<-1> ReturnType;
};



/**
 * @brief Analyzer 的實作，此為計算Heterogeneity。
 * @tparam INPUT_TYPE 輸入資料的型別，一定為 vector，通常為 vector<AnnoRawBed>
 * @tparam ANALYZER_TYPELIST Analyzer要用的參數設定，通常為 boost::mpl::vector<boost::mpl::map<boost::mpl::pair<KEY, VALUE> > >
 * @tparam ANALYZER_TYPE 特化 Analyzer用的參數，此為Heterogeneity特化
 */
template<class INPUT_TYPE, class ANALYZER_TYPELIST>
class AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::Heterogeneity>
{
public:
	/// @brief 此為要使用的 analyzer parameter
	typedef AnalyzerParameter <AnalyzerTypes::Heterogeneity> AnaPara;
	
	/// @brief FilterType 決定Filter後，此analyzer 要取那個Tag，-1=>全，1=>去掉filter tag=1，0=>卻掉filter tag=0。每一個AnalyzerParameter都應該要有此參數
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::FilterType, boost::mpl::int_<1> >::type FilterType;
	
	/// @brief DbIndexType 決定annotation 的 db。只可以為數字，表指定db。不可以為負值, 不可以為空。
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::DbIndexType, boost::mpl::int_<0> >::type DbIndexType;
	
	/// @brief DbDepthTypeFor3Or5Prime 決定第N個 annotation。只可以為數字，指定第N個 annotation對應至3/5 p' information。不可以為負值, 不可以為空。
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::DbDepthTypeFor3Or5Prime, boost::mpl::int_<0> >::type DbDepthTypeFor3Or5Prime;

	/// @brief DbDepthTypeForName 決定第N個 annotation。只可以為數字，指定第N個 annotation對應至name information。不可以為負值, 不可以為空。
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::DbDepthTypeForName, boost::mpl::int_<1> >::type DbDepthTypeForName;
	
	/// @brief DbDepthTypeForName identifying the name indicating 5 Prime species
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::Db5PrimeName, boost::mpl::string<'5P'> >::type Db5PrimeName;

	/// @brief DbDepthTypeForName identifying the name indicating 3 Prime species
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::Db3PrimeName, boost::mpl::string<'3P'> >::type Db3PrimeName;

	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::ReturnType >::type ReturnType;


	/// @brief 記錄此run為第N個使用者要求的length distribution，因為輸出資料節過為 vector<map<...>>
	int this_analyzer_count_;
	
	/// @brief 輸入的資料
	INPUT_TYPE &in;
	
	//map [ tuple < filter, db_index, mirnaName, encoded int for H/T/3p/5p> ][start/end][count]
	typedef std::map < std::tuple<int, int, std::string, int>, std::map <uint32_t, int> > ScanType ;

	/// @brief 輸出的資料
	ScanType out_set_;
	
	/** 
	 * @brief 輸出的資料型別, 因為每個run為mt，所以最後要整合進一個 global 的資料結構
	 * \n output map { tuple <filter, db_idx, string>, tuple< 5P_Head, 5P_Tail, 3P_Head, 3P_Tail >} 
	 */
	//typedef std::map < std::tuple<int, int, std::string>, std::tuple<double, double, double, double> > OutPutType ;
	//can't figure out which double to fill in
	typedef std::map < std::tuple<int, int, std::string>, std::vector<double> > OutPutType ;

	static std::vector<OutPutType> gOutSetHeterogeneity_;
	
	static std::mutex gOutMutexHeterogeneity_;
	
	/// @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::Heterogeneity>
	/// @brief AnalyzerImpl 建構子
	AnalyzerImpl(INPUT_TYPE &i)
		: in (i), this_analyzer_count_(0)
	{}
	/// @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::Heterogeneity>
	/// @brief AnalyzerImpl 建構子
	AnalyzerImpl()
	{}
	
	/**
	 * @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::Heterogeneity>
	 * @brief 主要執行Analyzer的對外介面
	 * @param this_analyzer_count 第N個 使用者要求的length distribution，因為輸出資料節過為 vector<map<...>>
	 * @return void
	 */
	void* operator()(int this_analyzer_count, size_t pipe_index, bool eof_flag, int barcode_index=0)
	{
		this_analyzer_count_ = this_analyzer_count;
		Analysis (in, FilterType::value, DbIndexType::value, DbDepthTypeFor3Or5Prime::value, 
DbDepthTypeForName::value);
		{
			std::lock_guard<std::mutex> lock(gOutMutexHeterogeneity_);
			gOutSetHeterogeneity_.resize(this_analyzer_count+1);	
			MapSum(gOutSetHeterogeneity_[this_analyzer_count], out_set_);
		}
		
		for (auto Q=0; Q!=gOutSetHeterogeneity_.size(); ++Q)
		{
			size_t iii=0;
			for (auto q=gOutSetHeterogeneity_[Q].begin(); q!=gOutSetHeterogeneity_[Q].end(); ++q)
			{
				std::cerr<<"==============round "<<iii<<"======================"<<'\n';
				std::cerr<<"Key: "<<'\n';
				TupleUtility<std::tuple<int, int, std::string>, 3>::PrintTuple(std::cerr, q->first);
				std::cerr<<"value: "<<'\n';
				for (auto& Q: q->second)
					std::cerr<<Q<<'\t';
				std::cerr<<"===================================="<<'\n';
				++iii;
			}
		}

if (eof_flag)
	gOutSetHeterogeneity_.clear();
		return (void*) &in;//NULL;
	}
	
	
	/**
	 * @brief Analysis在run時，所呼叫的function，主要是判斷此 read 是不是要記錄
	 * @param is_filter Filter後 read(anno_rawbed)內部所記錄的 filter tag
	 * @param set_filter 使用者參數所設定的 filter tag (要filter 哪個 tag)，配合 is_filter 來決定 read 是否納入計算。-1 表示全部納入計算，0 表示去掉 tag=0，1表示去掉tag=1
	 * @return bool 回傳是否 filter掉（不納入計算）
	 */
	inline bool IsFilter(const int is_filter, const int set_filter)
	{
		if(set_filter == -1)
		{
			return false;
		}
		else
		{
			if(is_filter == set_filter)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	
	/**
	 * @brief Analysis實作，對 read做計算，記入進 output
	 * @param in 輸入的資料
	 * @param filter 使用者定的 filter 參數，詳情請看上面的 typedef
	 * @param db_index 使用者定的 db_index 參數，詳情請看上面的 typedef
	 * @param db_depth_for_prime 使用者定的 db_depth 參數，詳情請看上面的 typedef
	 * @param db_depth_for_name 使用者定的 db_depth_name 參數，詳情請看上面的 typedef
	 * @return void
	 */
	void Analysis(INPUT_TYPE in, const int filter, const int db_index, const int db_depth_for_prime, const int db_depth_for_name)
	{
		for(auto &anno_rawbed : *in)
		{
//			CalOutput< decltype(anno_rawbed)>(anno_rawbed,-1, db_index, db_depth_for_prime, db_depth_for_name);

			//沒有被 Filter掉
			//if(IsFilter(anno_rawbed.is_filtered_, filter))
			//	CalOutput< decltype(anno_rawbed)>(anno_rawbed, 0, db_index, db_depth_for_prime, db_depth_for_name);

			//有被 Filter掉
			if(!IsFilter(anno_rawbed.is_filtered_, filter))
				CalOutput< decltype(anno_rawbed)>(anno_rawbed, 1, db_index, db_depth_for_prime, db_depth_for_name);
		}
	}
	
	/**
	 * @brief Analysis在run時，所呼叫的function，主要是簡化重複程式碼，此部份主要負責 insert into map of output
	 * @tparam READ_TYPE 自動決定 anno rawbed data type
	 * @param anno_rawbed read 的資料結構，詳情請看 annotation_raw_bed.hpp
	 * @param filter 使用者定的 filter 參數，詳情請看上面的 typedef
	 * @param db_index 使用者定的 db_index 參數，詳情請看上面的 typedef
	 * @param db_depth_for_prime 使用者定的 db_depth 參數，詳情請看上面的 typedef
	 * @param db_depth_for_name 使用者定的 db_depth_name 參數，詳情請看上面的 typedef
	 * @return void
	 */
	template<class READ_TYPE>
	inline void CalOutput (READ_TYPE &anno_rawbed, const int filter, const int db_index, const int db_depth_for_prime, const int db_depth_for_name)
	{
		if (anno_rawbed.annotation_info_.size()<=db_index)
			return;
		else if (anno_rawbed.annotation_info_[db_index].size() <=db_depth_for_prime || 
				 anno_rawbed.annotation_info_[db_index].size() <=db_depth_for_name)
			return;

		if (anno_rawbed.annotation_info_[db_index][db_depth_for_prime] == boost::mpl::c_str<Db5PrimeName>::value)
		{
	//map [ tuple<filter,encoded int for H/T/3p/5p, std::string for name>][start/end][count]
			++out_set_[std::make_tuple (filter, db_index, anno_rawbed.annotation_info_[db_index][db_depth_for_name], 
				HeterogeneityClusterCoding::FivePrimeHead) ]
					[anno_rawbed.start_];
			++out_set_[std::make_tuple (filter, db_index, anno_rawbed.annotation_info_[db_index][db_depth_for_name], 
				HeterogeneityClusterCoding::FivePrimeTail) ]
					[anno_rawbed.end_];
		}
		else if (anno_rawbed.annotation_info_[db_index][db_depth_for_prime] == boost::mpl::c_str<Db3PrimeName>::value)
		{
			++out_set_[std::make_tuple (filter, db_index, anno_rawbed.annotation_info_[db_index][db_depth_for_name], 
				HeterogeneityClusterCoding::ThreePrimeHead) ]
					[anno_rawbed.start_];
			++out_set_[std::make_tuple (filter, db_index, anno_rawbed.annotation_info_[db_index][db_depth_for_name], 
				HeterogeneityClusterCoding::ThreePrimeTail) ]
					[anno_rawbed.end_];
		}
		else
			;//std::cerr<<"calloutput fail for current anno_rawbed "<<'\n';
	}

	void MapSum (OutPutType &map_sum, ScanType &map_item)
	{
		// map[tuple < filter, db_index, miRNA_name, heterogeneityclustercoding > ][start/end]
		for ( auto& map_item_pair : map_item )
		{
			int sum=0, most_abundant_count=0;
			uint32_t most_abundant_start_or_end=0;
			auto& map_item_pair_item = map_item_pair.second;
			// map[start/end]
			for ( auto& map_item_pair_item_pair : map_item_pair_item )
			{
				sum += map_item_pair_item_pair.second;
				if (most_abundant_count < map_item_pair_item_pair.second)
				{
					most_abundant_start_or_end = map_item_pair_item_pair.first;
					most_abundant_count = map_item_pair_item_pair.second;
				}
			}
			int hetero_amount=0;
			for ( auto& map_item_pair_item_pair : map_item_pair_item )
			{
				//ignore reads satisfy condition of abs (the most_abundant_start_or_end - start/end) > 10 
				if (std::abs (most_abundant_start_or_end - map_item_pair_item_pair.first) > 10 )
					continue;
				hetero_amount += (most_abundant_start_or_end - map_item_pair_item_pair.first) * map_item_pair_item_pair.second;
			}
			//std::get< std::get<3>(map_item_pair.first) >
			auto key_value = std::make_tuple ( 
                            std::get<0>(map_item_pair.first),
                            std::get<1>(map_item_pair.first),
                            std::get<2>(map_item_pair.first));
			map_sum[key_value].resize(4);
			map_sum[key_value][std::get<3>(map_item_pair.first)] = hetero_amount / (sum-most_abundant_count);
		}
	}
};

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::vector< std::map < std::tuple<int, int, std::string>, std::vector<double> > > 
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::Heterogeneity>::gOutSetHeterogeneity_;

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::mutex
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::Heterogeneity>::gOutMutexHeterogeneity_;

#endif
