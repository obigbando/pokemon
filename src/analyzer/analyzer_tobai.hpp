/**                                                                                                                                                                                     
 *  @file analyzer_tobai.hpp
 *  @brief achieve bai file written for the analyzer 
 *  @author C-Salt Corp.
 */
#ifndef ANALYZER_TOBAI_HPP_
#define ANALYZER_TOBAI_HPP_
#include <algorithm>
#include <fstream>
#include <iostream>
#include <memory>
#include <map>
#include <tuple>
#include <utility>
#include <vector>
#include "../iohandler/iohandler.hpp"
#include "../iohandler/basespace_def.hpp"
//#include "../file_writer/basespace_setting.hpp"
/**
 * @brief Class handling bai file written
 */
class ToBai
{
   	typedef std::map <std::string, std::tuple <std::pair<int, int>, std::map < std::pair<std::string, int>, std::tuple <int, int, int, int> > > > BamIndexType;
	std::map <int, BamIndexType>* index_ptr_;
	std::vector <int> carryin_bin_;// ({0, 9, 73, 585, 4681, 37450});
	int barcode_index_;
	std::map < std::string, 	//file_name	item1
		std::map < std::string, 	//chromosome	item2
			std::map < int, 	//bin	item3
				std::vector < std::tuple <int, int, int, int> > > > > // min_coffset, max_coffset, min_uoffset, max_uoffset
					bai_bin_;

	std::map < std::string, 	//file_name 
		std::map < std::string, 	//chromosome
			std::map <int,	//tile
				std::tuple <uint64_t, int, int> > > > //ioffset, coffset, uoffset
					bai_intv_;	

public:
	ToBai ( std::map <int, BamIndexType>* indexptr//, int index
			, std::string& file_path, int barcode_in )
			: index_ptr_ (indexptr)
			, carryin_bin_ ({0, 9, 73, 585, 4681, 37450})	
			, barcode_index_ (barcode_in)
		{
			GetBinInfo();
//			PrintBinInfo(file_path);
			GetIndexInfo();
//			PrintIndexInfo(file_path);
			WriteBaiFw(file_path);
//			WriteBaiFile(file_path);
		}

	void PrintBinInfo (std::string& file_path)
	{
		std::stringstream ss;
		ss << file_path << "bin_info";
		std::ofstream bininfo (ss.str());
		for (auto& item1 : bai_bin_)
		{
			bininfo<<"file_name "<<item1.first<<'\n';
			for (auto& item2 : item1.second)
			{
				bininfo<<"=========chr "<<item2.first<<"=========\n";
				for (auto& item3 : item2.second)
				{
					bininfo<<"#bin "<<item3.first<<'\n';
					for (auto& yy : item3.second)
					{
						bininfo<<"original: "<<std::get<0>(yy)<<'\t'<<std::get<1>(yy)<<'\t'<< std::get<2>(yy)<<'\t'<< std::get<3>(yy)<<'\n';
						bininfo<<"chunk beg_ "<<(size_t) (std::get<0>(yy)*pow(2,16)+std::get<2>(yy))<<'\n';
						bininfo<<"chunk end_ "<<(size_t) (std::get<1>(yy)*pow(2,16)+std::get<3>(yy))<<'\n';
						bininfo<<"min coffset & uoffset "<<std::get<0>(yy)<<'\t'<<std::get<2>(yy)<<'\n';
						bininfo<<"max coffset & uoffset "<<std::get<1>(yy)<<'\t'<<std::get<3>(yy)<<'\n';
					}
				}
			}
		}
		bininfo.close();
	}

	void PrintIndexInfo (std::string& file_path)
	{
		std::stringstream ss;
		ss << file_path << "index_info";
		std::ofstream indexinfo (ss.str());
		for (auto& item1 : bai_intv_)
		{
			indexinfo<<"file_name "<<item1.first<<'\n';
			for (auto& item2 : item1.second)
			{
				indexinfo<<"---------chromosome_ "<<item2.first<<"---------\n";
				for (auto& item3 : item2.second)
					indexinfo<<"tile "<<item3.first<<" : "<<std::get<0>(item3.second)<<'\t'<<std::get<1>(item3.second)<<'\t'<<std::get<2>(item3.second)<<'\n';
			}
		}
	}

private:
	int reg2bins(int beg, int end, std::vector <int>& list)/* calculate the list of bins that may overlap with region [beg,end) (zero-based) */
	{   
		int i = 0, k;
		--end;
		list.push_back (0);//[i++] = 0;
		for (k = 1 + (beg>>26); k <= 1 + (end>>26); ++k) list.push_back (k);//[i++] = k;
		for (k = 9 + (beg>>23); k <= 9 + (end>>23); ++k) list.push_back (k);//[i++] = k;
		for (k = 73 + (beg>>20); k <= 73 + (end>>20); ++k) list.push_back (k);//[i++] = k;
		for (k = 585 + (beg>>17); k <= 585 + (end>>17); ++k) list.push_back (k);//[i++] = k;
		for (k = 4681 + (beg>>14); k <= 4681 + (end>>14); ++k) list.push_back (k);//[i++] = k;
		return i;
	}

	void GetBinInfo (void)
	{
		for (auto& item1 : *index_ptr_)
		{
			for (auto& item2 : item1.second)
			{
				for (auto& item3 : std::get<1>(item2.second) )
				{
					std::vector <int> QQ;
					reg2bins (std::get<2>(item3.second), std::get<3>(item3.second), QQ );
					for (auto& yy : QQ)
					{
						if (bai_bin_[item2.first][item3.first.first][yy].size()==0)
						{
							bai_bin_[item2.first][item3.first.first][yy].resize (1);
							std::get<0> (bai_bin_[item2.first][item3.first.first][yy][0]) = (std::get<0>(item2.second)).first;//min_coffset
							std::get<2> (bai_bin_[item2.first][item3.first.first][yy][0]) = std::get<0>(item3.second);//min_uoffset
//						}
//						else
//						{
							std::get<1> (bai_bin_[item2.first][item3.first.first][yy][0]) = (std::get<0>(item2.second)).first;//max_coffset
							std::get<3> (bai_bin_[item2.first][item3.first.first][yy][0]) = std::get<1>(item3.second);//max_uoffset
						}
					}
				}
			}
		}
		for (auto& item1 : bai_bin_)
		{
			for (auto& item2 : item1.second)
			{
				for (auto& item3 : item2.second)
				{
					if ( std::find (carryin_bin_.begin(), carryin_bin_.end(), item3.first)==carryin_bin_.end()
							&& item2.second.find (item3.first+1)!=item2.second.end() )
					{
						std::get<1> (item3.second[0] ) = 
							std::get<0> (item1.second[(item2.first)][(item3.first)+1][0]);
						std::get<3> (item3.second[0] ) = 
							std::get<2> (item1.second[(item2.first)][(item3.first)+1][0]);
					}
					else if (std::find ( carryin_bin_.begin(), carryin_bin_.end(), (item3.first)) !=carryin_bin_.end() )
					{
						std::get<1> (item3.second[0] ) = 
						std::get<0>((*index_ptr_)[index_ptr_->rbegin()->first][item1.first]).first + std::get<0>((*index_ptr_)[index_ptr_->rbegin()->first][item1.first]).second - 28;
						std::get<3> (item3.second[0] ) = 0;
					}
				}
			}
			for (auto& item2 : item1.second)	
			// add last bin #37450 content, which always includes two bins, 
			// wherein the 1st bin having content corresponding to bin #0 and the 2nd bin having zero content
			{
				item2.second[37450].push_back (
						std::make_tuple (std::get<0>(item2.second[0][0]), 
							std::get<1>(item2.second[0][0]), 
							std::get<2>(item2.second[0][0]), 
							std::get<3>(item2.second[0][0])));
				item2.second[37450].push_back (std::make_tuple (0, 0, 0, 0));
			}
		}
	}

	void GetIndexInfo(void)
	{
		for (auto& item1 : *index_ptr_)
			for (auto& item2 : item1.second)
				for (auto& item3 : std::get<1>(item2.second) )
				{
					bai_intv_[item2.first][item3.first.first][0] = std::make_tuple ( 0, 0, 0 );
					bai_intv_[item2.first][item3.first.first].insert ({item3.first.second, std::make_tuple (
							((((uint64_t)std::get<0>(item2.second).first) << 16) + std::get<0>(item3.second)), 
							std::get<0>(item2.second).first, 
							std::get<0>(item3.second) )});
				}
		for (auto& item1 : bai_intv_)
		// filling missing tile elements into bai_intv_[file_name][chromosome] 
		{
			for (auto& item2 : item1.second)
			{
				for (auto ii=++item2.second.begin(); ii!=item2.second.end(); ++ii)
				{
					int tail = ii->first;
					int head = (--ii)->first;
					++ii;
					for (auto idx=head+1; idx!=tail; ++idx)
						bai_intv_[item1.first][item2.first][idx] = bai_intv_[item1.first][item2.first][head];
				}
			}
		}
	}

	void WriteBaiFw (std::string& file_path)
	{
//		std::vector <std::shared_ptr <std::ofstream> > fvec;
		std::vector <std::shared_ptr <IoHandlerBaseSpace> > fvec;
		for (auto& item0 : bai_bin_)
		{
			//std::stringstream ss;
			//ss << file_path << item0.first << ".bai";
//			fvec.push_back (std::make_shared<std::ofstream> (ss.str().c_str(), std::ofstream::binary) );
		    DeviceParameter file_writter_parameter (PipelinePreparator<>::gDeviceParameter_);
			file_writter_parameter.bs_file_path_="files?name="+item0.first+".bai&";//text.txt&"
//			file_writter_parameter.bs_dir_path_ = "directory=output/sample-"+std::to_string(barcode_index_)+"/bam/&";
			file_writter_parameter.bs_dir_path_ = "directory=output/sample-"+PipelinePreparator<>::gBarcode_vector_[barcode_index_]+"/bam/&";
			file_writter_parameter.bs_file_open_url_ =
				file_writter_parameter.bs_basic_url_ + file_writter_parameter.bs_version_ +
				file_writter_parameter.bs_app_result_ + file_writter_parameter.bs_file_path_ +
				file_writter_parameter.bs_dir_path_ + file_writter_parameter.bs_tailing_str_;
			std::cerr<<"sorted BAI bs_file_open_url_\t"<<file_writter_parameter.bs_file_open_url_<<'\n';

			fvec.push_back (std::make_shared<IoHandlerBaseSpace> (file_writter_parameter) );

			char* magic = "BAI\1";
			int32_t n_ref = PipelinePreparator<>::gChromosome_vector_.size();//item0.second.size();	
			fvec.back()->write(magic, 4);
			fvec.back()->write(reinterpret_cast<const char *>(&n_ref), sizeof(n_ref));
			int iiii=0;

			for (auto& item1 : PipelinePreparator<>::gChromosome_vector_)
			{
				int32_t n_bin = item0.second[item1].size();
				fvec.back()->write(reinterpret_cast<const char *>(&n_bin), sizeof(n_bin));
				for (auto& item2 : item0.second[item1])//item1.second)
				{
					uint32_t bin = item2.first;
					int32_t n_chunk = item2.second.size();
					fvec.back()->write (reinterpret_cast<const char *>(&bin), sizeof(bin));
					fvec.back()->write (reinterpret_cast<const char *>(&n_chunk), sizeof(n_chunk));

					for (auto &item3: item2.second)
					{
						uint64_t chunk_beg = (uint64_t)(std::get<0>(item3)*pow(2,16)+std::get<2>(item3));
						uint64_t chunk_end = (uint64_t)(std::get<1>(item3)*pow(2,16)+std::get<3>(item3));
						fvec.back()->write (reinterpret_cast<const char *>(&chunk_beg), sizeof(chunk_beg));
						fvec.back()->write (reinterpret_cast<const char *>(&chunk_end), sizeof(chunk_end));
					}
				}
				int32_t n_intv = bai_intv_[item0.first][item1].size();
				fvec.back()->write (reinterpret_cast<const char *>(&n_intv), sizeof(n_intv));

				for (auto& qq : bai_intv_[item0.first][item1])
				{
					uint64_t ioffset = std::get<0>(qq.second);
					fvec.back()->write (reinterpret_cast<const char *>(&ioffset), sizeof(ioffset));
				}	
				++iiii;
			}
			fvec.back()->bs_async_close();
		}
		for (auto& q : fvec)
			q->close();
	}

	void WriteBaiFile (std::string& file_path)
	{
		std::vector <std::shared_ptr <std::ofstream> > fvec;
		for (auto& item0 : bai_bin_)
		{
			std::stringstream ss;
			ss << file_path << item0.first << ".bai";

			fvec.push_back (std::make_shared<std::ofstream> (ss.str().c_str(), std::ofstream::binary) );

			char* magic = "BAI\1";
			int32_t n_ref = PipelinePreparator<>::gChromosome_vector_.size();//item0.second.size();	
			fvec.back()->write(magic, 4);
			fvec.back()->write(reinterpret_cast<const char *>(&n_ref), sizeof(n_ref));
			int iiii=0;

			for (auto& item1 : PipelinePreparator<>::gChromosome_vector_)
			{
				int32_t n_bin = item0.second[item1].size();
				fvec.back()->write(reinterpret_cast<const char *>(&n_bin), sizeof(n_bin));
				for (auto& item2 : item0.second[item1])//item1.second)
				{
					uint32_t bin = item2.first;
					int32_t n_chunk = item2.second.size();
					fvec.back()->write (reinterpret_cast<const char *>(&bin), sizeof(bin));
					fvec.back()->write (reinterpret_cast<const char *>(&n_chunk), sizeof(n_chunk));

					for (auto &item3: item2.second)
					{
						uint64_t chunk_beg = (uint64_t)(std::get<0>(item3)*pow(2,16)+std::get<2>(item3));
						uint64_t chunk_end = (uint64_t)(std::get<1>(item3)*pow(2,16)+std::get<3>(item3));
						fvec.back()->write (reinterpret_cast<const char *>(&chunk_beg), sizeof(chunk_beg));
						fvec.back()->write (reinterpret_cast<const char *>(&chunk_end), sizeof(chunk_end));
					}
				}
				int32_t n_intv = bai_intv_[item0.first][item1].size();
				fvec.back()->write (reinterpret_cast<const char *>(&n_intv), sizeof(n_intv));

				for (auto& qq : bai_intv_[item0.first][item1])
				{
					uint64_t ioffset = std::get<0>(qq.second);
					fvec.back()->write (reinterpret_cast<const char *>(&ioffset), sizeof(ioffset));
				}	
				++iiii;
			}
		}
		for (auto& q : fvec)
			q->close();
	}

};

#endif
