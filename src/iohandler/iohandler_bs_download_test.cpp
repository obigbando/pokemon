#include <iostream>
#include <sstream>
#include <algorithm>// copy, min
#include <iosfwd>            
#include <string>// streamsize
#include <cassert>
#include <ios>// ios_base::beg
#include <string>
#include <boost/iostreams/categories.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/positioning.hpp>
#include "iohandler.hpp"
#include "gtest/gtest.h"
#include "../format/fastq.hpp"

typedef boost::mpl::map
<
	boost::mpl::pair< IoHandlerGlobalParameter::FilteringStreamType, boost::iostreams::filtering_streambuf<boost::iostreams::input> >
	, boost::mpl::pair< IoHandlerGlobalParameter::DeviceParameter, DeviceParameter >
	, boost::mpl::pair< IoHandlerGlobalParameter::MutipleNumber, boost::mpl::int_<2> >
	, boost::mpl::pair< IoHandlerGlobalParameter::DeviceBufferSize, boost::mpl::int_<8> >
	, boost::mpl::pair< IoHandlerGlobalParameter::DevicePushbackSize, boost::mpl::int_<16> >
	, boost::mpl::pair< IoHandlerGlobalParameter::BaseStreamType, std::istream >
> IO_HANDLER_GLOBAL_SETTING;

typedef SharedMemory<IO_HANDLER_GLOBAL_SETTING> SharedMemoryType;

typedef boost::mpl::vector
<
	boost::mpl::map
	<
		boost::mpl::pair< IoHandlerGlobalParameter::DeviceType, boost::mpl::int_<FileDeviceType::GzipDeCompressFilter> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< IoHandlerGlobalParameter::DeviceType, boost::mpl::int_<FileDeviceType::BasespaceDevice_download> >
	>
> IO_HANDLER_LIST;

TEST (basespace, test_with_sizeinfo)
{
    DeviceParameter dp;
	dp.bs_download_url_ = //"http://www.jhhlab.tw/tt";//
"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7";
//"https://api.basespace.illumina.com/v1pre3/files/3360154/content?access_token=bfb36a45cfa747d88b9b5d933c7c0259";
	dp.bs_download_size_ = 7493990;     

    iohandler<IO_HANDLER_GLOBAL_SETTING, IO_HANDLER_LIST>  bb(dp);
//	std::chrono::milliseconds dura ( 2000 );
//	std::this_thread::sleep_for ( dura );

	std::ofstream ffy ("content_withsizeinfo.fq");
	int ii = 1024*1024;//444670062;
	char* ff = new char[ii+1];//1024*1024];
	while(!bb.eof())
	{
		bb.read(ff, ii);//1024*1024);
		ffy.write (ff, bb.gcount());
		//	std::string yy;
		//	std::getline (bb, yy);
		//	bb >> yy;	
	}
	delete ff;
	ffy.close();
    //bb.close();//ff, 1024*1024);
};

TEST (basespace, test1)
{
    DeviceParameter dp;
	dp.bs_download_url_ = //"http://www.jhhlab.tw/tt";//
"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7";
//"https://api.basespace.illumina.com/v1pre3/files/3360154/content?access_token=bfb36a45cfa747d88b9b5d933c7c0259";
	dp.bs_download_size_ = 0;
    
    iohandler<IO_HANDLER_GLOBAL_SETTING, IO_HANDLER_LIST>  bb(dp);
//	std::chrono::milliseconds dura ( 2000 );
//	std::this_thread::sleep_for ( dura );

	std::ofstream ffy ("content.fq");
	int ii = 1024*1024;//444670062;
	char* ff = new char[ii+1];//1024*1024];
	while(!bb.eof())
	{
		bb.read(ff, ii);//1024*1024);
		ffy.write (ff, bb.gcount());
		//	std::string yy;
		//	std::getline (bb, yy);
		//	bb >> yy;	
	}
	delete ff;
	ffy.close();
    //bb.close();//ff, 1024*1024);
};


