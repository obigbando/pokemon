#include <iostream>
#include <sstream>
#include <algorithm>// copy, min
#include <iosfwd>            
#include <string>// streamsize
#include <cassert>
#include <ios>// ios_base::beg
#include <string>
#include <boost/iostreams/categories.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/positioning.hpp>
#include "ihandler_impl.hpp"
#include "gtest/gtest.h"
#include "../../format/fastq.hpp"

typedef boost::mpl::map
<
    boost::mpl::pair< IoHandlerGlobalParameter::FilteringStreamType, boost::iostreams::filtering_streambuf<boost::iostreams::input> >
    , boost::mpl::pair< IoHandlerGlobalParameter::DeviceParameter, DeviceParameter >
    , boost::mpl::pair< IoHandlerGlobalParameter::BaseStreamType, std::istream >

> GLOBAL_SETTING;

typedef boost::mpl::vector
<   
    boost::mpl::map
    <  
        boost::mpl::pair< IoHandlerGlobalParameter::DeviceType, boost::mpl::int_<FileDeviceType::Ifstream> >
    >
> LIST;

typedef iohandler<GLOBAL_SETTING, LIST> IoHandlerIfstream;




TEST (file_reader_impl, fastq_1)
{
	typedef std::tuple <std::string, std::string, std::string, std::string> TUPLETYPE;
	std::vector<std::string> url_vec ({
	"/home/oman/work/pokemon/src/iohandler/ihandler/test.fq"//"test.fq"
	});

	IHandler_impl <Fastq, TUPLETYPE, IoHandlerIfstream> IHandler (url_vec); 
	auto fq = IHandler.get_next_entry(0);
	std::cerr<<fq;
}

/*
TEST (file_reader_impl_2, test1)
{
	typedef std::tuple <std::string, std::string, std::string, std::string> TUPLETYPE;

	std::vector<std::string> url_vec ({
	"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7",
	"https://api.basespace.illumina.com/v1pre3/files/3360154/content?access_token=bfb36a45cfa747d88b9b5d933c7c0259"
	});
	std::vector<uint64_t> size_vec ({7493990, 444670062});

	IHandler_impl <Fastq, TUPLETYPE, IoHandlerBaseSpaceDownload> IHandler (url_vec, size_vec); 

	std::ofstream ffy ("content.fq");
	while(true)
	{
		auto fq = IHandler.get_next_entry(0);
		if (!fq.eof_flag)
			ffy << fq;
		else
			break;
	}
	ffy.close();
   
	std::ofstream ffy2 ("content2.fq");
	while(true)
	{
		auto fq = IHandler.get_next_entry(1);
		if (!fq.eof_flag)
			ffy2 << fq;
		else
			break;
	}
	ffy2.close();
};
*/

