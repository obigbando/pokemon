#include <iostream>
#include "parser.hpp"
#include <cstdint>
#include <string>
#include <tuple>
using namespace std;

int main(int argc, char* argv[])
{
	//fasta
	//Parser<format_types::FASTA> p1;
	FileParser< Bed<std::tuple <std::string, uint32_t, uint32_t> >, Plain> p1 (argv[1]);
	p1.read_file();

//	cout << "pa[0] = " << p1[0] << endl;
	
	//fastq
	/*
	string file_path2;
	file_path2 = "fastq.txt";
	Parser<format_types::FASTQ> p2;
	//Parser< Format<format_types::FASTA> > p1;
	p2.read_file(file_path2);
	*/
	return 0;
}
