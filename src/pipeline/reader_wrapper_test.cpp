#include "reader_wrapper.hpp"
#include <fstream>
#include <iostream>

typedef std::tuple <std::string, std::string, std::string, std::string> TUPLETYPE;

int main (void)
{
	std::vector< std::vector <std::string> > url_vec ({ 
	{"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7"}, 
	{"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7"},
//	{"https://api.basespace.illumina.com/v1pre3/files/3360154/content?access_token=bfb36a45cfa747d88b9b5d933c7c0259"},
	{"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7"}, 
	{"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7"},
	{"https://api.basespace.illumina.com/v1pre3/files/3360154/content?access_token=bfb36a45cfa747d88b9b5d933c7c0259"}
	});
	std::vector <std::vector <uint64_t> > size_vec ({
	{7493990}, {7493990}, //{444670062}, 
	{7493990}, {7493990}, {444670062}
	});

	FileReaderWrapper frw (url_vec, size_vec);

	std::ofstream ff ("test.fq");
	auto ptr = new std::map <int, std::vector< Fastq<TUPLETYPE> > >;
	while (true)
	{
		auto eof = frw.run (ptr, 55661);
		for (auto& q : (*ptr)[0])
			ff << q;
		if (eof)
			break;
	}
	delete ptr;
};
