///@file mpi_pool.hpp
///@brief Provide MPI features, to facilitate multi-process operation. 
///@author C-Salt Corp.
#ifndef MPI_POOL_HPP_
#define MPI_POOL_HPP_
#include <queue>
#include <vector>
#include <string>
#include <tuple>
#include <fstream>
#include <iostream>
#include <boost/serialization/utility.hpp>
#include <boost/mpi.hpp>
#include "format/fasta.hpp"
#include "constant_def.hpp"
#include "tuple_utility.hpp"
/// @class MPIPool
/// @brief Include boost::mpi::environment and boost::mpi::communicator member elements for establishing the MPI operation environment.  MPI_Request element and vector < queue < tuple < MPI_Request, MPI_Status, int > > > are also included for achieving MPI pool manaagement.  
class MPIPool
{
public:
/// @brief boost::mpi::environment element, constructing the MPI operation environment
	boost::mpi::environment env;	//employ boost::mpi tool to construct MPI environment & MPI communicator objects
/// @brief boost::mpi::communicator element, including subelements corresponding to different rank value. \n For example, master process corresponds to world.rank()==0, and slave processes corresponds to world.rank()!=0
	boost::mpi::communicator world;
/// @brief integer for achieing MPIPool management. 
	int job_index,  //job_index keep current job index
/// @brief integer for achieing MPIPool management. 
	running_index;//current_index;	
/// @brief a std vector has a size the same as the number of slave processes.  Each vector::value_type element corresponds to one of the slave processes, and holds non-blocking transmission information, e.g. MPI_Request, MPI_Status, and rank value,  corresponding to that slave process.
	std::vector < std::queue < std::tuple < MPI_Request, MPI_Status, int > > > post_list;	
	//define a post_list data structure, wherein MPI_Request & MPI_Status objects are nneded for wait & test operations of flush and MasterOperate functions
/// @brief an MPI_Rquest object needed for MPI_IRecv operation taking place in MasterOperate member function
	MPI_Request req0[2];	//MPI_Request object, needed for MPI_IRecv operation in MasterOperate function
/// @brief object for achieing MPIPool management. 
	MPI_Status status;
/// @brief Constructor, taking a Num of int format, and accordingly creating that much of processes. \n The element of world is initialized with a default constructed boost::mpi::communicator element.  The element of post_list is initialized with a size of world.size()
	MPIPool (int Num, char* Str[])	//constructor
		: env (Num, Str)
		, world (boost::mpi::communicator())
		, job_index (0)
//		, current_index (0)
		, running_index (1)
		, post_list (world.size()) 
	{}
/// @brief Member function that wait till each and every non-blocking operations proceeds to an end, and accordingly update the post_list data structure.
	void FlushPool (void)	//flush function to wait till each and every pending non-blocking operations proceeds to an end 
	{
	   if (world.rank() == 0)
		{
			for (int k=1; k<world.size(); k++)
			{
				while  (!post_list[k].empty()) 
				{
					MPI_Wait (&std::get<0>(post_list[k].front()), &std::get<1>(post_list[k].front()));
					post_list[k].pop();
				}
			}
		}
		running_index = 1;
		job_index = 0;
	}
/// @brief the main master operation, that employing: \n 1) an non-blocking isend operation to send job_index to a slave process; \n 2) a blocking recv operation to receive buffer size information; \n 3) a non-blocking irecv operation to receive the data sent back by the corresponding slave process; \n 4) a recording operation to keep track of the irecv operation carried out in 3). 
	void MasterOperate (boost::shared_ptr<boost::mpi::packed_iarchive> ptr)	//detail communication operation between master & slaves
	{
		world.isend (running_index, 0, job_index);
		std::size_t count;
		MPI_Recv (&count, 1, boost::mpi::get_mpi_datatype<std::size_t>(count), running_index, 1, world, &status);
		ptr->resize (count);	//resize current packed_iarchive object, so as to accomodate the incoming iarchive sent by slaves
		MPI_Irecv (ptr->address(), ptr->size(), MPI_PACKED, status.MPI_SOURCE, status.MPI_TAG, world, req0+1);
		post_list[running_index].push (std::make_tuple (req0[1], status, job_index));	//keep a operation log in post_list data structure 
		++job_index;
//		current_index = running_index;
		running_index = (running_index+1) % world.size();
		if (running_index == 0)	//bypass running_index == 0 situation, so as to prevent the post_operation from being assigned to the master process
			++running_index;
	}

/*
	template < typename JOB, typename STRUCTURE >	//JobPost function taking an incoming job and a vector of shared_ptr pointing to pack_iarchive object 
	void JobPost (std::vector<JOB>& job_queue, STRUCTURE iarc)//std::vector< boost::shared_ptr<boost::mpi::packed_iarchive> > iarc)
	{
		if (world.rank() == 0)	//master process
		{
			job_num = job_queue.size();
			while (true)
			{
				if ( job_index == job_num )
				{
					int k=1;
					for (int i = 1; i != world.size(); ++i)
					{
						int end_int =-1;
						world.send (i, 0, end_int);
					}
					break;
				}
				if (post_list[running_index].empty())
				{
					MasterOperate (iarc[job_index]);
					continue;
				}
				else
				{
					int flag;	   
					MPI_Test(&std::get<0>(post_list[running_index].front()), &flag, &std::get<1>(post_list[running_index].front()));
					if (flag)	//indicate whether a previous job, represented by the MPI_Request and MPI_Status objects, has been achieved
					{
						post_list[running_index].pop();	//remove previous finished job, from the post_list 
						continue;
					}
					if (!post_list[running_index].empty())
					{
						if ( current_index == running_index )	
						// Situation that the running_index has been incremented through every slaves of the communicator and still can't find an empty queue
						{
								MasterOperate (iarc[job_index]);
								continue;
						}
						else 
						{
  							running_index = (running_index+1) % world.size();
							if (running_index == 0)
								++running_index;
							JobPost (job_queue, iarc);
						}
					}
				}
			}
		}
		else if (world.rank() != 0)	//assign slave job to the slave corresponding to the current running_index
		{
			while (true)
			{
				boost::mpi::packed_oarchive arc (world);
				int job_index;
				world.recv (0, 0, job_index);
				if ( job_index == -1)
					break;
				job_queue[job_index]();
				arc & job_queue[job_index];
				const void* size = &arc.size();
				MPI_Send (const_cast<void*>(size), 1, boost::mpi::get_mpi_datatype<std::size_t>(arc.size()), 0, 1, world);
				// provide size info to the master, so that the master can accordingly reserve a buffer space capable of accomodating the upcomming MPI_PACKED data
				MPI_Send (const_cast<void*>(arc.address()), arc.size(), MPI_PACKED, 0, 1, world);
				running_index = (running_index+1) % world.size();
				if (running_index == 0)
					++running_index;
			}
		}
	}
*/

/// @brief first version of JobPost, achieving a single job posting operation, driving the MPIPool to execute the corresponding job, by calling the MasterOperate function. 
/// @tparam JOB indicating a target job, preferably in functor type.
/// @param job an object of the type of JOB.
/// @return a boost::shared_ptr<boost::mpi::packed_iarchive> holding the return value of the slave process
	template < typename JOB >	//JobPost function taking an incoming job, and return a shared_ptr pointing to the output iarchive object, storing the return data
	boost::shared_ptr<boost::mpi::packed_iarchive> JobPost (JOB& job)
	{
		boost::shared_ptr <boost::mpi::packed_iarchive> arc_ptr ( new boost::mpi::packed_iarchive (world) );
		if (world.rank() == 0)
		{
			while (true)
			{
				if (post_list[running_index].empty())
				{
					MasterOperate (arc_ptr);
					break;
				}
				else
				{
					int flag;	   
					MPI_Test(&std::get<0>(post_list[running_index].front()), &flag, &std::get<1>(post_list[running_index].front()));
					if (flag)
					{
						post_list[running_index].pop();
						continue;
					}
					if (!post_list[running_index].empty())
					{
						//if ( current_index == running_index )
						//{
						//		MasterOperate (arc_ptr);
						//		break;
						//}
						//else 
						//{
  							 running_index = (running_index+1) % world.size();
							if (running_index == 0)
								++running_index;
							JobPost (job);
						//}
					}
				}
			}
			return arc_ptr;
		}
		else if (world.rank() == running_index)
		{
			boost::mpi::packed_oarchive arc (world);
			int job_index;
			world.recv (0, 0, job_index);
			job();
			arc & job;
			const void* size = &arc.size();
			MPI_Send (const_cast<void*>(size), 1, boost::mpi::get_mpi_datatype<std::size_t>(arc.size()), 0, 1, world);
			MPI_Send (const_cast<void*>(arc.address()), arc.size(), MPI_PACKED, 0, 1, world);
			running_index = (running_index+1) % world.size();
			if (running_index == 0)
				++running_index;
		}
		else
		{
			running_index = (running_index+1) % world.size();
			if (running_index == 0)
				++running_index;
		}
	}

/// @brief second version of JobPost, achieving a single job posting operation, driving the MPIPool to execute the corresponding job, by calling the MasterOperate function. 
/// @tparam JOB indicating a target job, preferably in functor type.
/// @param job an object of the type of JOB.
/// @param iarc a boost::shared_ptr<boost::mpi::packed_iarchive> holding the return value of the slave process
	template < typename JOB >	
	//JobPost function taking an incoming job and a STRUCTURE, designed as a shared_ptr pointing to the output iarchive object, storing the return data
	void JobPost (JOB& job, boost::shared_ptr<boost::mpi::packed_iarchive>& iarc)
	{
		iarc = JobPost ( job );
	}
};

#endif
