#include <iostream>
#include <fstream>
#include <algorithm>
#include <cstdint>
#include <cmath>
#include <array>
#include <string>
#include <unordered_map>
#include <map>
#include <set>
#include <deque>
#include <random>
#include "gtest/gtest.h"
#include "boost/unordered_map.hpp" 
// dynamic solotion 
template <size_t MISMATCH_COUNT, size_t MATCH_COUNT>
class FAAST
{
public:
	std::string pattern;
	size_t m, n, length;
	std::string alphabet;
//	std::map<size_t, boost::unordered_map<char, std::set<size_t> > >Ukx_map;
//	boost::unordered_map<size_t, boost::unordered_map<std::string, size_t > >Vkx_map;
//	std::map<std::string, size_t> Dkx_map;
	boost::unordered_map<size_t, boost::unordered_map<int, std::set<size_t> > >Ukx_map;
	boost::unordered_map<size_t, boost::unordered_map<int, size_t > >Vkx_map;
	mutable std::vector<std::pair<int, std::string> > cvec;
	mutable boost::unordered_map <char, int> reverse_alphabet;

public:
	FAAST (std::string& str_pattern)//, int i)
		: pattern (str_pattern)//, txt (str_txt)
		, m (pattern.size())//, n (txt.size())
		, length (MISMATCH_COUNT + MATCH_COUNT)
		, alphabet //("ACGT")
					("ACGNT")
	{
		TableProcess();
	}

	FAAST (std::string& str_pattern, int i)
		: pattern (str_pattern)//, txt (str_txt)
		, m (pattern.size())//, n (txt.size())
		, length (MISMATCH_COUNT + MATCH_COUNT)
		, alphabet //("ACGT")
					("ACGNT")
	{
	}

	void TableProcess (void)
	{
//count_vec generation
		std::vector<std::string> count_vec;
		pattern_generate (count_vec);

//reverse_alphabet generation
//		std::map <char, int> reverse_alphabet;
		int index=0;
		std::for_each (alphabet.begin(), alphabet.end(),
		[&] (const char& q)
		{ reverse_alphabet.insert (std::make_pair (q, index));
			++index; });

//Ukx
		for (size_t a=0; a!=alphabet.size(); ++a)
		{
			for (auto i=m; i>=m-MISMATCH_COUNT-MATCH_COUNT+1; --i)
		  		Ukx_map[i][a].insert(m-MISMATCH_COUNT);
 		}

		for (size_t i=m; i>=m-MISMATCH_COUNT-MATCH_COUNT+1; --i)
		{
			for (size_t s=i-1; s>=1; --s)
			{
				if (i-s < m-MISMATCH_COUNT)
					Ukx_map[i][reverse_alphabet[pattern[s]]].insert (i-s);
			}	
		}

//cvec generation
		size_t vecsize = pow(16, MISMATCH_COUNT+MATCH_COUNT+1);
		cvec.resize (vecsize);
		std::for_each (count_vec.begin(), count_vec.end(),
		[&] (const std::string& q)
		{
			int result=0;
			for (auto i=0; i!=q.size(); ++i)
//				result+=reverse_alphabet[q[i]] *pow(alphabet.size(), i);
				result+= (reverse_alphabet[q[i]] << (i<<2));
			cvec[result]= {-5566, q};
		});

//Vkx & Dkx
		for (auto itr=0; itr!=vecsize; ++itr)//(auto itr=cvec.begin(); itr!=cvec.end(); ++itr)
		{
			if (cvec[itr].first!=-5566)
				continue;
			for (size_t l=1; l<=m-MISMATCH_COUNT; ++l)
				Vkx_map[l][cvec[itr].first] = 0 ; //use insert for instead	//takes 4 secs instead!! 
//				Vkx_map[l].insert (std::make_pair (cvec[itr].first, 0));	//takes 16 secs under FAAST<2,3,> situation; emplace doesn't do any help !!
//				Vkx_map[l].insert ({cvec[itr].first, 0});					//takes 16 secs under FAAST<2,3,> situation; emplace dosen't do any help !!

			for (size_t l=1; l<=m-MISMATCH_COUNT; ++l)// boost::unordered_map<size_t, boost::unordered_map<std::string, size_t > >Vkx_map;
			{
				for (size_t i=m; i>=m-MISMATCH_COUNT-MATCH_COUNT+1; --i)
				{
					if (Ukx_map[i][reverse_alphabet[cvec[itr].second[MISMATCH_COUNT+MATCH_COUNT-1-m+i]]].find (l) != 
							Ukx_map[i][reverse_alphabet[cvec[itr].second[MISMATCH_COUNT+MATCH_COUNT-1-m+i]]].end())
						Vkx_map[l][cvec[itr].first]+=1;
				}

				if (Vkx_map[l][cvec[itr].first] >= std::min (MATCH_COUNT, m-MISMATCH_COUNT-l))
				{
// found that if we have Dkx_map[*itr]-1 when l>1, then we can have correct answer
					if (l>1)
						cvec[itr].first = l-1;
					else
						cvec[itr].first = l;
					break;
				}
			}
		}
	}

	bool faast_search (std::string& txt, std::vector<size_t>& found_vec)//, int i)
	{
		n = txt.size();
		bool return_value = false;

		size_t j = m ;
		while (j<=n)
		{
			int h = j, i=m, e=0;
			while (i>0 && e<=MISMATCH_COUNT)
			{
				if (txt[h-1]!=pattern[i-1])
					++e;
				--i, --h;
			}
			if (e <= MISMATCH_COUNT)
			{
				found_vec.push_back (j-1);
				return_value = true;
			}
			int result=0;
			for (auto i=0; i!=length; ++i)
//				result+=reverse_alphabet[txt[j-length+i]] *pow(alphabet.size(), i);
				result += (reverse_alphabet[ txt[j-length+i] ] << (i<<2));

			j+=cvec[result].first;
		} 
		return return_value;
	}

	void pattern_generate (std::vector<std::string>& count_vec)
	{	
		count_vec.reserve (pow(alphabet.size(), MISMATCH_COUNT+MATCH_COUNT));
		std::string r (MISMATCH_COUNT+MATCH_COUNT, '1');
		std::deque<std::string> pp;
		GetCombination (alphabet, 0, r, 0, alphabet.size()-MISMATCH_COUNT-MATCH_COUNT, pp);

		std::string repetitive_str;
		std::for_each (alphabet.begin(), alphabet.end(),
			[&] (const char& q)
			{( repetitive_str += ( std::string (MISMATCH_COUNT+MATCH_COUNT, q) ) ); } );

//std::cerr<<"repetitive_str "<<repetitive_str<<std::endl;
		std::string r2 (MISMATCH_COUNT+MATCH_COUNT, '1');
		GetCombination (repetitive_str, 0, r2, 0, alphabet.size()*(MISMATCH_COUNT+MATCH_COUNT)-MISMATCH_COUNT-MATCH_COUNT, pp);

		std::set<std::string> Q (pp.begin(), pp.end());

		std::for_each (Q.begin(), Q.end(),
		[&] (std::string q)
		{
//std::cerr<<"current combination "<<q<<std::endl;
//size_t i=1;	
			std::sort (q.begin(), q.end());
			count_vec.push_back (q);
			while ( std::next_permutation (q.begin(), q.end()) )
			{
//std::cerr<<i<<"th permutation "<<q<<std::endl;
//++i;
				count_vec.push_back (q);
			}
		});
	}

	void GetCombination (std::string target_str, size_t target_column, std::string result, size_t result_column, int loop, std::deque<std::string>& container)
	{
		int r_size = result.size();
		int localloop = loop;
		int local_target_column = target_column;
	
		//A different combination is out
		if (result_column > r_size-1)//(r_column>(r_size-1))
		{
			container.push_back (result);
			return;
		}
		//===========================
	
		for(auto i=0; i<=loop; ++i)
		{
			auto it1 = result.begin();//rbegin;
			for(auto cnt=0; cnt<result_column; ++cnt)
				++it1;
			auto it2 = target_str.begin();//nbegin;
			for(auto cnt2=0; cnt2<target_column+i; ++cnt2)
				++it2;
			*it1=*it2;
	
			++local_target_column;
			GetCombination(target_str, local_target_column, result, result_column+1, localloop, container);
			--localloop;
		}
	}	

	bool linear_mismatch (std::string& txt, std::vector<size_t>& found_vec)
	{   
		n = txt.size();
		if (n<m)
			return false;
		
		bool return_value=false;
		size_t i=0,  jj=m-1;
		int found=-100;
		while ( i<=n-m )
		{
//std::cerr<<"current i "<<'\r';
			size_t mmc = MISMATCH_COUNT;
			int j=jj;
			while ( j>=0 && (pattern[j]==txt[i+j] || mmc!=0) )
			{
//std::cerr<<"current j and neq and distance "<<'\r';
				if (pattern[j]!=txt[i+j])
					--mmc;
				--j;
			}
			if (j < 0)
			{
				found = i+jj;
				found_vec.push_back (found);
				++i;
				return_value=true;
			}
			else
				++i;
		}
		return return_value;
	}

	void Print (void)
	{
		std::cerr<<"alphabet "<<std::endl;
		std::cerr<<alphabet<<std::endl;

		std::cerr<<"Ukx_map "<<std::endl;
		std::for_each ( Ukx_map.begin(), Ukx_map.end(),
			[&] (const std::pair<size_t, boost::unordered_map<int, std::set<size_t> > >& Q)
			{ std::cerr<<Q.first<<'\t'; 
			std::for_each (Q.second.begin(), Q.second.end(),
				[&] (const std::pair<int, std::set<size_t> >& q)
				{ std::cerr<<alphabet[q.first]<<'\t';
					std::cerr<<"q.first "<<q.first<<std::endl; 
					std::for_each (q.second.begin(), q.second.end(),
					[] (const size_t& qq)
					{ std::cerr<<qq<<' '; });
				});
			std::cerr<<std::endl;
			});

		std::cerr<<"Vkx_map "<<std::endl;
		std::cerr<<Vkx_map.size()<<std::endl;
		std::for_each (Vkx_map.begin(), Vkx_map.end(),
			[&] (const std::pair<size_t, boost::unordered_map<int, size_t> >& Q)
			{ 
std::cerr<<"current l ";
			std::cerr<<Q.first<<'\n'<<Q.second.size()<<std::endl;
			std::for_each (Q.second.begin(), Q.second.end(),
				[&] (const std::pair <int, size_t>& q)
				{ std::cerr<<q.first<<'\t'<<q.second<<std::endl;});
			});

		std::cerr<<"cvec "<<std::endl;
		std::for_each (cvec.begin(), cvec.end(),
			[&] (const std::pair<int, std::string>& Q)
			{ std::cerr<<Q.first<<'\t'<<Q.second<<std::endl;});
	}
};

template <size_t MISMATCH_COUNT>
class LinearSearch
{
	std::string pattern, txt;
	size_t m, n, length;
	std::string alphabet;
public:
	LinearSearch (std::string& str_pattern, std::string& str_txt)
		: pattern (str_pattern), txt (str_txt)
		, m (pattern.size()), n (txt.size()), length (MISMATCH_COUNT + 1)
	{
		for (size_t i=0; i!=txt.length(); ++i)
		{
			if (alphabet.find(txt[i],0)==std::string::npos)
			alphabet.push_back (txt[i]);
		}   
		for (size_t i=0; i!=pattern.length(); ++i)
		{
			if (alphabet.find(pattern[i],0)==std::string::npos)
			alphabet.push_back (pattern[i]);
		}
	}

	bool linear_mismatch_search (std::vector<size_t>& found_vec)
	{
		if (n<m)
			return false;
		bool return_value=false;
		size_t i=0, jj=m-1;
		int found=-100;
		while ( i<=n-m )
		{
//std::cerr<<"current i "<<'\r';
			size_t mmc = MISMATCH_COUNT;
			int j=jj;
			while ( j>=0 && (pattern[j]==txt[i+j] || mmc!=0) )
			{
//std::cerr<<"current j and neq and distance "<<'\r';
				if (pattern[j]!=txt[i+j])
					--mmc;
				--j;
			}
			if (j < 0)
			{
				found = i+jj;
				found_vec.push_back (found);
				++i;
				return_value=true;
			}
			else
				++i;
		}
		return return_value;
	}
};

std::vector <std::string> pattern_vec;
std::vector <std::string> txt;
std::string pattern //("ataaccctaaccct");//("TCCTTCCCTCAGCGCCACCCCCAGACT");//("CTCCCCTAAATATC");//("AAAAAAAAAAAAAA");
					("TTTGCTGTTCCTGCA");
std::vector<size_t> found_faast, found_linear1, found_linear2;

TEST (non_test, random_gen_string)//(bm_mismatch, test)
{
	std::ifstream ofs ("/Users/obigbando/Downloads/human_chr/chrY.fa");
	std::string s2;
	for (auto i=0; i<199; ++i)
		std::getline(ofs, s2);
	int i =0;
	std::getline (ofs, s2);
	while (!ofs.eof())//(i<1187473)//100000)
	{
//		std::cerr<<i<<'\r';
		std::getline (ofs,s2) ;
		std::transform (s2.begin(), s2.end(), s2.begin(), ::toupper);
		txt.push_back(s2);
		++i;
	}
	std::cerr<<txt.size()<<std::endl;
//	std::cerr<<'\n'<<'\n'<<std::endl;
}

std::vector<std::string> xstrs;

#include <simstring/simstring.h>

void retrieve(
    simstring::reader& dbr,
    const std::string& query,
    int measure,
    double threshold
    )
{

    // Retrieve similar strings into a string vector.
    std::vector<std::string> tmp;
    dbr.retrieve(query, measure, threshold, std::back_inserter(tmp));
	xstrs.insert (xstrs.begin(), tmp.begin(),tmp.end());

    // Output the retrieved strings separated by ", ".
//    for (int i = 0;i < (int)xstrs.size();++i) {
//        std::cout << (i != 0 ? ", " : "") << xstrs[i];
//    }
//    std::cout << std::endl;
}

/*
TEST(example, test)
//int main(int argc, char *argv[])
{
    // Create a SimString database with two person names.
    simstring::ngram_generator gen(3, false);
    simstring::writer_base<std::string> dbw(gen, "sample.db");

    dbw.insert("Barack Hussein Obama II");
    dbw.insert("James Gordon Brown");
std::cerr<<"dbw's database size "<<dbw.m_num_entries<<'\t'<<dbw.m_name<<std::endl;
    dbw.close();

    // Open the database for reading.
    simstring::reader dbr;
    
    dbr.open("sample.db");
//    retrieve(dbr, "Barack Obama", simstring::exact, 0.6);
    retrieve(dbr, "Barack Obama", simstring::cosine, 0.6);
//    retrieve(dbr, "Gordon Brown", simstring::cosine, 0.6);
//    retrieve(dbr, "Obama", simstring::cosine, 0.6);
//    retrieve(dbr, "Obama", simstring::overlap, 1.0);

 //   return 0;
}
*/

TEST(simstring, test)
{
//	for (auto i=txt.begin(); i!=txt.end(); ++i)
//	{
		// Create a SimString database with two person names.
		simstring::ngram_generator gen(3, false);
		simstring::writer_base<std::string> dbw(gen, "sample.db");
	
	//	std::cout<<"dbw's size"<<dbw.m_num_entries<<std::endl;
	//	std::cerr<<txt.size()<<std::endl;	
	
	for (auto i=txt.begin(); i!=txt.end(); ++i)
	{
		dbw.insert (*i);
//std::cerr<<"dbw's database size "<<dbw.m_num_entries<<'\t'<<dbw.m_name<<std::endl;
	//	for (auto i=txt.begin(); i!=txt.end(); ++i)
	//		dbw.insert (*i);
	}
		dbw.close();
	
		// Open the database for reading.
		simstring::reader dbr;	
		dbr.open("sample.db");
	//	retrieve(dbr, pattern, simstring::exact, 0.6);

//for (auto i=txt.begin(); i!=txt.end(); ++i)
//		retrieve(dbr, pattern, simstring::cosine, 0.5);
		retrieve(dbr, pattern, simstring::exact, 0.1);
	//	retrieve(dbr, "abcd", simstring::cosine, 0.6);
	//	retrieve(dbr, "abc", simstring::cosine, 0.6);
	//	retrieve(dbr, "abc", simstring::overlap, 1.0);
	
std::for_each (xstrs.begin(), xstrs.end(),
[] (const std::string& q)
{std::cerr<<q<<std::endl;});
	//	return 0;
//	}
}


TEST (FAAST0, test_2_3)
{
		size_t i=0;
		FAAST<2,3> q (pattern);
//		FAAST<3,1> q (pattern_vec[5]);
	//	q.Print();
		for (auto itr=txt.begin(); itr!=txt.end(); ++itr)
		{
//			q.faast_search(*itr, found_faast);
			if (q.faast_search(*itr, found_faast))
				std::cout<<i<<" th operation found"<<*itr<<std::endl;
			++i;
		}
}

TEST (compare0, linear_2_3)
{
		size_t i=0;
		FAAST<2,3> q (pattern);
//		FAAST<3,1> q (pattern_vec[5], 1);
		for (auto itr=txt.begin(); itr!=txt.end(); ++itr)
		{
//			q.linear_mismatch (*itr, found_linear1);
			if (q.linear_mismatch (*itr, found_linear1))
				std::cout<<i<<" th operation found"<<*itr<<std::endl;
			++i;
		}
	//	EXPECT_EQ (found_faast, found_linear1);
}

TEST (validation0, both_2_3)
{

	std::cerr<<xstrs.size()<<'\t'<<found_faast.size()<<"\t"<<found_linear1.size()<<std::endl;
	EXPECT_EQ (found_faast, found_linear1);
//	EXPECT_EQ (found_faast, xstrs);
//	EXPECT_EQ (xstrs, found_linear1);
//std::for_each (xstrs.begin(), xstrs.end(), 
//std::for_each (found_faast.begin(), found_faast.end(),
}
/*
TEST (control_test0, table_only_2_3)
{
	for (int j=0; j<1000; ++j)
	{
		std::string pat (15,'1');

		for (auto q=0; q!=15; ++q)
		{
			std::uniform_int_distribution<int> d(0, 4);
			std::random_device rd1;
			std::string alphabet ("ACGNT");
			pat[q] = alphabet[d(rd1)];
		}
		std::cerr<<pat<<std::endl;
		pattern_vec.push_back (pat);		

		size_t i=0;
	//	FAAST<4,3> q (pattern);	//takes about 
	//	FAAST<3,3> q (pattern);	//takes about 40 sec
		FAAST<2,3> q (pat);	//takes about 2.3 sec
	//	q.Print();
	}
}

TEST (FAAST0, test_2_3)
{
	for (auto j=pattern_vec.begin(); j!=pattern_vec.end(); ++j)
	{
std::cerr<<j-pattern_vec.begin()<<"round"<<std::endl;
		size_t i=0;
		FAAST<2,3> q (pattern);
//		FAAST<3,1> q (pattern_vec[5]);
	//	q.Print();
		for (auto itr=txt.begin(); itr!=txt.end(); ++itr)
		{
			q.faast_search(*itr, found_faast);
	//		if (q.faast_search(*itr, found_faast))
	//			std::cout<<i<<" th operation found"<<*itr<<std::endl;
	//		++i;
		}
	}
}

TEST (compare0, linear_2_3)
{
	for (auto j=pattern_vec.begin(); j!=pattern_vec.end(); ++j)
	{
std::cerr<<j-pattern_vec.begin()<<"round"<<std::endl;
		size_t i=0;
		FAAST<2,3> q (pattern);
//		FAAST<3,1> q (pattern_vec[5], 1);
		for (auto itr=txt.begin(); itr!=txt.end(); ++itr)
		{
			q.linear_mismatch (*itr, found_linear1);
	//		if (q.linear_mismatch (*itr, found_linear1))
	//			std::cout<<i<<" th operation found"<<*itr<<std::endl;
	//		++i;
		}
	//	EXPECT_EQ (found_faast, found_linear1);
	}
}

TEST (validation0, both_2_3)
{
	std::cerr<<found_faast.size()<<"\t"<<found_linear1.size()<<std::endl;
	EXPECT_EQ (found_faast, found_linear1);
pattern_vec.clear();
}
*/

/*
TEST (control_test1, table_only_3_1)
{
	for (int j=0; j<10; ++j)
	{
		std::string pat (15,'1');

		for (auto q=0; q!=15; ++q)
		{
			std::uniform_int_distribution<int> d(0, 4);
			std::random_device rd1;
			std::string alphabet ("ACGNT");
			pat[q] = alphabet[d(rd1)];
		}
		std::cerr<<pat<<std::endl;
		pattern_vec.push_back (pat);		

		size_t i=0;
	//	FAAST<4,3> q (pattern);	//takes about 
	//	FAAST<3,3> q (pattern);	//takes about 40 sec
		FAAST<3,1> q (pat);	//takes about 2.3 sec
	//	q.Print();
	}
}

TEST (FAAST1, test_3_1)
{
	for (auto j=pattern_vec.begin(); j!=pattern_vec.end(); ++j)
	{
std::cerr<<j-pattern_vec.begin()<<"round"<<std::endl;
		size_t i=0;
		FAAST<3,1> q (pattern);
//		FAAST<3,1> q (pattern_vec[5]);
	//	q.Print();
		for (auto itr=txt.begin(); itr!=txt.end(); ++itr)
		{
			q.faast_search(*itr, found_faast);
	//		if (q.faast_search(*itr, found_faast))
	//			std::cout<<i<<" th operation found"<<*itr<<std::endl;
	//		++i;
		}
	}
}

TEST (compare1, linear_3_1)
{
	for (auto j=pattern_vec.begin(); j!=pattern_vec.end(); ++j)
	{
std::cerr<<j-pattern_vec.begin()<<"round"<<std::endl;
		size_t i=0;
		FAAST<3,1> q (pattern);
//		FAAST<3,1> q (pattern_vec[5], 1);
		for (auto itr=txt.begin(); itr!=txt.end(); ++itr)
		{
			q.linear_mismatch (*itr, found_linear1);
	//		if (q.linear_mismatch (*itr, found_linear1))
	//			std::cout<<i<<" th operation found"<<*itr<<std::endl;
	//		++i;
		}
	//	EXPECT_EQ (found_faast, found_linear1);
	}
}

TEST (validation1, both_3_1)
{
	std::cerr<<found_faast.size()<<"\t"<<found_linear1.size()<<std::endl;
	EXPECT_EQ (found_faast, found_linear1);
pattern_vec.clear();
}

TEST (control_test2, table_only_4_1)
{
	for (int j=0; j<10; ++j)
	{
		std::string pat (15,'1');

		for (auto q=0; q!=15; ++q)
		{
			std::uniform_int_distribution<int> d(0, 4);
			std::random_device rd1;
			std::string alphabet ("ACGNT");
			pat[q] = alphabet[d(rd1)];
		}
		std::cerr<<pat<<std::endl;
		pattern_vec.push_back (pat);		

		size_t i=0;
	//	FAAST<4,3> q (pattern);	//takes about 
	//	FAAST<3,3> q (pattern);	//takes about 40 sec
		FAAST<4,1> q (pat);	//takes about 2.3 sec
	//	q.Print();
	}
}

TEST (FAAST2, test_4_1)
{
	for (auto j=pattern_vec.begin(); j!=pattern_vec.end(); ++j)
	{
std::cerr<<j-pattern_vec.begin()<<"round"<<std::endl;
		size_t i=0;
		FAAST<4,1> q (pattern);
//		FAAST<3,1> q (pattern_vec[5]);
	//	q.Print();
		for (auto itr=txt.begin(); itr!=txt.end(); ++itr)
		{
			q.faast_search(*itr, found_faast);
	//		if (q.faast_search(*itr, found_faast))
	//			std::cout<<i<<" th operation found"<<*itr<<std::endl;
	//		++i;
		}
	}
}

TEST (compare2, linear_4_1)
{
	for (auto j=pattern_vec.begin(); j!=pattern_vec.end(); ++j)
	{
std::cerr<<j-pattern_vec.begin()<<"round"<<std::endl;
		size_t i=0;
		FAAST<4,1> q (pattern);
//		FAAST<3,1> q (pattern_vec[5], 1);
		for (auto itr=txt.begin(); itr!=txt.end(); ++itr)
		{
			q.linear_mismatch (*itr, found_linear1);
	//		if (q.linear_mismatch (*itr, found_linear1))
	//			std::cout<<i<<" th operation found"<<*itr<<std::endl;
	//		++i;
		}
	//	EXPECT_EQ (found_faast, found_linear1);
	}
}

TEST (validation2, both_4_1)
{
	std::cerr<<found_faast.size()<<"\t"<<found_linear1.size()<<std::endl;
	EXPECT_EQ (found_faast, found_linear1);
}

TEST (FAAST3, test_5_0)
{
	for (auto j=pattern_vec.begin(); j!=pattern_vec.end(); ++j)
	{
std::cerr<<j-pattern_vec.begin()<<"round"<<std::endl;
		size_t i=0;
		FAAST<5,0> q (pattern);
//		FAAST<3,1> q (pattern_vec[5]);
	//	q.Print();
		for (auto itr=txt.begin(); itr!=txt.end(); ++itr)
		{
			q.faast_search(*itr, found_faast);
	//		if (q.faast_search(*itr, found_faast))
	//			std::cout<<i<<" th operation found"<<*itr<<std::endl;
	//		++i;
		}
	}
}

TEST (compare3, linear_5_0)
{
	for (auto j=pattern_vec.begin(); j!=pattern_vec.end(); ++j)
	{
std::cerr<<j-pattern_vec.begin()<<"round"<<std::endl;
		size_t i=0;
		FAAST<5,0> q (pattern);
//		FAAST<3,1> q (pattern_vec[5], 1);
		for (auto itr=txt.begin(); itr!=txt.end(); ++itr)
		{
			q.linear_mismatch (*itr, found_linear1);
	//		if (q.linear_mismatch (*itr, found_linear1))
	//			std::cout<<i<<" th operation found"<<*itr<<std::endl;
	//		++i;
		}
	//	EXPECT_EQ (found_faast, found_linear1);
	}
}

TEST (validation3, both_5_0)
{
	std::cerr<<found_faast.size()<<"\t"<<found_linear1.size()<<std::endl;
	EXPECT_EQ (found_faast, found_linear1);
}
*/

/*
TEST (linear_varidating_2, pure_linear)
{
	size_t i=0;
	for (auto itr=txt.begin(); itr!=txt.end(); ++itr)
	{
//		LinearSearch<3> q (pattern, *itr);
		LinearSearch<2> q (pattern, *itr);
		q.linear_mismatch_search (found_linear2);
//		if (q.linear_mismatch_search (found_linear2))
//			std::cout<<i<<" th operation found"<<*itr<<std::endl;
//		++i;
	}
//	EXPECT_EQ (found_faast, found_linear2);
}
*/

