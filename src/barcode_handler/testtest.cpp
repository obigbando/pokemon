#include <iostream>
#include <tuple>
#include "boost/lexical_cast.hpp"
#include "boost/mpl/map.hpp"
#include "boost/mpl/string.hpp"
#include "boost/algorithm/string.hpp"
#include "../format/fastq.hpp"
#include "../file_reader.hpp"

typedef std::tuple < 
		boost::mpl::string<'AAAA','AA'>, 
		boost::mpl::string<'CCCC','CCCC'>,
		boost::mpl::string<'GGGG'>,
		boost::mpl::string<'TT'> 
		> BarcodeTypeT;
//typedef std::tuple_size <BarcodeTypeT> BarcodeSizeT;

template<BarcodeHandleScheme BARCODE_SCHEME, typename BARCODE_TYPE, 
		 template <typename> class FORMAT=Fastq, typename TUPLETYPE=std::tuple<std::string, std::string, std::string, std::string> >
class BarcodeHandlerImpl
{};

template< typename BARCODE_TYPE, template <typename> class FORMAT, typename TUPLETYPE >
class BarcodeHandlerImpl <BarcodeHandleScheme::Five_Prime, BARCODE_TYPE, FORMAT, TUPLETYPE >
{
public:
	typedef std::tuple_size <BARCODE_TYPE> BarcodeSize;

    BarcodeHandlerImpl (void)
    {}

    void Remove (std::map < int, std::vector< Fastq<TUPLETYPE> > >* result2)//, size_t start=0, size_t dif)
	{
		std::swap ( (*result2)[0], (*result2)[-1]);
		for (size_t itr=0; itr!=(*result2)[-1].size(); ++itr)
			BarcodeFormatType<0, char>::ScanBarcodeSeq ((*result2)[-1][itr], result2);
	}

    std::map < int, std::vector< Fastq<TUPLETYPE> > >* operator() (std::map < int, std::vector< Fastq<TUPLETYPE> > >* result2)//, size_t start=0, size_t dif)
    {
        Remove (result2);
		std::vector < Fastq<TUPLETYPE> > Q;
		std::swap ( (*result2)[-1], Q);
		return result2;
    }

	template <int N, typename T>
	struct BarcodeFormatType 
	{
		typedef typename std::tuple_element<N, BARCODE_TYPE>::type ListedType;
		static void ScanBarcodeSeq (FORMAT<TUPLETYPE>& content, std::map < int, std::vector< Fastq<TUPLETYPE> > >* result2)
		{
			if ( boost::mpl::c_str <ListedType>::value == content.getSeq().substr (0, boost::mpl::size<ListedType>::value) )
			{
//				std::cerr<<"CURRENT N: "<<N<<"boost::mpl::c_str<TAG_TYPE>::value == tag_name\t" <<
//					boost::mpl::c_str< ListedType >::value << "\tsize of barcode: "<<boost::mpl::size<ListedType>::value<<'\n';
				(*result2)[N].push_back (std::move (content) );
			}
			else
			{
//				std::cerr<<"CURRENT N: "<<N<<"boost::mpl::c_str<TAG_TYPE>::value != tag_name\t" <<
//					boost::mpl::c_str< ListedType >::value << "\nsize of barcode: "<<boost::mpl::size<ListedType>::value<<'\n';
				BarcodeFormatType <N+1, T>::ScanBarcodeSeq (content, result2);
			}
		}
	};

	template <typename T>
	struct BarcodeFormatType <std::tuple_size <BARCODE_TYPE>::value-1, T>
	{
		typedef typename std::tuple_element<std::tuple_size <BarcodeTypeT>::value-1, BARCODE_TYPE>::type ListedType;
		static void ScanBarcodeSeq (FORMAT<TUPLETYPE>& content, std::map < int, std::vector< Fastq<TUPLETYPE> > >* result2)
		{
			if ( boost::mpl::c_str <ListedType>::value == content.getSeq().substr (0, boost::mpl::size<ListedType>::value) )
			{
//				std::cerr<<"CURRENT N: "<<std::tuple_size <BarcodeTypeT>::value-1<<"boost::mpl::c_str<TAG_TYPE>::value == tag_name\t" <<
//					boost::mpl::c_str< ListedType >::value << "\tsize of barcode: "<<boost::mpl::size<ListedType>::value<<'\n';
				(*result2)[std::tuple_size <BarcodeTypeT>::value-1].push_back (std::move (content) );
			}
			else
			{
//				std::cerr<<"CURRENT N: "<<std::tuple_size <BarcodeTypeT>::value-1<<"boost::mpl::c_str<TAG_TYPE>::value == tag_name\t" <<
//					boost::mpl::c_str< ListedType >::value << "\tsize of barcode: "<<boost::mpl::size<ListedType>::value<<'\n';
			}
		}
	};
};

typedef std::tuple <std::string, std::string, std::string, std::string> TUPLETYPE;


void Print ( std::map <int, std::vector < Fastq<TUPLETYPE> > >* ptr)
{
	for (auto& Q: (*ptr ) )
	{
		std::cerr<<"Key : vector.size "<<Q.first<<" : "<<Q.second.size()<<'\n';
		for (auto & QQ : Q.second)
			std::cerr<<QQ.getSeq()<<'\n';
	}
};


int main (void)
{
	typedef FileReader < ParallelTypes::NORMAL, Fastq, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE > Reader;

	std::map <int, std::vector < Fastq<TUPLETYPE> > > yy;
	std::vector<std::string> read_vec ({"/mnt/godzilla/jones/dra000205/DRX000313/DRR000555.fastq"});////
	Reader o (read_vec, &yy);
	std::vector <size_t> job_id;
size_t i=0;
//	while (true)//(i!=limit)
	{
std::cout<<i<<'\r';
		auto ptr = new std::map <int, std::vector < Fastq<TUPLETYPE> > >;
		auto eof_info = o.Read_NSkip (ptr, 10000);
//		if (eof_info)
//			break;
		BarcodeHandlerImpl <BarcodeHandleScheme::Five_Prime, BarcodeTypeT> BC;
		BC (ptr);
		Print (ptr);
++i;
	}
};


template <typename TUPLETYPE>
struct TypeList
{
   template <int N>
   using type = typename std::tuple_element< N, TUPLETYPE>::type;
};

enum BarcodeType
{
	LengthofFour
};


template<enum BarcodeType>
struct BarcodeParameter
{};

template<>
struct BarcodeParameter <BarcodeType::LengthofFour>
{
	typedef std::tuple < //	typedef TypeList <
		boost::mpl::string<'aaaa'>, 
		boost::mpl::string<'cccc'>,
		boost::mpl::string<'gggg'>,
		boost::mpl::string<'tttt'> > BarcodeType;
	typedef std::tuple_size <BarcodeType> BarcodeSize;
//	 static const int BarcodeParameterSize = std::tuple_size <BarcodeType>::value;
};

template <int N, typename BARCODE_PARAMETER>
struct BarcodeFormatType 
{
	typedef typename std::tuple_element<N, typename BARCODE_PARAMETER::BarcodeType>::type ListedType;
//	typedef typename TypeList <typename BARCODE_PARAMETER::BarcodeType>::type<N> ListedType;
	static void assign (std::string& strin)
	{
		if ( boost::mpl::c_str <ListedType>::value == strin )
		{
			std::cerr<<"CURRENT N: "<<N<<"boost::mpl::c_str<TAG_TYPE>::value == tag_name\t" <<
				boost::mpl::c_str< ListedType >::value << "\tsize of barcode: "<<boost::mpl::size<ListedType>::value<<'\n';
//			std::get<N>(tuple) = boost::lexical_cast< TagValueType > ( tag_value );
		}
		else
		{
			std::cerr<<"CURRENT N: "<<N<<"boost::mpl::c_str<TAG_TYPE>::value != tag_name\t" <<
				boost::mpl::c_str< ListedType >::value << "\nsize of barcode: "<<boost::mpl::size<ListedType>::value<<'\n';
			BarcodeFormatType <N+1, BARCODE_PARAMETER>::assign (strin);
//			AssignTuple<N+1, ARGS...>::assign(tag_name, tag_value, tuple);
		}
	}
};

template <typename BARCODE_PARAMETER>
struct BarcodeFormatType < BARCODE_PARAMETER::BarcodeSize::value-1, BARCODE_PARAMETER>
{
	typedef typename TypeList <typename BARCODE_PARAMETER::BarcodeType>::template type<3> ListedType;
//template type<BARCODE_PARAMETER::BarcodeSize::value-1> ListedType;
//	typedef typename std::tuple_element< BARCODE_PARAMETER::BarcodeSize::value-1 , typename BARCODE_PARAMETER::BarcodeType>::type ListedType;
	static void assign (std::string& strin)
	{
		if ( boost::mpl::c_str <ListedType >::value == strin )
		{
			std::cerr<<"END !! \nCURRENT N: "<<BARCODE_PARAMETER::BarcodeSize::value-1<<"boost::mpl::c_str<TAG_TYPE>::value == tag_name\t" <<
				boost::mpl::c_str< ListedType >::value << "\tsize of barcode: "<<boost::mpl::size<ListedType>::value<<'\n';
			//std::get<N>(tuple) = boost::lexical_cast< TagValueType > ( tag_value );
		}
		else
		{
			std::cerr<<"END !! \nCURRENT N: "<<BARCODE_PARAMETER::BarcodeSize::value-1<<"boost::mpl::c_str<TAG_TYPE>::value != tag_name\t" <<
				boost::mpl::c_str< ListedType >::value << "\tsize of barcode: "<<boost::mpl::size<ListedType>::value<<'\n';
//			AssignTuple<N+1, ARGS...>::assign(tag_name, tag_value, tuple);
		}
	}
};

/*
int main (void)
{
	std::string QQ ("tttt");//acgtacgtAAA");
	//AssignTuple<0, boost::mpl::string<'NH'>, boost::mpl::string<'TL'> >::assign(tmp_splits2[0], tmp_splits2[2], qq);
	BarcodeFormatType <0,  BarcodeParameter <BarcodeType::LengthofFour> > :: assign (QQ);
	return 0;
};
*/


/*	Sam format study
template<class TAG_NAME_TYPE>
struct TagFormatType
{};

template<> struct TagFormatType< boost::mpl::string<'AM'> > { typedef int ValueType; };
template<> struct TagFormatType< boost::mpl::string<'NH'> > { typedef int ValueType; };
template<> struct TagFormatType< boost::mpl::string<'TL'> > { typedef std::string ValueType; };

template<int N, class TAG_TYPE, class... ARGS>
struct AssignTuple
{
    typedef typename TagFormatType<TAG_TYPE>::ValueType TagValueType;
    
    template<class tag_type, class tuple_type>
    static void assign(std::string &tag_name, tag_type &tag_value, tuple_type & tuple)
    {
        if(boost::mpl::c_str<TAG_TYPE>::value == tag_name)
        {
std::cerr<<"boost::mpl::c_str<TAG_TYPE>::value == tag_name" <<
boost::mpl::c_str<TAG_TYPE>::value << '\t'<< tag_name<<'\n';
	        std::get<N>(tuple) = boost::lexical_cast< TagValueType > ( tag_value );
        }
        else
        {
std::cerr<<"boost::mpl::c_str<TAG_TYPE>::value != tag_name" <<
boost::mpl::c_str<TAG_TYPE>::value << '\t'<< tag_name<<'\n';
            AssignTuple<N+1, ARGS...>::assign(tag_name, tag_value, tuple);
        }
    }
};

template<int N, class TAG_TYPE>
struct AssignTuple < N, TAG_TYPE >
{
    typedef typename TagFormatType<TAG_TYPE>::ValueType TagValueType;
    
    template<class tag_type, class tuple_type>
    static void assign(std::string &tag_name, tag_type &tag_value, tuple_type & tuple)
    {
        if(boost::mpl::c_str<TAG_TYPE>::value == tag_name)
        {
std::cerr<<"END !! boost::mpl::c_str<TAG_TYPE>::value == tag_name" <<
boost::mpl::c_str<TAG_TYPE>::value << '\t'<< tag_name<<'\n';
            std::get<N>(tuple) = boost::lexical_cast< TagValueType > ( tag_value ); 
        }
    }
};

template <typename T, typename... ARGS>
struct PackTupleType
{};

template <typename T, typename... ARGS>
struct PackTupleType <T, std::tuple<ARGS...> >
{
	typedef std::tuple <T, ARGS...> PackedTupleType;
};

template <typename TAG, typename... TYPE_ARGS>
struct UnWindType
{
	typedef typename TagFormatType <TAG>::ValueType TagType;
	typedef typename PackTupleType <TagType, typename UnWindType <TYPE_ARGS...>::TupleType>::PackedTupleType TupleType;
};

template <typename TAG>
struct UnWindType <TAG>
{
	typedef typename TagFormatType <TAG>::ValueType TagType;
	typedef std::tuple <TagType> TupleType;
};

int main (void)
{
	std::tuple <int, std::string> qq;
    const std::string tags_str //("rd29286\t16\tchr1\t29286\t0\t18M0M0S\t*\t0\t0\tACCCTACTTCCCGCGGCC\t*\tNH:i:5\tTL:Z:ATAT");
//	("NH:i:5\tTL:Z:ATAT");
	("TL:Z:ATAT\tNH:i:5");
	std::vector<std::string> tmp_splits;
	boost::split( tmp_splits, tags_str, boost::is_any_of( "\t" ));
	for(auto iter(tmp_splits.begin()); iter != tmp_splits.end(); iter++ )
	{
		std::vector<std::string> tmp_splits2;
		boost::split( tmp_splits2, *iter, boost::is_any_of( ":" ));
		if(tmp_splits2.size() != 3)
		{
			std::cerr << "Error! '" << *iter << "' is not a sam tag format" << std::endl;
			continue;
		}
		AssignTuple<0, boost::mpl::string<'NH'>, boost::mpl::string<'TL'> >::assign(tmp_splits2[0], tmp_splits2[2], qq);
		//AssignTuple<0, boost::mpl::string<'TL'>, boost::mpl::string<'NH'> >::assign(tmp_splits2[0], tmp_splits2[2], qq);
	}
	return 0;
};
*/


