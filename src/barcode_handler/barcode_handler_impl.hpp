/**
 * @file barcode_handler_impl.hpp
 * @brief Provide a barcode handle class, capable of scanning barcode sequence from Fastq, either from 5' end or 3' end
 * @author C-Salt corp.
 */
#ifndef BARCODE_HANDLER_IMPL_HPP_
#define BARCODE_HANDLER_IMPL_HPP_
#include <tuple>
#include "boost/algorithm/string/trim.hpp"
#include "boost/algorithm/string/erase.hpp"
//#include "../format/fastq_barcode.hpp"
#include "../file_reader_impl.hpp"
#include "../tuple_utility.hpp"
#include "../constant_def.hpp"

/**
 * @class BarcodeHandlerImpl
 * @brief provide a generic form of barcode handler
 * @tparam none-type parameter of int / enum of BARCODE_SCHEME, indicating which kind of barcode the handler is trying to handle
 * @tparam template template parameter of FORMAT, designed to be any one of current data formats
 * @tparam TUPLETYPE indicating data format of the got piece of data
 */
template< BarcodeHandleScheme BARCODE_SCHEME, template <typename> class FORMAT, typename TUPLETYPE>
class BarcodeHandlerImpl
{};

/**
 * @class BarcodeHandlerImpl
 * @brief specialized form of BarcodeHandlerImpl, with BarcodeHandleScheme specialized to be Five_Prime.  The class checks whether each of the elements of the barcode_content_ appears in the front portion of the sequence of Fastq; if so, have the corresponding barcode content removed from the sequence.
 */
template <typename TUPLETYPE>
class BarcodeHandlerImpl < BarcodeHandleScheme::Five_Prime, 
						   Fastq,
						   TUPLETYPE
						 >
{
public:
	std::vector <std::string> barcode_content_;
	size_t barcode_length_; 

/**
 * @brief constructor with barcode vector
 */
	BarcodeHandlerImpl (std::vector<std::string>& barcode_in)
		: barcode_content_ (barcode_in)
		, barcode_length_ (barcode_content_.front().size())
	{}

/**
 * @brief actual function to handle adapter removing function 
 */
	void Remove (std::map < int, std::vector< Fastq<TUPLETYPE> > >* result2)//, size_t start=0, size_t dif)
	{
		for ( size_t index=0; index!=result2->size(); ++index)
			for (size_t itr=0; itr!=(*result2)[0].size(); ++itr)// ( size_t itr=start; itr!=start+dif; ++itr )
			{
				if ( ScanBarcodeSeq ((*result2)[index][itr]) >= 0 )
				{
					std::get<1> ( ((*result2)[index][itr]).data ).replace (0, barcode_length_, "");
					std::get<3> ( ((*result2)[index][itr]).data ).replace (0, barcode_length_, "");
				}
			}
	}

	std::map < int, std::vector< Fastq<TUPLETYPE> > >* operator() (std::map < int, std::vector< Fastq<TUPLETYPE> > >* result2)//, size_t start=0, size_t dif)
	{
		Remove (result2);
		return result2;
	}

//private:
/**
 * @brief scan the first barcode_length_ characters of sequence, to determine which groups the corresponding Fastq should be grouped.  Return -5566 to indicate that the corresponding Fastq is not grouped in any of the designated barcode group
 */
	int ScanBarcodeSeq (Fastq<TUPLETYPE>& content)
	{
		for (auto index=0; index!=barcode_content_.size(); ++index)	
			if (std::get<1> (content.data).substr (0, barcode_length_) == barcode_content_[index])
			{
				content.group_index_ = index;
				break;
			}
		return content.group_index_;
	}
};

/**
 * @class BarcodeHandlerImpl
 * @brief specialized form of BarcodeHandlerImpl, with BarcodeHandleScheme specialized to be Three_Prime.  The class checks whether each of the elements of the barcode_content_ appears in the sequence of Fastq; if so, report the one with the greatest index as the hit and have the corresponding barcode content removed from the sequence.
 */
template <typename TUPLETYPE>
class BarcodeHandlerImpl < BarcodeHandleScheme::Three_Prime, 
						   Fastq,
						   TUPLETYPE
						 >
{
public:
	std::vector <std::string> barcode_content_;
	size_t barcode_length_; 

/**
 * @brief constructor with barcode vector
 */
	BarcodeHandlerImpl (std::vector<std::string>& barcode_in)
		: barcode_content_ (barcode_in)
		, barcode_length_ (barcode_content_.front().size())
	{}

/**
 * @brief actual function to handle adapter removing function 
 */
	void Remove (std::map < int, std::vector< Fastq<TUPLETYPE> > >* result2)//, size_t start=0, size_t dif)
	{
		for ( size_t index=0; index!=result2->size(); ++index)
			for (size_t itr=0; itr!=(*result2)[0].size(); ++itr)// ( size_t itr=start; itr!=start+dif; ++itr )
			{
				int position = ScanBarcodeSeq ((*result2)[index][itr]);
				if ( position >= 0 )
				{
					std::get<1> ( ((*result2)[index][itr]).data ).replace (position, barcode_length_, "");
					std::get<3> ( ((*result2)[index][itr]).data ).replace (position, barcode_length_, "");
				}
			}
	}

//private:
/**
 * @brief having the sequence scanned by each of the barcode sequences, and report the match with greatest index as the hit.  Ungroup_default value of -5566 will be returned if no match can be found
 */
	int ScanBarcodeSeq (Fastq<TUPLETYPE>& content)
	{
		int temp_pos=Ungroup_default, temp_index;
		for (auto index=0; index!=barcode_content_.size(); ++index)	
		{
			if (std::get<1> (content.data).rfind (barcode_content_[index]) !=std::string::npos && 
				std::get<1> (content.data).rfind (barcode_content_[index]) > temp_pos)
			{
				temp_pos = (std::get<1>(content.data)).rfind (barcode_content_[index]);
				temp_index = index;
			}
		}
		content.group_index_ = temp_index;
		return temp_pos;
	}
};


#endif
