#include <iostream>
#include <tuple>
#include "boost/lexical_cast.hpp"
#include "boost/mpl/map.hpp"
#include "boost/mpl/string.hpp"
#include "boost/algorithm/string.hpp"
#include "../format/fastq.hpp"
#include "../file_reader.hpp"
#include "barcode_handler_impl_static.hpp"

typedef std::tuple < 
		boost::mpl::string<'AAAA','AA'>, 
		boost::mpl::string<'CCCC','CCCC'>,
		boost::mpl::string<'GGGG'>,
		boost::mpl::string<'TT'> 
		> BarcodeTypeT;

typedef std::tuple <std::string, std::string, std::string, std::string> TUPLETYPE;

void Print ( std::map <int, std::vector < Fastq<TUPLETYPE> > >* ptr)
{
	for (auto& Q: (*ptr ) )
	{
		std::cerr<<"Key : vector.size "<<Q.first<<" : "<<Q.second.size()<<'\n';
		for (auto & QQ : Q.second)
			std::cerr<<QQ.getSeq()<<'\n';
	}
};


int main (void)
{
	typedef FileReader < ParallelTypes::NORMAL, Fastq, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE > Reader;

	std::map <int, std::vector < Fastq<TUPLETYPE> > > yy;
	std::vector<std::string> read_vec ({"/mnt/godzilla/jones/dra000205/DRX000313/DRR000555.fastq"});////
	Reader o (read_vec, &yy);
	std::vector <size_t> job_id;
size_t i=0;
//	while (true)//(i!=limit)
	{
std::cout<<i<<'\r';
		auto ptr = new std::map <int, std::vector < Fastq<TUPLETYPE> > >;
		auto eof_info = o.Read_NSkip (ptr, 10000);
//		if (eof_info)
//			break;
		BarcodeHandler <BarcodeHandleScheme::Five_Prime, BarcodeTypeT> BC;
		//BarcodeHandler <BarcodeHandleScheme::Three_Prime, BarcodeTypeT> BC;
		BC (ptr);
		Print (ptr);
++i;
	}
};


