/**
 *  @file sam2rawbed.hpp
 *  @brief Last pipeline for converting sam to bed_raw
 *  @author C-Salt Corp.
 */
#ifndef SAM2RAWBED
#define SAM2RAWBED
#include <mutex>
#include <thread>
#include "../format/raw_bed.hpp"

/**
 * @struct Sam2RawBed
 * @brief used to maintain a static std::map < RawBed<>, uint16_t> >* to keep track of the converted RawBed <>, which serves as a space reduced and histogramed data structure of the inputted Sam file 
 * @tparam INPUT_TYPE designed with the type of std::vector< Sam<> >*, representing the inputted Sam data
 */
template<class INPUT_TYPE>
class Sam2RawBed
{
public:
/**
 * @static
 * @memberof Sam2RawBed <INPUT_TYPE>
 * @brief provide a static mutex competed by each of the pipeline arranged Sam2RawBed classes, so as to prevent data race situation therebetween
 */
	static std::mutex map_insert_mutex_;

/**
 * @static
 * @memberof Sam2RawBed <INPUT_TYPE>
 * @brief keep track of the recorded std::map <RawBed<>, uint16_t >*.  The mapped value uint16_t is actually a dummy value, with its existance only for assigning a pointer step size of 2 bytes.  
 */
	static std::map < RawBed<>, uint16_t >* rawbed_map_;

	static std::map < int, std::map < RawBed<>, uint16_t > >* rawbed_map2_;

/**
 * @brief default constructor
 */
	Sam2RawBed()
	{}

/**
 * @memberof Sam2RawBed <INPUT_TYPE>
 * @brief what we actually wanna to do is to calculate the address of reads_count_ of the key value of RawBed<> type, which is 5 steps, with the stepsize of 2 bytes, backward of the mapped value type of uint16_t, and update the value of reads_count_ in the Convert function.
 * @param in with type of std::vector< Sam<> >*
 */
	std::map < RawBed<>, uint16_t >* Convert (INPUT_TYPE in)
	{
		std::lock_guard <std::mutex> lock (map_insert_mutex_);
		{
			for(auto &i : *in)
				++ (*((&((*rawbed_map_)[RawBed<>(i)]))-5));	//what we want to get is the address of reads_count_ member of the key value (*rawbed_map_)[RawBed<>(i)] 
															//and update its value accordingly
			return rawbed_map_;
		}
	}

	std::map < int, std::map < RawBed<>, uint16_t > >* Convert2 (INPUT_TYPE in)	//std::map <int, std::vector< Sam<> >* >*
	{
		std::lock_guard <std::mutex> lock (map_insert_mutex_);
		{
			for (auto& Q : *in)
				for (auto &i : (Q.second))
					++ (*((&((*rawbed_map2_)[Q.first][RawBed<>(i)]))-5));//what we want to get is the address of reads_count_ member of the key value (*rawbed_map_)[RawBed<>(i)] 
																		 //and update its value accordingly
			return rawbed_map2_;
		}
	}
};

template <typename INPUT_TYPE>
std::mutex Sam2RawBed<INPUT_TYPE>::map_insert_mutex_;

template <typename INPUT_TYPE>
std::map < RawBed<>, uint16_t >* Sam2RawBed<INPUT_TYPE>::rawbed_map_ = new std::map < RawBed<>, uint16_t >;

template <typename INPUT_TYPE>
std::map < int, std::map < RawBed<>, uint16_t > >* Sam2RawBed<INPUT_TYPE>::rawbed_map2_ 
	= new std::map < int, std::map < RawBed<>, uint16_t > >;
#endif
