#include <iostream>
#include <sstream>

#include <algorithm>// copy, min
#include <iosfwd>            
#include <string>// streamsize
#include <cassert>
#include <ios>// ios_base::beg
#include <string>
#include <boost/iostreams/categories.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/positioning.hpp>

#include "file_writter.hpp"
#include "gtest/gtest.h"
#include "../format/fastq.hpp"
#include "file_reader_2.hpp"

TEST (file_reader, Read)
{
	typedef std::tuple <std::string, std::string, std::string, std::string> TUPLETYPE;

	std::vector<std::string> url_vec ({
	"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7"//,
//	"https://api.basespace.illumina.com/v1pre3/files/3360154/content?access_token=bfb36a45cfa747d88b9b5d933c7c0259"
	});
	std::vector<uint64_t> size_vec ({7493990});//, 444670062});

	std::map <int, std::vector<Fastq<TUPLETYPE> > > yy;

	FRTest::FileReader <ParallelTypes::NORMAL, Fastq, TUPLETYPE, FileWritterBaseSpaceDownload> FileReader (url_vec, &yy, size_vec); 

	std::ofstream ffy ("content.fq");
	while(true)
	{
		std::map <int, std::vector<Fastq<TUPLETYPE> > > y2;
		auto eof_flag = FileReader.Read(&y2, 1000);
		if (!eof_flag)
			for (auto& Q : y2[0])
				ffy << Q;
		else
			break;
	}
	ffy.close();
};

TEST (file_reader, Read_Skip_N)
{
	typedef std::tuple <std::string, std::string, std::string, std::string> TUPLETYPE;

	std::vector<std::string> url_vec ({
	"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7"//,
//	"https://api.basespace.illumina.com/v1pre3/files/3360154/content?access_token=bfb36a45cfa747d88b9b5d933c7c0259"
	});
	std::vector<uint64_t> size_vec ({7493990});//, 444670062});

	std::map <int, std::vector<Fastq<TUPLETYPE> > > yy;

	FRTest::FileReader <ParallelTypes::NORMAL, Fastq, TUPLETYPE, FileWritterBaseSpaceDownload> FileReader (url_vec, &yy, size_vec); 

	std::ofstream ffy ("content_skipN.fq");
//	std::ofstream ffy2 ("content_skipN2.fq");
	while (true)
	{
		std::map <int, std::vector<Fastq<TUPLETYPE> > > y2;
		auto eof_flag = FileReader.Read_NSkip(&y2, 10000);
		for (auto& Q : y2[0])
			ffy << Q;
//		for (auto& q : y2[1])
//			ffy2 << q;
		if (eof_flag)
			break;
	}
//	ffy2.close();
	ffy.close();
};


