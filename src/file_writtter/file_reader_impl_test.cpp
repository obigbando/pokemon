#include <iostream>
#include <sstream>

#include <algorithm>// copy, min
#include <iosfwd>            
#include <string>// streamsize
#include <cassert>
#include <ios>// ios_base::beg
#include <string>
#include <boost/iostreams/categories.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/positioning.hpp>

//#include "basespace_setting.hpp"
#include "file_writter.hpp"
//#include "fw_device_ofstream.hpp"
//#include "fw_device_basespace_download.hpp"
//#include "fw_filter_gzip_compressor.hpp"
#include "gtest/gtest.h"
#include "../format/fastq.hpp"
#include "file_reader_impl_2.hpp"
TEST (file_reader_impl_2, test1)
{
	typedef std::tuple <std::string, std::string, std::string, std::string> TUPLETYPE;

	std::vector<std::string> url_vec ({
	"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7",
	"https://api.basespace.illumina.com/v1pre3/files/3360154/content?access_token=bfb36a45cfa747d88b9b5d933c7c0259"
	});
	std::vector<uint64_t> size_vec ({7493990, 444670062});

	FRTest::FileReader_impl <Fastq, TUPLETYPE, FileWritterBaseSpaceDownload> FileReader (url_vec, size_vec); 

	std::ofstream ffy ("content.fq");
	while(true)
	{
		auto fq = FileReader.get_next_entry(0);
		if (!fq.eof_flag)
			ffy << fq;
		else
			break;
	}
	ffy.close();
   
	std::ofstream ffy2 ("content2.fq");
	while(true)
	{
		auto fq = FileReader.get_next_entry(1);
		if (!fq.eof_flag)
			ffy2 << fq;
		else
			break;
	}
	ffy2.close();
};
