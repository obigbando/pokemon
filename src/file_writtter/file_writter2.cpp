#include <iostream>
#include <sstream>

#include <algorithm>// copy, min
#include <iosfwd>            
#include <string>// streamsize
#include <cassert>
#include <ios>// ios_base::beg
#include <string>
#include <boost/iostreams/categories.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/positioning.hpp>

#include "file_writter.hpp"
#include "fw_device_ofstream.hpp"
#include "fw_device_basespace_download.hpp"
#include "fw_filter_gzip_compressor.hpp"
#include "gtest/gtest.h"
#include "../format/fastq.hpp"

typedef boost::mpl::map
<
	boost::mpl::pair< FWGlobalParameter::FilteringStreamType, boost::iostreams::filtering_streambuf<boost::iostreams::input> >
	, boost::mpl::pair< FWGlobalParameter::DeviceParameter, DeviceParameter >
	, boost::mpl::pair< FWGlobalParameter::MutipleNumber, boost::mpl::int_<2> >
	, boost::mpl::pair< FWGlobalParameter::DeviceBufferSize, boost::mpl::int_<8> >
	, boost::mpl::pair< FWGlobalParameter::DevicePushbackSize, boost::mpl::int_<16> >

> FILE_WRITTER_GLOBAL_SETTING;

typedef SharedMemory<FILE_WRITTER_GLOBAL_SETTING> SharedMemoryType;

typedef boost::mpl::vector
<
	boost::mpl::map
	<
		boost::mpl::pair< FWGlobalParameter::DeviceType, boost::mpl::int_<FileDeviceType::GzipDeCompressFilter> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< FWGlobalParameter::DeviceType, boost::mpl::int_<FileDeviceType::BasespaceDevice_download> >/*,
		boost::mpl::pair< BaseSpaceLocalParameter::CurlSendMaxLength, boost::mpl::int_<6*1024*1024> >,
		boost::mpl::pair< BaseSpaceLocalParameter::CurlSendLastLength, boost::mpl::int_<5*1024*1024> >*/
	>//,
> FILE_WRITER_LIST;

TEST (basespace, test_with_sizeinfo)
{
    DeviceParameter dp;
	dp.bs_download_url_ = //"http://www.jhhlab.tw/tt";//
"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7";
//"https://api.basespace.illumina.com/v1pre3/files/3360154/content?access_token=bfb36a45cfa747d88b9b5d933c7c0259";
	dp.bs_download_size_ = 7493990;     

    File_writter<FILE_WRITTER_GLOBAL_SETTING, FILE_WRITER_LIST, std::istream>  bb(dp);
//	std::chrono::milliseconds dura ( 2000 );
//	std::this_thread::sleep_for ( dura );

	std::ofstream ffy ("content_withsizeinfo.fq");
	int ii = 1024*1024;//444670062;
	char* ff = new char[ii+1];//1024*1024];
	while(!bb.eof())
	{
		bb.read(ff, ii);//1024*1024);
		ffy.write (ff, bb.gcount());
		//	std::string yy;
		//	std::getline (bb, yy);
		//	bb >> yy;	
	}
	delete ff;
	ffy.close();
    //bb.close();//ff, 1024*1024);
};

TEST (basespace, test1)
{
    DeviceParameter dp;
	dp.bs_download_url_ = //"http://www.jhhlab.tw/tt";//
"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7";
//"https://api.basespace.illumina.com/v1pre3/files/3360154/content?access_token=bfb36a45cfa747d88b9b5d933c7c0259";
	dp.bs_download_size_ = 0;
    
    File_writter<FILE_WRITTER_GLOBAL_SETTING, FILE_WRITER_LIST, std::istream>  bb(dp);
//	std::chrono::milliseconds dura ( 2000 );
//	std::this_thread::sleep_for ( dura );

	std::ofstream ffy ("content.fq");
	int ii = 1024*1024;//444670062;
	char* ff = new char[ii+1];//1024*1024];
	while(!bb.eof())
	{
		bb.read(ff, ii);//1024*1024);
		ffy.write (ff, bb.gcount());
		//	std::string yy;
		//	std::getline (bb, yy);
		//	bb >> yy;	
	}
	delete ff;
	ffy.close();
    //bb.close();//ff, 1024*1024);
};


