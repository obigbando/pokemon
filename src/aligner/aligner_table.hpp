/**
 *  @file aligner_table.h
 *  @brief Aligner Searcher與Indexer，所必須的 table，包含建表以及使用方法
 *  @author C-Salt Corp.
 */
#ifndef ALIGNER_TABLE_HPP_
#define ALIGNER_TABLE_HPP_
#include <algorithm>
#include <ctime>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <tuple>
#include <vector>
#include <utility>
#include <memory>

#include "../constant_def.hpp"
#include "../compression/abit.hpp"
#include "../compression/jbit.hpp"

#include "boost/serialization/utility.hpp"
#include "boost/archive/binary_oarchive.hpp"
#include "boost/archive/binary_iarchive.hpp"
#include "boost/iostreams/filtering_stream.hpp"
#include "boost/iostreams/device/file.hpp"
#include "boost/iostreams/filter/zlib.hpp"
#include "boost/serialization/map.hpp"

/**
 * @class Aligner_table< int AlignerType >
 * @brief Aligner_table 範型版本，可以特化不同 Aligner 所需要的 table, e.g.,BWT or BLAST
 * @tparam AlignerType (emun)，用來特化用的參數
 */

template<int AlignerType>
class Aligner_table
{};


/**
 * @class Aligner_table< Aligner_types::BWT_Aligner >
 * @brief Aligner Table 的 BWT 特化版本，專門處理建表與基本搜尋會使用的方法
 * @tparam AlignerType (emun)，BWT_Aligner 版本
 */

template<>
class Aligner_table<Aligner_types::BWT_Aligner>
{
private:
	/**
	 * @brief char_size 為內部使用的變數，256，決定一些表的大小，此參數不可變動
	 */
	INTTYPE char_size;
	
	/**
	 * @brief powerV 為內部使用變數，為2的n次方，為了加速計算
	 */
	INTTYPE powerV;
	
	/**
	 * @brief first_location 為內部使用變數，因為$在壓縮bwt後資訊會消失，也就損失suffix index 在起始的位置，因此要特別記錄
	 */
	INTTYPE first_location;
	
	/**
	 * @brief occ_char 為內部使用變數，{'A', 'C', 'G', 'T'}，方便使用
	 */
	std::vector<int> occ_char;
	
	/**
	 * @brief mtable 為內部使用變數，256大小，存放字元對應欲壓縮的數字，A=0, C=1, G=2, T=3
	 */
	std::vector<INTTYPE> mtable;
	
	/**
	 * @brief jbwt_idx_char 為內部使用變數，為了還原壓縮 jbwt，以及方便計算所記的表
	 */
	std::string jbwt_idx_char;
	
	/**
	 * @brief jbwt 為內部使用變數，指向 JBit class
	 */
	std::shared_ptr<JBit> jbwt;

	
public:
	
	static int is_table_loaded;
	
	/**
	 * @brief interval 為建表時的間隔大小，間隔越小記憶體使用越大，搜尋越慢
	 */
	INTTYPE interval;
	
	/**
	 * @brief bwt 舊版用來儲存 bwt 字串的變數，現在已經捨棄不用。改用 jbwt 壓縮過的
	 */
	std::string bwt;
	
	/**
	 * @brief BWT 演算法中，要復原字串時所記的 C 表，記錄排序後，ACGT在suffix第一個字的起始位置
	 */
	std::vector<INTTYPE> c_function;
	
	/**
	 * @brief BWT 演算法中，要復原字串時所記的 C 表，不一樣的是，這邊不只記錄一個字，而是記錄一串字(12字，窮舉)的起始位置
	 */
	std::vector<INTTYPE> c_functions;
	
	/**
	 * @brief BWT 演算法中，要復原字串時所記的 OCC 表，記錄某個 bwt index 之上，累積有幾個 A, C, G or T
	 */
	std::vector< std::vector<INTTYPE> > occ_function;
	
	/**
	 * @brief BWT 演算法中，要復原字串時所記的 OCC 表，因為最後採用壓縮 jbwt，所以累積字元 A, C, G or T，需要重新記錄，為三維陣列
	 * 1d: size=256 但只存放 ACGT的ascii，2d
	 * 2d: size=5 要查詢的壓縮字串中的第n個字，前累積有C字幾個, 01234, 4比較特殊，就是下一個壓縮字的 0 所記的數
	 * 3d: size=256 壓縮字元，256個字
	 */
	std::vector< std::vector< std::vector<INTTYPE> > > occ_jbwt;
	
	/**
	 * @brief BWT 演算法中，記錄最後要將 bwt index 轉成真正在 Genome 上對應的 location。此表用來加速，避免 trace back 太多字。
	 */
	std::vector< std::pair<INTTYPE, INTTYPE> > location_table;
	
	/**
	 * @brief 超快速找 location表，此表記錄某個 bwt index trace back 幾次就一定在location表中查的到對應的位置，可以避免無謂的 binary search
	 */
	std::vector<uint8_t> fbwt;
	
	/**
	 * @brief 記錄壓縮後的 bwt，一個 uint8_t 代表四個 A, C, G or T 
	 */
	std::vector<uint8_t> jbwt_seq;
	
	/**
	 * @brief FIXME: this is just temperarily storing the real size of the genome
	 */
	INTTYPE _realSize = 0;
	
	/**
	 * @brief starting site -> chromosome
	 */
	std::map <INTTYPE, std::string> chr_start_pos {};
	
	/**
	 * @brief chr -> chr size
	 */
	std::map <std::string, INTTYPE> chr_length {};
	
	/**
	 * @brief unambiguous segment sequence starting position of each chr
	 */
	std::map <INTTYPE, INTTYPE > chr_umbiguous_starting_length {}; 
	
	
	Aligner_table(INTTYPE iv)
		: interval(iv)
		, char_size(256) 
		, c_function(char_size, 0)//, occ_function(bwt_size/interval+1, std::vector<INTTYPE>(char_size, 0))//, location_table(bwt_size/interval+1,0)
		, c_functions()
		, mtable(256)
		, occ_jbwt(256, std::vector< std::vector<INTTYPE> >(5,std::vector<INTTYPE>(256,0) ) )
		, occ_char({'A','C','G','T'})
	{
		set_interval(interval);
		mtable['A']=0;
		mtable['C']=1;
		mtable['G']=2;
		mtable['T']=3;
	}
	Aligner_table()
		: interval(0)
		, char_size(256) 
		, c_function(char_size, 0)//, occ_function(bwt_size/interval+1, std::vector<INTTYPE>(char_size, 0))//, location_table(bwt_size/interval+1,0)
		, c_functions()
		, mtable(256)
		, occ_jbwt(256, std::vector< std::vector<INTTYPE> >(5,std::vector<INTTYPE>(256,0) ) )
		, occ_char({'A','C','G','T'})
	{
		mtable['A']=0;
		mtable['C']=1;
		mtable['G']=2;
		mtable['T']=3;
	}
	


	void readChrStartPos (const std::string& chrStartPosFile) {
		std::ifstream in {chrStartPosFile};
		std::string line {}, chr {};
		INTTYPE startPos {0};
		while (getline (in, line)) {
			std::stringstream ss {line};
			ss >> chr >> startPos;
			chr_start_pos.insert (std::make_pair (startPos, chr));
		}
	}

	void readChrLen (const std::string& chrLenFile) {
		std::ifstream in {chrLenFile};
		std::string line {}, chr {};
		INTTYPE length {0};
		while (getline (in, line)) {
			std::stringstream ss {line};
			ss >> chr >> length;
			// FIXME: replace _realSize...
			_realSize += length;
			if (chr_length.find (chr) == chr_length.end())
				chr_length.insert (std::make_pair (chr, length));
			else {
				std::cerr << "Error: duplicated chromosome name" << std::endl;
				exit (1);
			}
		}
	}
	void readNPosLen (const std::string& fileName) {
		
		std::ifstream fp(fileName, std::ios::binary);
		boost::archive::binary_iarchive archive_fp( fp );
		archive_fp & chr_umbiguous_starting_length;
		fp.close();
		/*
		boost::iostreams::filtering_istream fis;
		fis.push (boost::iostreams::zlib_decompressor());
		fis.push (boost::iostreams::file_source (fileName));
		boost::archive::binary_iarchive iar (fis);
		iar >> chr_umbiguous_starting_length;
		*/
//		std::ifstream in {fileName};
//		uint64_t a, b;
//		while (in.good ()) {
//			in >> a >> b;
//			chr_umbiguous_starting_length.insert (std::make_pair (a,b));
//		}
	}
	void set_interval(INTTYPE iv)
	{
		interval = iv;
		
		if(interval == 2)
			powerV = 1;
		else if(interval == 4)
			powerV = 2;
		else if(interval == 8)
			powerV = 3;
		else if(interval == 16)
			powerV = 4;
		else if(interval == 32)
			powerV = 5;
		else if(interval == 64)
			powerV = 6;
		else if(interval == 128)
			powerV = 7;
		else if(interval == 256)
			powerV = 8;
		else if(interval == 512)
			powerV = 9;
		else if(interval == 1024)
			powerV = 10;
		
	}
	
	
	/**
	 * @fn void Aligner_table::using_jbwt (void)
	 * @brief 建構 occ_jbwt 與 jbwt_idx_char 表
	 * @return void
	 */
	void using_jbwt()
	{
		std::string all_char("ACGT");
		for( char c : all_char)
		{
			for( std::pair< const uint8_t, std::array<char, 4> > &byte : jbwt->byte2word )
			{
				std::vector<INTTYPE> char_number(5,0);
				//for(char c_in_array : byte.second)
				for(int i(0); i<4; ++i)
				{
					char c_in_array(byte.second[i]);	
					if(c_in_array == c)
					{
						for(int j(0); j<5; ++j)
						{
							if( i < j)
								++char_number[j];
						}
					}
				}
				for(int j(0); j<5; ++j)
				{
					occ_jbwt[c][j][byte.first] = char_number[j];
				}
			}
		}
		
		jbwt_idx_char = "";
		for(int i(0); i<256; i++)
		{
			char w1 = all_char[ (i&255) >> 6 ];
			char w2 = all_char[ (i&63) >> 4 ];
			char w3 = all_char[ (i&15) >> 2 ];
			char w4 = all_char[ (i&3) >> 0 ];
			jbwt_idx_char += w1;
			jbwt_idx_char += w2;
			jbwt_idx_char += w3;
			jbwt_idx_char += w4;
		}
		
		std::cerr << "jbwt ok" << std::endl;
		
		//
	}

	/**
	 * @fn void Aligner_table::saveTable (void)
	 * @brief 將所有Aligner會用到的表儲存archive到檔案
	 * @param[in] filename 儲存的檔案名稱
	 * @return void
	 */
	void saveTable(std::string filename)
	{
		std::ofstream fp(filename, std::ios::binary);
		boost::archive::binary_oarchive archive_fp( fp );
		archive_fp & interval;
		archive_fp & c_function;
		archive_fp & c_functions;
		//archive_fp & c_function_sp;
		archive_fp & occ_function;
		archive_fp & location_table;
		archive_fp & fbwt;
		archive_fp & first_location;
		archive_fp & occ_jbwt;
		archive_fp & jbwt->seq_;
		archive_fp & jbwt_idx_char;
		fp.close();
	}
	
	/**
	 * @fn void Aligner_table::readTable (void)
	 * @brief 將所有Aligner會用到的表從檔案讀取進記憶體
	 * @param[in] filename 讀取的檔案名稱
	 * @return void
	 */
	void readTable(std::string filename)
	{
		std::ifstream fp(filename, std::ios::binary);
		boost::archive::binary_iarchive archive_fp( fp );
		archive_fp & interval;
		archive_fp & c_function;
		archive_fp & c_functions;
		//archive_fp & c_function_sp;
		archive_fp & occ_function;
		archive_fp & location_table;
		archive_fp & fbwt;
		archive_fp & first_location;
		archive_fp & occ_jbwt;
		archive_fp & jbwt_seq;
		archive_fp & jbwt_idx_char;
		fp.close();
		
		set_interval(interval);
	}
	
	
	
	/**
	 * @fn void Aligner_table::createAllTable (void)
	 * @brief 會根據排序過後的bwt index 與原始genome，建構出所有 Searcher 需要用到的表
	 * @tparam SEQTYPE genome sequence 型別
	 * @param[in] seq 原始 genome sequence
	 * @param[in] filenames split sort後，儲存起來的bwt index檔案集合
	 * @return void
	 */
	template<class SEQTYPE>
	void createAllTable(SEQTYPE &seq, std::vector<std::string>& filenames)
	{
	
		jbwt = std::shared_ptr<JBit>( new JBit( seq.size() ) );
		
		/**
		 * @brief occ_function 為二元vector，256 (包含 ACGT) * 數量(genome length / interval +1)
		 */
		occ_function.resize(char_size, std::vector<INTTYPE>() );//256
		
		/**
		 * @brief location_table 數量(genome length / interval +1)
		 */
		location_table.reserve(seq.size()/interval+1);
		
		INTTYPE idx(0);
		char tmp_char('$'), bwt_char;
		std::string tmp_str, tmp_strs("");
		INTTYPE tmp_str_i(0), tmp_strs_i(0);
		
		INTTYPE tmp_cs(0);
		std::vector<INTTYPE> tmp_occ_count(char_size, 0);
		fbwt.resize( seq.size() ,0);
		
		
		/**
		 * @brief c_functions 多字元 C 表，長度
		 */
		INTTYPE c_functions_interval(12);
		
		c_functions.resize( std::pow(4,c_functions_interval) +1,0);
		
		
		char tmp_c_char='\0';
		uint8_t tmp_c_int = 0;
		
		
		for(std::string& filename : filenames)
		{
			/**
			 * @brief 讀取 split sort 後儲存的 bwt index
			 */
			std::ifstream in (filename, std::ios::binary);
			boost::archive::binary_iarchive tmp_archive(in);	
			std::vector<INTTYPE> sorted_table;
			tmp_archive & sorted_table;
			in.close();
			
			
			
			for(INTTYPE c_idx : sorted_table)
			//for(INTTYPE c_idx : seq_table)
			//for(INTTYPE i(1); i<seq_table.size(); ++i) // no first line ($...)
			{
				/**
				 * @brief 取得 BWT char
				 */
				if(c_idx == 0)
				{
					bwt_char = 'A'; //$
					first_location = idx;
				}
				else
					bwt_char = seq[c_idx-1];
				
				//bwt += bwt_char;
				jbwt->push_back(bwt_char);
				
				
				/**
				 * @brief 取得 bwt index trace back 幾個字後，location table 一定有記錄
				 */
				fbwt[idx] = (c_idx & (interval-1));
				
				
				
				/**
				 * @brief 記錄排序後 suffix 第一個字，出現的位置，C 表
				 */
				if(seq[c_idx] != tmp_char)
				{
					tmp_char = seq[c_idx];
					std::cerr << "ooo - " << tmp_char << " " << (INTTYPE)tmp_char << " " << idx << std::endl;
					c_function[ (INTTYPE)tmp_char ] = idx;
				}
				
				/**
				 * @brief 記錄排序後 suffix 前N個字，出現的位置，C 表，多字版本
				 */
				//C functions
				//tmp_strs = seq.substr(c_idx, c_functions_interval);
				tmp_strs_i = c_idx;
				//if(tmp_strs != tmp_str && tmp_strs.size() == c_functions_interval)
				if( str_idx_compare(seq, tmp_strs_i, tmp_str_i, c_functions_interval) )
				{
					
					tmp_cs = 0;
					int is_exist_$(0);
					for(INTTYPE i(0); i < c_functions_interval; ++i)
					{
						//if(tmp_strs[i] == '$' )
						if(seq[tmp_strs_i+i] == '$')
						{
							is_exist_$ = 1;
							break;
						}
						//tmp_cs += mtable[ tmp_strs[ i ] ] * std::pow(4,(c_functions_interval-1-i)) ;
						tmp_cs += mtable[ seq[tmp_strs_i+i] ] * std::pow(4,(c_functions_interval-1-i)) ;
					}
					if( !is_exist_$ )
					{
						c_functions[ tmp_cs ] = idx;
						tmp_str = tmp_strs;
						tmp_str_i = tmp_strs_i;
					}
				}
				
				/**
				 * @brief 記錄 OCC表，每個字元 ACGT都要記錄
				 */
				// OCC function
				if( (idx & (interval-1) ) == 0)
				{
					//for (int v(0); v < occ_char.size(); ++v)
					//for(INTTYPE v(0); v < char_size; ++v)
					for (int v(0); v < 4; ++v)
					{
						occ_function[ occ_char[v] ].push_back(tmp_occ_count[ occ_char[v] ]);
						//occ_function[ v ].push_back(tmp_occ_count[v]);//==0? 0: tmp_occ_count[v]-1;
					}
				}
				/**
				 * @brief 記錄 OCC表，幕後記錄功臣
				 */
				tmp_occ_count[ bwt_char ] ++;
				
				/**
				 * @brief 記錄location表
				 */
				//location table
				if( (c_idx & (interval-1) ) == 0)
				{
					location_table.push_back( {idx, c_idx} );
				}
				
				idx++;
			}
		}
		
		jbwt->last_push_back();
		c_function[c_function.size()-1] = seq.size();
		INTTYPE tmp( 0 );
		
		/**
		 * @brief C 表有 256個字元，但實際記錄只有 ACGT，searcher再搜尋的時候是，字元++ (A++ => B)，所以要在字元間補齊
		 */
		for(INTTYPE i(c_function.size()-1); i > 0; --i)
		{
			//std::cerr << "c function : i :" << i << " c : " << c_function[i] << std::endl;
			if(c_function[i] != 0) // magic number
				tmp = c_function[i];
			//else
			//	occ_function[i].clear();
			c_function[i] = tmp;
			//std::cerr << "c function : i :" << i << " c : " << c_function[i] << std::endl;
		}
		
		/**
		 * @brief C 表最後一個字，補seq.size
		 */
		c_functions[c_functions.size()-1] = seq.size();
		
		/**
		 * @brief C 表多字元版本，有4^N個字元，但實際記錄有些為 0，searcher再搜尋的時候是，字元++ (AAA++ => AAC)，所以要在字串計數為0的補上數字
		 */
		tmp = c_functions[c_functions.size()-1];
		for(INTTYPE i(c_functions.size()-1); i > 0; --i)
		{
			if(c_functions[i] != 0)
				tmp = c_functions[i];
			c_functions[i] = tmp;
		}
		std::cerr << "Creating Jbwt..." << std::endl;
		
		/**
		 * @brief 建構 occ_jbwt表與必要表
		 */
		using_jbwt();
	}


	/**
	 * @fn void Aligner_table::str_idx_compare (void)
	 * @brief 加速用比較 str idx 大小，內部使用
	 * @tparam SEQTYPE genome sequence 型別
	 * @param[in] a sequence a 位置為起始
	 * @param[in] b sequence b 位置為起始
	 * @param[in] len 比較幾個字，超過為平手
	 * @return bool
	 */
	template<class SEQTYPE>
	inline bool str_idx_compare(SEQTYPE &seq, INTTYPE a, INTTYPE b, INTTYPE len)
	{
		if(a+len >= seq.size() || b+len >= seq.size())
			return false;
		if(a==b)
			return false;
		for(INTTYPE i=0; i<len; i++)
		{
			if(seq[a+i] != seq[b+i])
				return true;
		}
		return false;
	}
	
	/**
	 * @fn void Aligner_table::get_c (INTTYPE)
	 * @brief 查C表，回傳某字元起始的 bwt index 
	 * @param[in] i bwt index
	 * @return INTTYPE bwt index 
	 */
	inline INTTYPE get_c(INTTYPE i) const
	{
		//clock_t start = clock();
		return c_function[ (INTTYPE)bwt[i] ];
		//clock_t end = clock();
	}
	
	/**
	 * @fn void Aligner_table::get_c (char)
	 * @brief 查C表，回傳某字元起始的 bwt index 
	 * @param[in] c A,C,G or T 字元
	 * @return INTTYPE ，查C表，回傳某字元起始的 bwt index 
	 */
	inline INTTYPE get_c(char c) const 
	{
		return c_function[ c ];
	}
	
	/**
	 * @fn void Aligner_table::get_jbwt_char (INTTYPE)
	 * @brief 解壓縮。從 jbwt 壓縮字串，取得某 bwt index 位置的字元
	 * @param[in] i bwt index
	 * @return char 字元 A, C, G or T
	 */
	inline char get_jbwt_char(INTTYPE i) const 
	{
		INTTYPE jbwt_idx( (i >> 2) );
		uint8_t chars = jbwt_seq[jbwt_idx];
		return jbwt_idx_char[ (chars << 2) + (i & 3) ];
		
	}
	
	/**
	 * @fn void Aligner_table::get_occ_using_jbwt (INTTYPE, char, int)
	 * @brief 壓縮版本，取得某bwt index位置之上有幾個 A, C, G or T。先查表，取得最近距離數值後，再重新累加計算
	 * @param[in] i bwt index
	 * @param[in] c 看某個字，沒給值則為 bwt index上的字
	 * @return INTTYPE 累積數量
	 */
	inline INTTYPE get_occ_using_jbwt(INTTYPE i, char c = '\0') const
	{
			
		INTTYPE pre_interval( (i >> powerV) );
		INTTYPE j( (pre_interval << powerV) );
		INTTYPE count( occ_function[ c ][ pre_interval ] );
		if(i > first_location &&	c =='A')
			--count;
		if(i == j)
			return count;
		
		INTTYPE jbwt_idx (j>>2), jbwt_idx_end(i>>2);
		
		INTTYPE tmp_count = count; 
		
		for( ; jbwt_idx != jbwt_idx_end; j+=4, ++jbwt_idx)
		{
			//count += occ_jbwt [ c ][4] [ jbwt->seq_[ jbwt_idx ] ];
			count += occ_jbwt [ c ][4] [ jbwt_seq[ jbwt_idx ] ];
			
		}
		//count += occ_jbwt [ c ][i-j] [ jbwt->seq_[ jbwt_idx ] ];
		count += occ_jbwt [ c ][i-j] [ jbwt_seq[ jbwt_idx ] ];
		return count;
	}
	
	/**
	 * @fn void Aligner_table::get_occ (INTTYPE, char, int)
	 * @brief 未壓縮版本，取得某bwt index位置之上有幾個 A, C, G or T。先查表，取得最近距離數值後，再重新累加計算
	 * @param[in] i bwt index
	 * @param[in] c 看某個字，沒給值則為 bwt index上的字
	 * @return INTTYPE 累積數量
	 */
	inline INTTYPE get_occ(INTTYPE i, char c = '\0') const
	{
		//std::cerr <<"i : "<<i<< " , (i & (interval-1) ) : "<< (i & (interval-1) ) << std::endl;
		//clock_t start = clock();
		if (c=='\0')
			c=bwt[i];
			
			INTTYPE pre_interval( (i >> powerV) );
			INTTYPE j( (pre_interval << powerV) );
			INTTYPE count( occ_function[ (INTTYPE) c ][ pre_interval ] );
			if(i > first_location &&	c =='A')
				--count;
			if(i == j)
				return count;
			//std::cerr << "OOori count " << count << std::endl;
			
			for(; j != i; ++j)
			{
				if (bwt[j] == c )
					++count;
			}
			return count;	
	}
	
	/**
	 * @fn void Aligner_table::back_tracking_using_jbwt (INTTYPE)
	 * @brief 壓縮版本，解壓縮BWT，往前 trace 一個字
	 * @param[in] i bwt index
	 * @return INTTYPE 前面一個字的新 bwt index
	 */
	inline INTTYPE back_tracking_using_jbwt(INTTYPE i) const
	{
		char c = get_jbwt_char(i);
		return get_c(c) + get_occ_using_jbwt(i, c);
		//return get_c(i) + get_occ_using_jbwt(i);
	}
	
	/**
	 * @fn void Aligner_table::back_tracking (INTTYPE)
	 * @brief 未壓縮版本，解壓縮BWT，往前 trace 一個字
	 * @param[in] i bwt index
	 * @return INTTYPE 前面一個字的新 bwt index
	 */
	inline INTTYPE back_tracking(INTTYPE i) const
	{
		//std::cerr << "i: " << i << "c: " << get_c(i) << " occ: " << get_occ(i) << std::endl;;
		return get_c(i) + get_occ(i);
	}
};


int Aligner_table<Aligner_types::BWT_Aligner>::is_table_loaded = 0;


#endif
