/// @file Searcher.hpp
#ifndef SEARCHER_HPP_
#define SEARCHER_HPP_
#include <algorithm>
#include <ctime>
#include <fstream>
#include <iostream>
#include <map>
#include <random>
#include <string>
#include <tuple>
#include <utility>
#include <ctime>

#include "boost/serialization/vector.hpp"
#include "boost/serialization/utility.hpp"
#include "boost/archive/binary_oarchive.hpp"
#include "boost/archive/binary_iarchive.hpp"
#include "boost/unordered_map.hpp"

#include "../compression/abit.hpp"
#include "../compression/jbit.hpp"
#include "../constant_def.hpp"
//#include "../format/abwt_fastq.hpp"
#include "../format/sam.hpp"

#include "job_distributer.hpp"
		
//template < Aligner_types AlignerType, ParallelTypes ParallelType, class AlignerTableType>
template< Aligner_types AlignerType
				 ,ParallelTypes ParallelType
				 ,class AlignerTableType
				 //,class QueryParserType
				 ,class FormatType
				 ,typename SearchReturnType
				 ,class... ARGS
				>
class Searcher_impl
{};



//template <class AlignerTableType>
//class Searcher_impl< AlignerTableType, Aligner_types::BWT_Aligner>
template< ParallelTypes ParallelType
				 ,class AlignerTableType
				 //,class QueryParserType
				 ,class FormatType
				 ,typename SearchReturnType
				 ,class... ARGS
				>
class Searcher_impl< Aligner_types::BWT_Aligner
							 			,ParallelType
							 			,AlignerTableType
							 			//,QueryParserType
							 			,FormatType
							 			,SearchReturnType
							 			,ARGS...
							 		 >

{
protected:
	AlignerTableType &abwt_table_;
	const INTTYPE &c_functions_interval;
	
	std::array<INTTYPE,256> mtable_;
	
	Searcher_impl(AlignerTableType &table, const INTTYPE &c_f_interval)
		: abwt_table_ (table)
		, c_functions_interval(c_f_interval)
	{
		mtable_['A'] = 0;
		mtable_['C'] = 1;
		mtable_['G'] = 2;
		mtable_['T'] = 3;
	}
	
	inline void init_exact_match( std::pair<INTTYPE, INTTYPE> &start_end_pos_, char c )
	{
		start_end_pos_.first = abwt_table_.c_function[ c ];
		start_end_pos_.second = abwt_table_.c_function[ c+1 ];
	}
	inline void init_exact_match( std::pair<INTTYPE, INTTYPE> &start_end_pos_, std::string tmp_str8 )
	{
		INTTYPE tmp_cs(0);
		for(INTTYPE i(0); i < c_functions_interval; ++i)
		{
			//tmp_cs += mtable_[ tmp_str8[ i ] ] << ((11-i)<<1); //slower.....@@
			//tmp_cs += mtable_[ tmp_str8[ i ] ] << ((c_functions_interval-1-i)<<1); //slower.....@@
			tmp_cs += mtable_[ tmp_str8[ i ] ] * std::pow(4,(c_functions_interval-1-i)) ;
			//std::cerr << tmp_str8[ i ] << " * " << std::pow(mtable_.size(),(7-i)) << " = " << tmp_c8 << std::endl;
		}
		//std::cerr << "tmp_cs " << tmp_cs << " size " << abwt_table_.c_functions.size() << std::endl;
		start_end_pos_.first = abwt_table_.c_functions[ tmp_cs ];
		start_end_pos_.second = abwt_table_.c_functions[ tmp_cs+1 ];
	}
	inline void init_exact_match( std::pair<INTTYPE, INTTYPE> &start_end_pos_, INTTYPE tmp_cs )
	{
		start_end_pos_.first = abwt_table_.c_functions[ tmp_cs ];
		start_end_pos_.second = abwt_table_.c_functions[ tmp_cs+1 ];
	}
	inline void exec_exact_match( std::pair<INTTYPE, INTTYPE> &start_end_pos_, char c )
	{
		start_end_pos_.first = abwt_table_.c_function[ c ] + abwt_table_.get_occ_using_jbwt( start_end_pos_.first, c );
		start_end_pos_.second = abwt_table_.c_function[ c ] + abwt_table_.get_occ_using_jbwt( start_end_pos_.second, c );
		//INTTYPE tmp_a = start_end_pos_.first;
		//INTTYPE tmp_b = start_end_pos_.second;
		
		//INTTYPE a = abwt_table_.c_function[ c ] + abwt_table_.get_occ_using_jbwt( start_end_pos_.first, c );
		//INTTYPE b = abwt_table_.c_function[ c ] + abwt_table_.get_occ_using_jbwt( start_end_pos_.second, c );
		
		//start_end_pos_.first = abwt_table_.c_function[ c ] + abwt_table_.get_occ( start_end_pos_.first, c );
		//start_end_pos_.second = abwt_table_.c_function[ c ] + abwt_table_.get_occ( start_end_pos_.second, c );

		
	}
	
	inline INTTYPE find_nearest_mark (std::pair<INTTYPE, INTTYPE> &start_end_pos_, INTTYPE trace_point)
	{
		
		//INTTYPE tmp = trace_point;
		INTTYPE traceback_count(0);
		INTTYPE flocation = abwt_table_.fbwt[trace_point];
		
		for(INTTYPE i(0); i<flocation;++i)
		{
			//trace_point = abwt_table_.back_tracking(trace_point);
			trace_point = abwt_table_.back_tracking_using_jbwt(trace_point);
			++traceback_count;
		}
		
		auto pp = std::lower_bound( 
			abwt_table_.location_table.begin(), 
			abwt_table_.location_table.end(), 
			std::pair<INTTYPE, INTTYPE>{trace_point, 0}, 
				[]( const std::pair<INTTYPE, INTTYPE>& a, const std::pair<INTTYPE, INTTYPE>& b)
				{
					return a.first < b.first;
				} 
		);
		traceback_count += pp->second;
		return traceback_count;
		
		while(1)//trace to the nearest upstream location_table
		{
			
			//std::cerr << "last trace_point : " << trace_point << " flocation : " << flocation << std::endl;
			
			auto pp = std::lower_bound( 
				abwt_table_.location_table.begin(), 
				abwt_table_.location_table.end(), 
				std::pair<INTTYPE, INTTYPE>{trace_point, 0}, 
					[]( const std::pair<INTTYPE, INTTYPE>& a, const std::pair<INTTYPE, INTTYPE>& b)
					{
						return a.first < b.first;
					} 
			);
			if ( pp->first == trace_point )
			{
				//std::cerr << "traceback_count : " << traceback_count << std::endl;

				//traceback_count += hit.first->second;
				traceback_count += pp->second;
				break;
			}
			else//no hit yet
			{
				//trace_point = abwt_table_.back_tracking(trace_point);
				trace_point = abwt_table_.back_tracking_using_jbwt(trace_point);
				traceback_count++;

			}

		}
		return traceback_count;
	}
		
	inline void push_result(std::vector<INTTYPE>& result, INTTYPE pos_f, INTTYPE pos_e)
	{
		if (pos_e - pos_f > 100)
			return;
		for (int i = pos_f; i < pos_e; i++)
		{
			result.push_back( find_nearest_mark(i) );
		}
	}
	
public:
	static std::mutex mux_;
	void load_table(std::string prefix_name)
	{
	std::lock_guard <std::mutex> lk (mux_);
		if(abwt_table_.is_table_loaded == 0)
		{
			abwt_table_.is_table_loaded=1;
			prefix_name += ".";
			abwt_table_.readTable(prefix_name + "t_table.bwt");
			abwt_table_.readNPosLen (prefix_name + "NposLen.z");
			abwt_table_.readChrStartPos (prefix_name + "chrStart");
			abwt_table_.readChrLen (prefix_name + "chrLen");
		}
	}	
};

//search_policy
//template<class AlignerTableType, int AlignerType, int SearcherPolicy ,typename... ARGS>
template< Aligner_types AlignerType
				 ,Searcher_types SearcherPolicy
				 ,ParallelTypes ParallelType
				 ,class AlignerTableType
				 //,class QueryParserType
				 //,class FormatType
				 ,typename SearchReturnType
				 ,typename... ARGS
				>
class Searcher
{};






/**
 * @struct 
 * 
 * @brief for tailer tailer
 *  
 * @tparam
 *  
 */
//template < class AlignerTableType, typename... ARGS >
//class Searcher <AlignerTableType, Aligner_types::BWT_Aligner, Searcher_types::Tailer, ARGS...>
template< ParallelTypes ParallelType
				 ,class AlignerTableType
				 //,class QueryParserType
				 ,class FormatType
				 ,typename SearchReturnType
				 ,class... ARGS
				>
class Searcher< Aligner_types::BWT_Aligner
							 ,Searcher_types::Tailer
							 ,ParallelType
							 ,AlignerTableType
							 //,QueryParserType
							 ,FormatType
							 ,SearchReturnType
							 ,ARGS...
							>
	:public Searcher_impl< Aligner_types::BWT_Aligner
							 					,ParallelType
							 					,AlignerTableType
							 					//,QueryParserType
							 					,FormatType
							 					,SearchReturnType
							 					,ARGS...
							 				 >
{
//private:
public:
	const AlignerTableType	&abwt_table_;
	const INTTYPE c_functions_interval;
	std::mutex reader_mutex, writer_mutex;	
	
public:

	
	
	typedef Searcher_impl< Aligner_types::BWT_Aligner
							 					,ParallelType
							 					,AlignerTableType
							 					//,QueryParserType
							 					,FormatType
							 					,SearchReturnType
							 					,ARGS...
							 				 > SearchImplType;
	
	/// @brief 定義 Input type 單筆
	typedef std::map<int, FormatType > IN_DATA_TYPE_1;						 				 
	
	/// @brief 定義 Input type 多筆
	typedef std::map<int, std::vector<FormatType> > IN_DATA_TYPE_2;
	
	/// @brief 定義 Output type
	typedef std::vector< Sam<> > OUT_DATA_TYPE_1;
	
	/// @brief 定義 Output type
	typedef std::stringstream OUT_DATA_TYPE_2;
	
							 				 
	Searcher(AlignerTableType& table)
		: SearchImplType (table, c_functions_interval)
		, abwt_table_(table)
		, c_functions_interval(12)

	{}
	
	/*@ a version of exec_exact_match that return the previous value of start_end_pos_ */
	std::pair<INTTYPE, INTTYPE> exec_exact_match2 (std::pair<INTTYPE,INTTYPE> &start_end_pos_, char c ) const 
	{
		auto old_start_end_pos_ = start_end_pos_;
		start_end_pos_.first = abwt_table_.c_function[ c ] + abwt_table_.get_occ_using_jbwt( start_end_pos_.first, c );
		start_end_pos_.second = abwt_table_.c_function[ c ] + abwt_table_.get_occ_using_jbwt( start_end_pos_.second, c );
		return old_start_end_pos_;
	}
	
	// tailing searching version for dual BWT
	void start_tailing_match_Dual (FormatType & fq, OUT_DATA_TYPE_1 &out, int minimalPrefixLength, int limitNumber = 0)
	{
		std::pair<INTTYPE,INTTYPE> start_end_pos_ ({0,0});
		std::string _query = std::move(fq.getSeq ());
		
		//std::cout << "read : " << _query << std::endl;
		/* reverse complement query string */
		std::string query {_query.crbegin(), _query.crend()};
		for (auto & c : query) {
			switch (c) {
			case 'A': c = 'T'; break;
			case 'T': c = 'A'; break;
			case 'C': c = 'G'; break;
			case 'G': c = 'C'; break;
			default : throw "illegal char";
			}
		} /* end of RC */
		
		
		bool isRC = false;
		/* the front c_functions_interval has to be exact match */
		this->init_exact_match( start_end_pos_, query.substr (query.size() - c_functions_interval, c_functions_interval) );
		/* if not found, exit */

		if ( start_end_pos_.first >= start_end_pos_.second) {
			return;
		}

		/* queryPosition need to be tested again */
		int queryPosition = query.length() -1 - c_functions_interval;
		/* last step need to be recorded */
		auto last_start_end_pos_ = start_end_pos_;
		for (; queryPosition >= 0 && start_end_pos_.first < start_end_pos_.second; --queryPosition) {
			last_start_end_pos_ = exec_exact_match2(start_end_pos_, query[queryPosition]);
		}
		
/// begin recording tailing
		if (start_end_pos_.first >= start_end_pos_.second) {
			++queryPosition; /// substract an extra one when exiting the loop, so add it back
			auto NH_tag = last_start_end_pos_.second - last_start_end_pos_.first; // record the theoretically hits
			
			/// @brief 如果一個 reads align到太多位置，則略過。此判斷為 tailing 的位置
			if(limitNumber != 0 && NH_tag > limitNumber )
			{
				return;
			}
			
			for (INTTYPE i = last_start_end_pos_.first; i < last_start_end_pos_.second; i++) {
				auto position = this->find_nearest_mark(start_end_pos_, i);
				auto prefixMatchLen = _query.size() - 1 - queryPosition;

				if (prefixMatchLen < minimalPrefixLength)
					continue;

				if (position >= this->abwt_table_._realSize && position < (abwt_table_._realSize<<1)) { /// the second comparsion is to suppress weird bug of TTTTTTTTTTTT mapping to position == 2*abwt_table_._realSize
					isRC = true;
					position = this->abwt_table_._realSize*2 - position - prefixMatchLen;
				} else if (position < this->abwt_table_._realSize) {
					isRC = false;
				} else {
					continue;
				}
				if (!isRC) { /// same as start_tailing_match_AS
					auto tailSeq = _query.substr(prefixMatchLen);
					auto lowerIter = this->abwt_table_.chr_start_pos.upper_bound (position);
					std::advance (lowerIter, -1);
					auto chr = lowerIter->second;

					auto lowerIter3 = this->abwt_table_.chr_start_pos.upper_bound (position + prefixMatchLen -1);
					std::advance (lowerIter3, -1);
					auto chr3 = lowerIter3->second;
					if (chr != chr3) continue;

					auto NLowerIter = this->abwt_table_.chr_umbiguous_starting_length.upper_bound (position);
					std::advance (NLowerIter, -1);
					auto NLowerIter3 = this->abwt_table_.chr_umbiguous_starting_length.upper_bound (position + prefixMatchLen -1);
					std::advance (NLowerIter3, -1);
					if (NLowerIter != NLowerIter3) continue;

					position = position - lowerIter->first + NLowerIter->second;
					//TODO: redefine MAPQ
					
					out.emplace_back
					//out << Sam<> 
					(
						std::move(
							std::make_tuple(
								std::move(fq.getName()),
								SAM_FLAG::REVERSE_COMPLEMENTED,
								std::move (chr),
								position + 1,
								255 - queryPosition - 1,
								std::to_string (prefixMatchLen) + 'S' + std::to_string (queryPosition+1) + 'M',
								"*",
								0,
								0,
								query,
								//std::move(fq.getQuality ()),
								//last_start_end_pos_.second - last_start_end_pos_.first,
								std::move(fq.getRevQuality()),
UserDefineTags< boost::mpl::string<'NH'>, boost::mpl::string<'TL'> > (NH_tag, tailSeq)
//UserDefineContent (NH_tag, std::move(tailSeq) )
//								NH_tag,
//								std::move(tailSeq)
							)
						)
					);
					
				} else { /// same as start_tailing_match_S
					auto tailSeq = _query.substr(prefixMatchLen);

					auto lowerIter = this->abwt_table_.chr_start_pos.upper_bound (position);
					std::advance (lowerIter, -1);
					auto chr = lowerIter->second;
					auto lowerIter3 = this->abwt_table_.chr_start_pos.upper_bound (position + prefixMatchLen -1);
					std::advance (lowerIter3, -1);
					auto chr3 = lowerIter3->second;
					if (chr != chr3) continue;

					auto NLowerIter = this->abwt_table_.chr_umbiguous_starting_length.upper_bound (position);
					std::advance (NLowerIter, -1);
					auto NLowerIter3 = this->abwt_table_.chr_umbiguous_starting_length.upper_bound (position + prefixMatchLen -1);
					std::advance (NLowerIter3, -1);
					if (NLowerIter != NLowerIter3) continue;

					position = position - lowerIter->first + NLowerIter->second;
					//TODO: redefine MAPQ
					
					out.emplace_back
					//out << Sam<> 
					(
						std::move(
							std::make_tuple(
								std::move(fq.getName ()),
								SAM_FLAG::MAPPED,
								std::move (chr),
								position+1,
								255 - queryPosition - 1,
								std::to_string (prefixMatchLen) + 'M' + std::to_string (queryPosition+1) + 'S',
								"*",
								0,
								0,
								_query,
								std::move(fq.getQuality ()),
								//last_start_end_pos_.second - last_start_end_pos_.first,
UserDefineTags< boost::mpl::string<'NH'>, boost::mpl::string<'TL'> > (NH_tag, tailSeq)
//UserDefineContent (NH_tag, std::move(tailSeq) )
//								NH_tag,
//								std::move(tailSeq)
							)
						)
					);
					
					
				}
			}
			return;
		}
/// found perfect match
		if (queryPosition == -1) {
			auto NH_tag = start_end_pos_.second - start_end_pos_.first;
			/// @brief 如果一個 reads align到太多位置，則略過。此判斷為 prefect match 的最後位置
			if(limitNumber != 0 && NH_tag > limitNumber )
			{
				return;
			}
			
			for (INTTYPE i = start_end_pos_.first; i < start_end_pos_.second; i++) {
				auto position = this->find_nearest_mark(start_end_pos_, i);
				if (position >= this->abwt_table_._realSize) {
					isRC = true;
					position = this->abwt_table_._realSize*2 - position - _query.size ();
				} else {
					isRC = false;
				}
				if (!isRC) {
					auto lowerIter = this->abwt_table_.chr_start_pos.upper_bound (position);
					std::advance (lowerIter, -1);
					auto chr = lowerIter->second;

					auto lowerIter3 = this->abwt_table_.chr_start_pos.upper_bound (position + _query.size () - 1);
					std::advance (lowerIter3, -1);
					auto chr3 = lowerIter3->second;
					if (chr != chr3) continue;

					auto NLowerIter = this->abwt_table_.chr_umbiguous_starting_length.upper_bound (position);
					std::advance (NLowerIter, -1);

					auto NLowerIter3 = this->abwt_table_.chr_umbiguous_starting_length.upper_bound (position + _query.size () - 1);
					std::advance (NLowerIter3, -1);
					if (NLowerIter != NLowerIter3) continue;

					position = position - lowerIter->first + NLowerIter->second;
					
					out.emplace_back
					//out << Sam<> 
					(
						std::move(
							std::make_tuple(
								std::move(fq.getName ()),
								SAM_FLAG::REVERSE_COMPLEMENTED,
								std::move (chr),
								position+1,
								255,
								std::to_string (_query.size ()) + 'M',
								"*",
								0,
								0,
								query,
								//std::move(fq.getQuality ()),
								//last_start_end_pos_.second - last_start_end_pos_.first,
								std::move(fq.getRevQuality()),
UserDefineTags< boost::mpl::string<'NH'>, boost::mpl::string<'TL'> > (NH_tag, "")
//UserDefineContent (NH_tag, "" )
//								NH_tag,
//								""
							)
						)
					);
					
				} else {
					auto lowerIter = this->abwt_table_.chr_start_pos.upper_bound (position);
					std::advance (lowerIter, -1);
					auto chr = lowerIter->second;

					auto lowerIter3 = this->abwt_table_.chr_start_pos.upper_bound (position + _query.size () - 1);
					std::advance (lowerIter3, -1);
					auto chr3 = lowerIter3->second;
					if (chr != chr3) continue;

					auto NLowerIter = this->abwt_table_.chr_umbiguous_starting_length.upper_bound (position);
					std::advance (NLowerIter, -1);

					auto NLowerIter3 = this->abwt_table_.chr_umbiguous_starting_length.upper_bound (position + _query.size () - 1);
					std::advance (NLowerIter3, -1);
					if (NLowerIter != NLowerIter3) continue;

					position = position - lowerIter->first + NLowerIter->second;
					
					out.emplace_back
					//out << Sam<> 
					(
						std::move(
							std::make_tuple(
								std::move(fq.getName ()),
								SAM_FLAG::MAPPED,
								std::move (chr),
								position+1,
								255,
								std::to_string (_query.size ()) + 'M',
								"*",
								0,
								0,
								_query,
								std::move(fq.getQuality ()),
								//last_start_end_pos_.second - last_start_end_pos_.first,
UserDefineTags< boost::mpl::string<'NH'>, boost::mpl::string<'TL'> > (NH_tag, "")
//UserDefineContent (NH_tag, "")
//								NH_tag,
//								""
							)
						)
					);
					
				}
			}
			return ;
		}
	}

	
	/// @brief Out put type 1
	SearchReturnType
	search(IN_DATA_TYPE_2 &in_data, OUT_DATA_TYPE_1 & out_data, int nthreads, int minLen, int limitNumber = 0, int limitLength = 30)
	{
		
		Job_distributer_pipeline <ParallelType, std::vector<FormatType> , OUT_DATA_TYPE_1 > jd;
		jd.distribute_jobs(in_data[0], out_data, nthreads,
			[this, minLen, limitNumber, limitLength](FormatType &format_data, OUT_DATA_TYPE_1 &out_buffer)
			{
				
				/// @brief format_data 為 fastq or fasta...
				/// @brief out_buffer 在 tailer 強迫為 vector<Sam<>>
				/// @brief minLen 為 tailing 的長度
				/// @brief limitNumber 為限制一個 read 可以對到幾個地方，0為不限制
				/// @brief limitLength reads最長長度不能超過，超過就不 alignment
				
				if(std::get<1>(format_data.data).length() <= limitLength)
					this->start_tailing_match_Dual(format_data, out_buffer, minLen, limitNumber);
			}
		);
	}
	
/*	//
	std::tuple<std::string, std::vector< Sam<> > > *
	search(IN_DATA_TYPE_2 *in_data, int nthreads, int minLen, int limitNumber = 0, int limitLength = 30)
	{
		std::tuple<std::string, std::vector< Sam<> > > *results = new std::tuple<std::string, std::vector< Sam<> > >();
		//std::vector< Sam<> > *results = new std::vector< Sam<> >();
		
		Job_distributer_pipeline <ParallelType, std::vector<FormatType> , std::vector< Sam<> > > jd;
		jd.distribute_jobs((*in_data)[0], std::get<1>(*results), nthreads,
			[this, minLen, limitNumber, limitLength](FormatType &format_data, OUT_DATA_TYPE_1 &out_buffer)
			{
				/// @brief format_data 為 fastq or fasta...
				/// @brief out_buffer 在 tailer 強迫為 vector<Sam<>>
				/// @brief minLen 為 tailing 的長度
				/// @brief limitNumber 為限制一個 read 可以對到幾個地方，0為不限制
				/// @brief limitLength reads最長長度不能超過，超過就不 alignment
				
				if(std::get<1>(format_data.data).length() <= limitLength)
					this->start_tailing_match_Dual(format_data, out_buffer, minLen, limitNumber);
			}
		);
		std::get<0>(*results) = get_sam_header_string();
		return results;
	}
*/
	//std::tuple<std::string, std::vector< Sam<> > > *
	std::vector< Sam<> > *
	search(IN_DATA_TYPE_2 *in_data, int nthreads, int minLen, int limitNumber = 0, int limitLength = 30, int map_index=0)
	{
		std::vector< Sam<> > *results = new std::vector< Sam<> > ();
		//std::vector< Sam<> > *results = new std::vector< Sam<> >();
		
		Job_distributer_pipeline <ParallelType, std::vector<FormatType> , std::vector< Sam<> > > jd;
		jd.distribute_jobs((*in_data)[map_index], *results, nthreads,
			[this, minLen, limitNumber, limitLength](FormatType &format_data, OUT_DATA_TYPE_1 &out_buffer)
			{
				/// @brief format_data 為 fastq or fasta...
				/// @brief out_buffer 在 tailer 強迫為 vector<Sam<>>
				/// @brief minLen 為 tailing 的長度
				/// @brief limitNumber 為限制一個 read 可以對到幾個地方，0為不限制
				/// @brief limitLength reads最長長度不能超過，超過就不 alignment
				
				if(std::get<1>(format_data.data).length() <= limitLength)
					this->start_tailing_match_Dual(format_data, out_buffer, minLen, limitNumber);
			}
		);
		//*results = get_sam_header_string();
		return results;
	}

	
	/// @brief Out put type 2, output is string stream
	/*
	SearchReturnType
	search(IN_DATA_TYPE_2 *in_data, OUT_DATA_TYPE_2 & out_data, int nthreads, int minLen)
	{
		Job_distributer<ParallelType, VectorParser > jd;
		jd.distribute_jobs(*in_data, out_data, nthreads,
			[this, minLen](FormatType &fp_value, std::stringstream &out_buffer_stream)
			{
				this->start_tailing_match_Dual(fp_value, out_buffer_stream, minLen);
			}
		);
	}
	*/
	/*
	SearchReturnType
	search(std::vector<std::string> filelist, std::size_t nthreads, std::string out_filename, int minLen)
	{
		std::ofstream out {out_filename};
		write_sam_header(out);
		
		INTTYPE in_group_reads_number(2000);
		GlobalPool.ChangePoolSize(nthreads);
		
		QueryParserType file_parser(filelist);

		bool eof_flag(false);
		int file_idx(0);
		
		Job_distributer<ParallelType, QueryParserType > jd;
		jd.distribute_jobs(file_parser, out, nthreads,
			[this, minLen](typename QueryParserType::format_type &fp_value, std::stringstream &out_stream)
			{
				this->start_tailing_match_Dual(fp_value, out_stream, minLen);
			}
		);
		
		out.close();
	}
	*/
	inline std::string get_sam_header_string()
	{
		std::stringstream header;
		header << "@HD" << '\t' << "VN:1.0" << '\t' << "SO:unsorted\n";
		for (const auto& chrSizes : abwt_table_.chr_length)
		{
			header << "@SQ\tSN:" << chrSizes.first << "\tLN:" << chrSizes.second << '\n';
		}
		return header.str();
	}

	inline void write_sam_header(std::ostream& out)
	{
		out << "@HD" << '\t' << "VN:1.0" << '\t' << "SO:unsorted\n";
		for (const auto& chrSizes : abwt_table_.chr_length)
		{
			out << "@SQ\tSN:" << chrSizes.first << "\tLN:" << chrSizes.second << '\n';
		}
	}

	inline std::vector<std::string> get_chromosome_string()
	{
		std::vector<std::string> chr_vec;
		for (const auto& chrSizes : abwt_table_.chr_length)
			chr_vec.push_back (chrSizes.first);
		return chr_vec;
	}
};

/**
 * @struct 
 * 
 * @brief for exact_match
 *  
 * @tparam
 *  
 */
//template <class AlignerTableType, typename... ARGS >
//class Searcher <AlignerTableType, Aligner_types::BWT_Aligner, Searcher_types::Exact_match, ARGS...>

template< ParallelTypes ParallelType
				 ,class AlignerTableType
				 //,class QueryParserType
				 ,class FormatType
				 ,typename SearchReturnType
				 ,class... ARGS
				>
class Searcher< Aligner_types::BWT_Aligner
							 ,Searcher_types::Exact_match
							 ,ParallelType
							 ,AlignerTableType
							 //,QueryParserType
							 ,FormatType
							 ,SearchReturnType
							 ,ARGS...
							>
	:public Searcher_impl< Aligner_types::BWT_Aligner
							 					,ParallelType
							 					,AlignerTableType
							 					//,QueryParserType
							 					,FormatType
							 					,SearchReturnType
							 					,ARGS...
							 				 >
{
private:
	const AlignerTableType	&abwt_table_;
	const INTTYPE c_functions_interval;
	mutable std::pair<INTTYPE, INTTYPE> start_end_pos_;
	
	
public:
	typedef Searcher_impl< Aligner_types::BWT_Aligner
							 					,ParallelType
							 					,AlignerTableType
							 					//,QueryParserType
							 					,FormatType
							 					,SearchReturnType
							 					,ARGS...
							 				 > SearchImplType;
							 				 
	Searcher(AlignerTableType& table)
		: SearchImplType (table, c_functions_interval)
		, abwt_table_(table)
		, c_functions_interval(12)
	{}
	
	inline void start_exact_match( std::string& query, std::vector<INTTYPE>& result )
	{
		std::pair<INTTYPE,INTTYPE> start_end_pos_;
		//initialize exact matching process
		this->init_exact_match(start_end_pos_, query.substr(query.size()-c_functions_interval,c_functions_interval) );
		//init_exact_match( query.back() );

		//extending matching query
		//for (int i=query.length()-1-1; i>=0 && start_end_pos_.first < start_end_pos_.second; i--)
		for (int i=query.length()-1-c_functions_interval; i>=0 && start_end_pos_.first < start_end_pos_.second; i--)
		{
			//c = query[i];
			this->exec_exact_match(start_end_pos_, query[i]);
		}
		//once there is any hit, this loop will run and print every hits
		if (start_end_pos_.second - start_end_pos_.first > 100)
			return;

		for (INTTYPE i = start_end_pos_.first; i < start_end_pos_.second; i++)
		{
			result.push_back( this->find_nearest_mark(i) );
		}
	}
};

template< ParallelTypes ParallelType
                 ,class AlignerTableType
                 //,class QueryParserType
                 ,class FormatType
                 ,typename SearchReturnType
                 ,class... ARGS
                >
std::mutex 
Searcher_impl< Aligner_types::BWT_Aligner
							 			,ParallelType
							 			,AlignerTableType
							 			//,QueryParserType
							 			,FormatType
							 			,SearchReturnType
							 			,ARGS...
							 		 >::mux_;

#endif
