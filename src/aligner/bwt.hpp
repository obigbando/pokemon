/**
 *  @file abwt.hpp
 *  @brief 正常版本 BWT 演算法實作 與 壓縮版本演算法實作（壓縮版本速度極慢，目前已經不使用）\n
 *  @author C-Salt Corp.
 *  
 */
#ifndef BWT_HPP_
#define BWT_HPP_
#include <algorithm>
#include <cctype>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <fstream>
#include <functional>
#include <iostream>
#include <list>
#include <locale>
#include <map>
#include <queue>
#include <random>
#include <string>
#include <tuple>
#include <utility>
#include <boost/ref.hpp>
#include <ctime>
#include <cmath>
#include <memory>
#include "../compression/abit.hpp"
#include "../compression/jbit.hpp"
#include "../constant_def.hpp"
#include "../mkq_sort.hpp"
#include "../difference_cover.hpp"
#include "../split_sort.hpp"
#include "../mkq_sort.hpp"

#include "boost/serialization/vector.hpp"
#include "boost/serialization/utility.hpp"
#include "boost/archive/binary_oarchive.hpp"
#include "boost/archive/binary_iarchive.hpp"
#include "boost/unordered_map.hpp"


template < class AlignerTableType
					,class IndexerType
					,class SearcherType
				 >
class BWT
	:public IndexerType
	,public SearcherType
{
public:
	/**
	 * @brief Dcs length
	 */
	
	AlignerTableType abwtt_;


	BWT()
		:IndexerType(abwtt_)
		,SearcherType(abwtt_)
	{};

private:
	
};







#endif
