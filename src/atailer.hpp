#ifndef ATAILER_HPP_
#define ATAILER_HPP_

//#include "format/fastq.hpp"



#include <mutex>
#include <sstream>

#include "boost/serialization/map.hpp"
#include "boost/filesystem.hpp"
#include "boost/thread.hpp"

#include "../src/thread_pool_update.hpp"
#include "file_reader.hpp"

#include "constant_def.hpp"

#include "abwt.hpp"
#include "abwt_table.hpp"
#include "abwt_search.hpp"


#include "format/abwt_fasta.hpp"
#include "format/abwt_fastq.hpp"
#include "format/abwt_sam.hpp"


/* Segmen class */
// a class that stores Ns and ACGTs separately, one Segment is basically: NNN...NNNACGT...ACGT
// there could be one segment that does not have N, then _offset == 0
// there could be one segment that has only N, then _len == 0
// one Fasta can contain many Segments



//Aligner_genome_pre_handler (parser)
class Building_BWT_IO
{
public:
	/* string to store the sense + reverse complementary of the genome seq */
	std::string seq, seqRC {};
	
	Building_BWT_IO(){}
	// pre_sort_length = dcs length
	void loadFile (std::vector<std::string> filelist, std::string prefixName)
	{	
		/* running accumulator recording the length of each chr */
		INTTYPE tempLen {0}, accumulatedLength {0};
		/* for concatenated seq */
		std::map <INTTYPE, INTTYPE> NPosLen { };
	
		/* file to store which regions has which chr*/
		std::ofstream chrStartPos {prefixName + "chrStart"};
		/* file to store the length of each chr */
		std::ofstream chrLen {prefixName + "chrLen"};
		/* read in each fasta and make two string */
		
		Fasta_parser fa (filelist);
		
		fa.parser_combo([&fa, &chrStartPos, &chrLen, &accumulatedLength, &tempLen, &NPosLen, this]()
			{
				/* store start position of each chr */
				chrStartPos << fa.getName () << '\t' << accumulatedLength << '\n';
				//std::cout << "accumulatedLength " << fa.getName () << '\t' << accumulatedLength << '\n';
				/* get chr length */
				tempLen = fa.getLengthNoN ();
				/* store chr length */
				chrLen << fa.getName () << '\t' << tempLen << '\n';
				//std::cout << "tempLen " << fa.getName () << '\t' << tempLen << '\n';
				/* update accumulated length */
				accumulatedLength += tempLen;
				/* update NPosLen */
				fa.updateNpos (NPosLen);
				this->seq += fa.getSeqNoN ();	
			}
		);
		chrStartPos.close ();
		chrLen.close ();
		
		/* resize to enough space for the reverse complemetary sequence and a $ sign */
		seq.resize (seq.size () * 2 + 1); // TODO: resize does mallocating the extra space and also initialization, the later is not necessary
		auto iter = seq.begin ();
		std::advance (iter, (seq.size ()-1)/2);
		auto iter2 = iter;
		--iter2;
		do {
			switch (*iter2) {
			case 'A':
				*iter = 'T'; break;
			case 'T':
				*iter = 'A'; break;
			case 'G':
				*iter = 'C'; break;
			case 'C':
				*iter = 'G'; break;
			}
			++iter;
		} while (iter2-- != seq.begin ());
		*iter = '$';
		/* writing NPosLen to file */
		
		{
			
			std::ofstream fp(prefixName + "NposLen.z", std::ios::binary);
			boost::archive::binary_oarchive archive_fp( fp );
			archive_fp & NPosLen;
			fp.close();
			std::cerr << "NPosLen size : " << NPosLen.size() << std::endl;
			
			for (auto i : NPosLen)
				std::cerr << i.first << ":" << i.second << std::endl;
			
			/*
			boost::iostreams::filtering_ostream fos;
			fos.push (boost::iostreams::zlib_compressor());
			fos.push (boost::iostreams::file_sink (prefixName + "NposLen.z"));
			boost::archive::binary_oarchive oa (fos);
			oa << NPosLen;
			*/
		}
		
	}

};


//Indexer
template< class _BWT_TYPE >
class Building_BWT
	:public Building_BWT_IO
{
public:
	Building_BWT()
	{}
	void buildBWT(std::vector<std::string> filelist, std::string prefixName, int pre_sort_length=512, int interval=64)
	{
		this->loadFile(filelist, prefixName);
		typename _BWT_TYPE::SeqType x ( this->seq );
		
		//BWT/Blast
		//SBWT, ABWT
		_BWT_TYPE y (x, pre_sort_length, interval, prefixName);
	}
	
};


/* ! SEARCHING BWT IMPL */

class Searching_BWT_IO
{
public:
	uint64_t in_group_reads_number;
	std::mutex reader_mutex, writer_mutex;
	// ! for searcher
	ABWT_table abwt_table_;
	
	Searching_BWT_IO()
		:in_group_reads_number(1000)
	{}
	
	inline void loadBWT(const std::string& prefixName)
	{
		abwt_table_.readTable(prefixName + "t_table.bwt");
		abwt_table_.readNPosLen (prefixName + "NposLen.z");
		abwt_table_.readChrStartPos (prefixName + "chrStart");
		abwt_table_.readChrLen (prefixName + "chrLen");
	}
	
	/**
	 * @brief mt_search 提供 search function，可以指定 searcher 的 member function
	 *			會提供 class searcher, BASIC FORMAT, 和輸出用 stringstream
	 *			此功能主要是可以讓使用者自定一些參數，像是 Tailer 用的 minLen 
	 * @tparam ParserType 
	 */
	// dispatch_job
	template<class ParserType, class SearcherType>
	void mt_search(
		ParserType &file_parser
		, std::size_t nthreads
		, std::ofstream &out
		, std::function<void(SearcherType&, class ParserType::value_type &, std::stringstream&)> search_function)
	{	
		size_t file_idx(0);	
		
		bool eof_flag(false);
		GlobalPool.ChangePoolSize(nthreads);
		

		for(auto i(0); i<nthreads; ++i )
		{
			
			GlobalPool.JobPost( 
				[this, &file_parser, &file_idx, &eof_flag, &out, &search_function]()
				{
					SearcherType searcher (this->abwt_table_);
					std::stringstream out_stream;
					
					std::vector< typename ParserType::value_type > group;
					group.reserve( this->in_group_reads_number );
					while(true)
					{
						{ // ! lock_guard 解構
							std::lock_guard<std::mutex> lock(reader_mutex);
							if(eof_flag)
								break;
								
							// ! read fastq reads to a group
							uint64_t num (0);
							for(uint64_t num(0); num != this->in_group_reads_number; ++num)
							{
								// ! read one fastq read and save into group
								group.emplace_back( std::move (file_parser.get_next_entry (file_idx) ) );
								//std::cerr << group.back().eof_flag << std::endl;
								if ( group.back().eof_flag )
								{
									
									//remove last nothing fastq
									group.pop_back();	
									// this file is eof, change to next file
									file_idx++;
									if(file_parser.file_num_ == file_idx)
										eof_flag = true;
									break;
								}
								++num;
							}
							
							// ! 此檔案已經讀完，讀下一個檔案
							if(group.size() == 0)
								continue;
						} // ! lock_guard 解構
						// ! for searching...
						for ( auto &fp_v : group)
						{
							search_function(searcher, fp_v, out_stream);
						}
						// ! for writing into file...
						{
							std::lock_guard<std::mutex> lock(writer_mutex);
							out << out_stream.rdbuf();
							out_stream.str("");
						}
						
						group.clear();
					}
				}
				, std::vector<size_t>(0) 
			);
			
			
		}
		GlobalPool.FlushPool();
	}
	
	
};



template < class _BWT_TYPE, int _SEARCHING_TYPE>
class Searching_BWT
{};


/* BWT search for Tailer */
template<>
class Searching_BWT< ABWT < ABSequence<> >, BWT_SEARCHING_TYPE::Tailer>
	:public Searching_BWT_IO
{
private:
	

public:

	typedef ABWT <ABSequence<>> _BWT_TYPE;
	typedef ABWT_search<ABWT_table, BWT_SEARCHING_TYPE::Tailer> SearcherType;

	Searching_BWT()
	{}
	
	inline void write_sam_header(std::ostream& out)
	{
		out << "@HD" << '\t' << "VN:1.0" << '\t' << "SO:unsorted\n";
		for (const auto& chrSizes : abwt_table_.chr_length)
		{
			out << "@SQ\tSN:" << chrSizes.first << "\tLN:" << chrSizes.second << '\n';
		}
	}
	
	template<class FileParserType>
	void search(std::vector<std::string> filelist, std::size_t nthreads, std::string out_filename, int minLen)
	{
		//std::cerr << "NPosLen size : " << this->abwt_table_.chr_umbiguous_starting_length.size() << std::endl;
		//for (auto i : this->abwt_table_.chr_umbiguous_starting_length)
		//		std::cerr << i.first << ":" << i.second << std::endl;
		
		std::ofstream out {out_filename};
		write_sam_header(out);
		
		/* !	File parser 必須繼承 file_reader_impl and BASIC FORMAT, e.g., Fasta<>, Fastq<> ... (oman library)
					File parser 必須有 public typedef value_type for BASIC FORMAT
		*/
		FileParserType file_parser (filelist);
		
		/* ! mt_search 提供 search function，可以指定 searcher 的 member function
				會提供 class searcher, BASIC FORMAT, 和輸出用 stringstream，不直接輸出到檔案，可以提高平行效率
				此功能主要是可以讓使用者自定一些參數，像是 Tailer 用的 minLen
		*/
		
		mt_search<FileParserType, SearcherType >
			(file_parser, nthreads, out, 
			[minLen](SearcherType & searcher, class FileParserType::value_type &fp_v, std::stringstream &out_stream)
			{
				searcher.start_tailing_match_Dual(fp_v, out_stream, minLen);
			}
		);
		out.close();
	}	
	
	
};






//Index_builder
//Index_searcher
//Aligner <tailer>
class ATailer
	:	public Building_BWT< ABWT < ABSequence<> > >,
		public Searching_BWT< ABWT < ABSequence<> >, BWT_SEARCHING_TYPE::Tailer>
{
public:
	ATailer()
	{}
private:
	
};



#endif
